from django.conf import settings
from django.core.mail import send_mail, EmailMultiAlternatives
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.contrib.sites.models import Site

from time import strptime
from rest_framework.request import Request
from xml.dom.minidom import parseString
from requests_oauthlib import OAuth1Session
import json


def clone_REST_request(request):
    req = Request(
        request=request._request,
        parsers=request.parsers,
        authenticators=request.authenticators,
        negotiator=request.negotiator,
        parser_context=request.parser_context
    )
    req._data = request.DATA.copy()
    return req


def get_api_version(request):
    current_path = request.path.split('/')
    return current_path[1].replace('v', '')


def get_oauth_session():
    return OAuth1Session(
        '',
        '',
        resource_owner_key=settings.VZAAR_API['key'],
        resource_owner_secret=settings.VZAAR_API['secret']
    )


def get_VZAAR_video_detail(vzaar_video_id, format='.xml', embed_only=False):
    session = get_oauth_session()
    url = settings.VZAAR_SETTINGS['video_url'].format(
        str(vzaar_video_id) + format
    )
    if embed_only:
        url += '?embed_only=True'
    resp = session.get(url)

    if format == '.xml':
        return parseString(resp.content)
    elif format == '.json':
        return json.loads(resp.content)
    else:
        return resp.content


def db_strptime(time_str):
    try:
        result = strptime(time_str, '%Y-%m-%dT%H:%M:%S.%f')
    except ValueError:
        result = strptime(time_str, '%Y-%m-%dT%H:%M:%S')
    return result


def send_alert_email(alert_type, options):
    to_email = settings.SUPPORT_EMAIL
    if alert_type == 'FLAG_VIDEO':
        subject = '[ALERT] Video gets many flagged'
        request = options.get('request')
        video = options.get('video')
        message = (
            u'Video "{}" ({}) has received more than {} flags. '
            'You can monitor and disable it at: {}'
            .format(
                video.name,
                video.web_url,
                options.get('limit'),
                request.build_absolute_uri(
                    '/urbanjungleadmin/videos/flaggedvideo/'
                ),
            )
        )
    elif alert_type == 'FLAG_COMMENT':
        subject = '[ALERT] Comment gets many flagged'
        request = options.get('request')
        comment = options.get('comment')
        message = (
            u'Comment:'
            '   "{}"'
            '(from user "{}") has received more than {} flags. '
            'You can monitor and disable it at: {}'
            .format(
                comment.comment,
                request.user.username,
                options.get('limit'),
                request.build_absolute_uri(
                    '/urbanjungleadmin/comments/flaggedcomment/'
                ),
            )
        )
    elif alert_type == 'VZAAR_ERROR':
        subject = '[ALERT] Vzaar error'
        message = (
            u'Error: {}\n'
            'Vzaar request: {}'.format(
                options.get('error'),
                options.get('vzaar_request')
            )
        )

    if subject:
        send_mail(subject, message, settings.SERVER_EMAIL, [to_email])


def find_dict_with_val(lst, key, value):
    result = []
    for i, dic in enumerate(lst):
        if dic[key] == value:
            result.append(dic)
    return result


def send_templated_email(template, subject, to_emails=[], context={}):
    current_site = Site.objects.get_current()
    context['domain'] = settings.PROTOCOL + current_site.domain

    html_content = render_to_string(template, context)
    text_content = strip_tags(html_content)
    msg = EmailMultiAlternatives(
        subject=unicode(subject),
        body=text_content,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=to_emails,
    )
    msg.attach_alternative(html_content, 'text/html')
    return msg.send()
