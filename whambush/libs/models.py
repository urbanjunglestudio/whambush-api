from django.db import models
from django.utils.timezone import now as django_now


class TimeStampedModel(models.Model):
    '''
    Abstract base class to provide created_at and modified_at
    '''
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class SoftDeleteModel(models.Model):
    '''
    Abstract base class to provide deleted_at field
    and soft-delete related methods and properties
    '''
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        abstract = True

    def delete(self, force=False):
        if force:  # force delete
            super(SoftDeleteModel, self).delete()
        else:  # soft delete in all normal cases
            self.deleted_at = django_now()
            self.save()

    @property
    def is_deleted(self):
        return self.deleted_at is not None
