from django.contrib.contenttypes.generic import GenericForeignKey
from django.db.models.query import QuerySet, EmptyQuerySet
from django.db.models import Manager, Q


class WBManager(Manager):
    """
    A manager that returns a WBQuerySet instead of a regular QuerySet.
    """
    def get_query_set(self):
        return WBQuerySet(self.model, using=self._db)


class WBQuerySet(QuerySet):
    """
    A QuerySet with a fetch_reverse_relations() method to bulk fetch
    all related items.  Similar to select_related(), but for
    reverse relations.
    """
    # TODO: possible very slow query
    def fetch_reverse_relations(
        self, target_name, related_model, related_field, *args, **kwargs
    ):
        objs = list(self._clone())
        objs_dict = dict([(obj.id, obj) for obj in objs])
        objs = []
        related_qs = related_model.objects.filter(
            **{related_field + '__in': objs_dict.keys()})
        related_qs.query.add_q(Q(*args, **kwargs))
        related_objs = list(related_qs)
        for k, obj in objs_dict.items():
            setattr(obj, target_name, [])
            for foreign in related_objs:
                obj_id = getattr(foreign, related_field + '_id')
                if obj_id == obj.id:
                    getattr(obj, target_name).append(foreign)
            objs.append(obj)
        objs_dict.clear()
        return objs


class GFKManager(Manager):
    """
    A manager that returns a GFKQuerySet instead of a regular QuerySet.

    """
    def get_query_set(self):
        return GFKQuerySet(self.model)
    get_queryset = get_query_set

    def none(self):
        return self.get_queryset().none()


class GFKQuerySet(QuerySet):
    """
    A QuerySet with a fetch_generic_relations() method to bulk fetch
    all generic related items.  Similar to select_related(), but for
    generic foreign keys. This wraps QuerySet.prefetch_related.
    """
    def fetch_generic_relations(self, *args):
        qs = self._clone()

        # if not settings.FETCH_RELATIONS:
        #     return qs

        gfk_fields = [
            g for g in self.model._meta.virtual_fields
            if isinstance(g, GenericForeignKey)
        ]

        if args:
            gfk_fields = [g for g in gfk_fields if g.name in args]

        return qs.prefetch_related(*[g.name for g in gfk_fields])

    def none(self):
        clone = self._clone(klass=EmptyGFKQuerySet)
        if hasattr(clone.query, 'set_empty'):
            clone.query.set_empty()
        return clone


class EmptyGFKQuerySet(GFKQuerySet, EmptyQuerySet):
    def fetch_generic_relations(self, *args):
        return self
