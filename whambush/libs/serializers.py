from rest_framework import serializers


class DynamicFieldsModelSerializer(serializers.HyperlinkedModelSerializer):
    '''
    A ModelSerializer that takes an additional `fields` argument that
    controls which fields should be displayed.
    '''
    excluded_fields = []

    def __init__(self, *args, **kwargs):
        # Don't pass the 'excluded_fields' arg up to the superclass
        self.excluded_fields = kwargs.pop('excluded_fields', [])

        # Instantiate the superclass normally
        super(DynamicFieldsModelSerializer, self).__init__(*args, **kwargs)

        if self.excluded_fields:
            # Drop any fields that are excluded.
            for field_name in self.excluded_fields:
                if field_name in self.fields:
                    self.fields.pop(field_name)
