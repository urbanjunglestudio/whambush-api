from django.core.files.storage import FileSystemStorage
from django.conf import settings
from django.contrib.sites.models import Site

import os
import re
import datetime
import urlparse


class OverwriteStorage(FileSystemStorage):
    storage_dir = ''

    def __init__(self, *args, **kwargs):
        self.storage_dir = kwargs['dir']
        del(kwargs['dir'])
        super(OverwriteStorage, self).__init__(*args, **kwargs)

    def get_available_name(self, name):
        '''
        Overwrite default get_available_name. When name file is uploaded,
        delete file which start with instance id
        '''
        base_dir = os.path.join(settings.MEDIA_ROOT, self.storage_dir)
        if not os.path.exists(base_dir):
            return name
        # search for instance id
        number_match = re.search(r'[0-9]+', name)
        if number_match:
            instance_id = number_match.group(0)
            pattern = '{}-[0-9]+-[0-9]+\.'.format(instance_id)
            # find file with pattern 'userid-number-number.'
            for f in os.listdir(base_dir):
                if re.match(pattern, f):
                    os.remove(os.path.join(base_dir, f))
        return name

    def get_path(self, instance, filename):
        # get file extension of file name
        filename_part = os.path.splitext(os.path.basename(
            urlparse.urlsplit(filename).path
        ))
        filename_extension = filename_part[1]
        date_time = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
        return os.path.join(
            self.storage_dir,
            '{}-{}{}'.format(instance.id, date_time, filename_extension)
        )

    def url(self, name):
        uri = super(OverwriteStorage, self).url(name)
        current_site = Site.objects.get_current()
        return (
            settings.PROTOCOL + current_site.domain + uri
        )


class ImageFileStorage(FileSystemStorage):
    storage_dir = ''

    def __init__(self, *args, **kwargs):
        self.storage_dir = kwargs['dir']
        del(kwargs['dir'])
        super(ImageFileStorage, self).__init__(*args, **kwargs)

    def get_path(self, instance, filename):
        # get file extension of file name
        filename_part = os.path.splitext(os.path.basename(
            urlparse.urlsplit(filename).path
        ))
        filename_extension = filename_part[1]
        date_time = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
        prefix = instance.id
        if prefix is None:
            if hasattr(instance, 'slug'):
                prefix = instance.slug
            else:
                prefix = 'image'
        return os.path.join(
            self.storage_dir,
            '{}-{}{}'.format(prefix, date_time, filename_extension)
        )

    def url(self, name):
        uri = super(ImageFileStorage, self).url(name)
        current_site = Site.objects.get_current()
        return (
            settings.PROTOCOL + current_site.domain + uri
        )
