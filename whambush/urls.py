from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic.base import RedirectView


from whambush.apps.main.sitemaps import StaticViewSitemap, VideoSitemap

admin.autodiscover()
sitemaps = {
    'static': StaticViewSitemap,
    'video': VideoSitemap
}

urlpatterns = patterns(
    '',
    url(r'^urbanjungleadmin/', include(admin.site.urls)),
    url(r'^sitemap\.xml/$',
        'django.contrib.sitemaps.views.sitemap',
        {'sitemaps': sitemaps},
        name='sitemap'),
    url(r'^rosetta/', include('rosetta.urls')),

    # ----- WHAMBUSH APP  ----- #
    url(r'^$', 'whambush.apps.main.views.home', name='home'),
    # redirect views for app store and google play
    url(r'^ios/$',
        RedirectView.as_view(
            url='https://itunes.apple.com/fi/app/whambush/id876370933'
        ),
        name='ios_redirect'),
    url(r'^android/$',
        RedirectView.as_view(
            url='https://play.google.com/store/apps/details?'
                'id=com.whambush.android.client'
        ),
        name='android_redirect'),


    # --- include ---
    url(r'^account/',  include('whambush.apps.accounts.urls')),
    # url(r'^v/',  include('whambush.apps.videos.urls')),
    url(r'^',  include('whambush.apps.api.urls')),
)

# serve media and static
if settings.DEBUG:
    urlpatterns += patterns(
        '',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',
            {'document_root': settings.MEDIA_ROOT}),
    )

    from django.contrib.staticfiles.urls import staticfiles_urlpatterns
    urlpatterns += staticfiles_urlpatterns()
