# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'SupportedCountry.name'
        db.delete_column(u'country_supportedcountry', 'name')


    def backwards(self, orm):

        # User chose to not deal with backwards NULL issues for 'SupportedCountry.name'
        raise RuntimeError("Cannot reverse this migration. 'SupportedCountry.name' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'SupportedCountry.name'
        db.add_column(u'country_supportedcountry', 'name',
                      self.gf('django.db.models.fields.CharField')(max_length=30),
                      keep_default=False)


    models = {
        u'country.supportedcountry': {
            'Meta': {'object_name': 'SupportedCountry'},
            'country': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['country']