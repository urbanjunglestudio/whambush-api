# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'SupportedCountry', fields ['country']
        db.create_unique(u'country_supportedcountry', ['country'])


    def backwards(self, orm):
        # Removing unique constraint on 'SupportedCountry', fields ['country']
        db.delete_unique(u'country_supportedcountry', ['country'])


    models = {
        u'country.supportedcountry': {
            'Meta': {'object_name': 'SupportedCountry'},
            'country': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['country']