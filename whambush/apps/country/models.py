from django.contrib.sites.models import Site
from django.db import models
from django.conf import settings
from django.templatetags.static import static

from os.path import isfile, join, getmtime


class SupportedCountry(models.Model):
    country = models.CharField(
        max_length=2, choices=settings.COUNTRY_CHOICES, unique=True
    )

    def __unicode__(self):
        return u"{0} - {1}".format(self.country, self.name)

    @property
    def country_code(self):
        return self.country

    @property
    def flag_url(self):
        file_name = "{}.png".format(self.country)
        return self._get_flag_file_url(file_name)

    @property
    def flag_banner_url(self):
        file_name = "{}_banner.jpg".format(self.country)
        return self._get_flag_file_url(file_name)

    @property
    def supported(self):
        return self.id is not None

    @property
    def name(self):
        if self.country in dict(settings.COUNTRY_CHOICES):
            return dict(settings.COUNTRY_CHOICES)[self.country]
        else:
            return ''

    def _get_flag_file_url(self, file_name):
        current_site = Site.objects.get_current()
        file_path = join(settings.COUNTRY_FLAGS_DIR, file_name)
        if isfile(file_path):
            file_mtime = getmtime(file_path)
            relative_path = static("img/{0}/{1}?mt={2}".format(
                settings.COUNTRY_FLAGS_DIR_NAME,
                file_name,
                file_mtime
            ))
            return (
                settings.PROTOCOL + current_site.domain + relative_path
            )
        else:
            return ""
