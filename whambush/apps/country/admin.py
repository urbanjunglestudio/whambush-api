from django.contrib import admin
from django.conf import settings

from .models import SupportedCountry


class CountryAdmin(admin.ModelAdmin):
    list_display = (
        'country_code',
        'name',
        'flag_url',
    )
    fields = (
        'country',
    )


admin.site.register(SupportedCountry, CountryAdmin)
