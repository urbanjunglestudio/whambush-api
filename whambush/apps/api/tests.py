from django.test import TestCase
from django.core.urlresolvers import reverse

from whambush.apps.videos.models import Video
from whambush.apps.activities.models import Activity
from whambush.apps.accounts.models import WhamUser

from datetime import datetime


class ApiWebhookTestcases(TestCase):
    fixtures = ['fixture_api_webhook.json']

    def test_vzaar_call_with_xml_content_type(self):
        path = reverse('vzaar_webhook')
        xml_data = (
            '<?xml version="1.0" encoding="UTF-8"?>'
            '<vzaar-api><video><id>1372082</id></video></vzaar-api>'
        )
        resp = self.client.post(
            path, data=xml_data, content_type='application/xml'
        )
        self.assertEqual(resp.status_code, 200)
        # check if video is processed
        v = Video.objects.get(pk=3)
        self.assertTrue(v.is_processed)
        # check the action stream
        actstr = Activity.objects.latest('id')
        self.assertEqual(actstr.actor.id, 4)
        self.assertEqual(actstr.verb, 'uploaded')
        self.assertEqual(actstr.target.id, 3)

    def test_vzaar_call_with_form_data_content_type(self):
        path = reverse('vzaar_webhook')
        xml_data = (
            '<?xml version="1.0" encoding="UTF-8"?>'
            '<vzaar-api><video><id>1372082</id></video></vzaar-api>'
        )
        resp = self.client.post(
            path,
            data=xml_data,
            content_type='application/x-www-form-urlencoded'
        )
        self.assertEqual(resp.status_code, 200)

    def test_vzaar_call_not_exist_video(self):
        path = reverse('vzaar_webhook')
        xml_data = (
            '<?xml version="1.0" encoding="UTF-8"?>'
            '<vzaar-api><video><id>1</id></video></vzaar-api>'
        )
        # xml content type
        resp = self.client.post(
            path, data=xml_data, content_type='application/xml'
        )
        self.assertEqual(resp.status_code, 400)
        # vzaar current content type
        resp = self.client.post(
            path,
            data=xml_data,
            content_type='application/x-www-form-urlencoded'
        )
        self.assertEqual(resp.status_code, 400)

    def test_vzaar_call_on_already_published_video(self):
        v = Video.objects.create(
            name='test vzaar call',
            external_id='99999999',
            added_by=WhamUser.objects.get(pk=4),
            published_at='2014-01-01T00:00:00',
            country='FI'
        )
        path = reverse('vzaar_webhook')
        xml_data = (
            '<?xml version="1.0" encoding="UTF-8"?>'
            '<vzaar-api><video><id>99999999</id></video></vzaar-api>'
        )
        resp = self.client.post(
            path, data=xml_data, content_type='application/xml'
        )
        self.assertEqual(resp.status_code, 200)
        # check if video is not being updated
        v = Video.objects.get(pk=v.id)
        self.assertEqual(v.published_at, datetime(2014, 1, 1, 0, 0))
