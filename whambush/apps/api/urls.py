from django.conf.urls import patterns, url, include

urlpatterns = patterns(
    '',

    url(r'^vzaar_webhook/$',
        'whambush.apps.api.views.vzaar_webhook',
        name='vzaar_webhook'),

    # we will need new version for each root
    url(
        r'^v1.2/',
        include('whambush.apps.api.v1_2.urls', namespace='api_v1_2')
    ),
    url(
        r'^v1.3/',
        include('whambush.apps.api.v1_3.urls', namespace='api_v1_3')
    ),
    url(
        r'^v2.0/',
        include('whambush.apps.api.v2_0.urls', namespace='api_v2_0')
    ),
    url(
        r'^v2.1/',
        include('whambush.apps.api.v2_1.urls', namespace='api_v2_1')
    ),
)
