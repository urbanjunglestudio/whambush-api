from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from rest_framework.decorators import permission_classes, api_view
from rest_framework.response import Response

from ..serializers import ActivitySerializer, KPISerializer
from whambush.apps.activities.models import Activity


@permission_classes((IsAuthenticated, ))
class ListActivity(generics.ListAPIView):
    model = Activity
    serializer_class = ActivitySerializer

    def get_queryset(self):
        return Activity.objects.wb_stream(self.request.user)


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def read(request):
    Activity.objects.wb_stream(request.user).update(is_read=True)
    return Response({'success': True}, status=status.HTTP_200_OK)


@permission_classes((IsAdminUser, ))
class ListAllActivity(generics.ListAPIView):
    model = Activity
    serializer_class = KPISerializer
