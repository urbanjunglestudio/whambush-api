from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.decorators import permission_classes

from ..serializers import (
    VideoSerializer, FlagsSerializer, VideoFeedSerializer
)
from ..services import (
    get_serializer_listview,
    response_invalid_args
)
from whambush.apps.api.permissions import IsOwnerOrReadOnly
from whambush.apps.videos.models import (
    Video, VideoFeed, FeaturedVideos, Flag
)
from whambush.apps.accounts.models import Follow
from whambush.apps.hashtags.models import Hashtag
from whambush.libs import utils

from constance import config
from random import shuffle, random
import re


@permission_classes((IsAuthenticated, ))
class ListCreateVideo(generics.ListCreateAPIView):
    model = Video
    serializer_class = VideoSerializer

    def pre_save(self, obj):
        obj.added_by = self.request.user
        obj.is_active = obj.video_type == 0
        if self.request.user.country:
            obj.country = self.request.user.country
        else:
            obj.country = 'FI'

    def post_save(self, obj, created=True):
        tags_request = self.request.DATA.get('tags')
        tags = []
        if tags_request:
            for tag in tags_request.strip().split(','):
                tag = unicode(re.sub('[^\w]+', '', tag, flags=re.UNICODE))
                qs = Hashtag.objects.filter(name__iexact=tag)
                if qs:
                    tags.append(qs[0])
                else:
                    tags.append(Hashtag.objects.create(name=tag))
        obj.tags = tags

    def get_queryset(self):
        filter_type = self.request.GET.get('type')
        if filter_type == 'my':
            # my videos
            return Video.objects.filter(
                added_by=self.request.user,
                is_active=True,
                deleted_at=None
            ).wb_order()
        elif filter_type == 'featured':
            # featured videos
            return FeaturedVideos.objects.last().videos.active()
        elif filter_type == 'following':
            # videos from user I'm following
            return Video.objects.active().filter(
                added_by__in=(
                    Follow.objects
                    .filter(from_user=self.request.user)
                    .values_list('to_user', flat=True)
                )
            ).wb_order()
        elif filter_type == 'random':
            # 10 random videos
            return Video.objects.random(10)
        elif filter_type == 'tv':
            # tv videos
            return Video.objects.active().filter(video_type=2).wb_order()
        else:
            # latest videos
            return Video.objects.active().exclude(video_type=2).wb_order()


@permission_classes((IsAuthenticated, IsOwnerOrReadOnly))
class RetrieveDestroyVideo(generics.RetrieveDestroyAPIView):
    model = Video
    serializer_class = VideoSerializer


@permission_classes((IsAuthenticated, ))
class ListCreateVideoFlag(generics.ListCreateAPIView):
    model = Flag
    serializer_class = FlagsSerializer

    def get_queryset(self):
        return Flag.objects.filter(
            object_pk=self.kwargs.pop('video_id'),
            flag_type=0,
            user=self.request.user
        )

    def post(self, request, *args, **kwargs):
        # validate request
        video_id = kwargs.pop('video_id')
        if video_id is None or not video_id.isdigit():
            return response_invalid_args()
        try:
            video = Video.objects.get(pk=video_id)
        except Video.DoesNotExist:
            return response_invalid_args()

        # remove previous flag of same user
        flag = Flag.objects.filter(
            object_pk=video_id,
            flag_type=0,
            user=request.user
        )
        flag.delete()

        # update the request and save
        req = utils.clone_REST_request(request)
        req._data.update({
            'video': video_id,
            'user': reverse(
                'api_v1_3:whamuser-detail', kwargs={'pk': request.user.id}
            )
        })
        result = super(ListCreateVideoFlag, self).post(req, args, kwargs)

        # send email to support if needed
        limit = int(config.FLAG_LIMIT)
        flag_count = Flag.objects.filter(
            object_pk=video.pk, flag_type=0
        ).count()
        if flag_count == limit:
            utils.send_alert_email('FLAG_VIDEO', {
                'video': video,
                'request': request,
                'limit': limit
            })

        return result


@permission_classes((IsAuthenticated, ))
class RetrieveDestroyFlag(generics.RetrieveDestroyAPIView):
    model = Flag
    serializer_class = FlagsSerializer


@permission_classes((IsAuthenticated, ))
class ListVideoFeed(generics.ListAPIView):
    model = VideoFeed
    serializer_class = VideoFeedSerializer

    def get_queryset(self):
        version = utils.get_api_version(self.request)
        return VideoFeed.objects.filter(version=version).order_by('order')

    def get(self, request, *args, **kwargs):
        version = utils.get_api_version(request)
        default = 1
        qs = VideoFeed.objects.filter(version=version, is_default=True)
        if qs:
            default = qs[0].id
        serializer = get_serializer_listview(self)  # TODO: better idea?
        serializer.data.update({'default': default})
        return Response(serializer.data)


@permission_classes((IsAuthenticated, ))
class SearchVideo(generics.ListAPIView):
    model = Video
    serializer_class = VideoSerializer

    def get_queryset(self):
        order = self.request.GET.get('order')
        if self.request.GET.get('user', ''):
            user_ids = [
                int(user_id.strip())
                for user_id in self.request.GET.get('user').split(',')
            ]
            return Video.objects.active().filter(
                added_by__in=user_ids
            ).wb_order(order)
        elif self.request.GET.get('mission', ''):
            # default in mission should be rank
            if not order:
                order = 'rank'
            return Video.objects.active().filter(
                mission=self.request.GET.get('mission')
            ).wb_order(order)
        elif self.request.GET.get('query', ''):
            search_query = self.request.GET.get('query', '')
            return Video.objects.search(search_query).wb_order(order)
        else:
            return Video.objects.none()

    def paginate_queryset(self, queryset):
        page = super(SearchVideo, self).paginate_queryset(queryset)
        # we need to handle the random order here after the queryset is sliced
        # so new request (for next page) will not intefere with the result
        if self.request.GET.get('order') == 'random':
            page.object_list = sorted(page.object_list, key=lambda x: random())
        return page

    def get(self, request, *args, **kwargs):
        if self.request.GET.get('user', ''):
            try:
                [
                    int(user_id.strip())
                    for user_id in self.request.GET.get('user').split(',')
                ]
            except ValueError:
                return Response(
                    {'detail': 'Invalid user ids'},
                    status=status.HTTP_400_BAD_REQUEST
                )
        elif self.request.GET.get('mission', ''):
            try:
                int(self.request.GET.get('mission'))
            except ValueError:
                return Response(
                    {'detail': 'Invalid mission id'},
                    status=status.HTTP_400_BAD_REQUEST
                )
        elif not self.request.GET.get('query', ''):
            return Response({
                'detail': 'Please specify a parameter'
                          ' (user, mission or query)'
                }, status=status.HTTP_400_BAD_REQUEST
            )
        return super(SearchVideo, self).get(request, *args, **kwargs)
