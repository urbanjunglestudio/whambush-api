from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from ..serializers import MissionSerializer
from whambush.apps.missions.models import Mission


@permission_classes((IsAuthenticated, ))
class ListMission(generics.ListAPIView):
    model = Mission
    serializer_class = MissionSerializer

    def get_queryset(self):
        filter_type = self.request.GET.get('type')
        if filter_type == 'old':
            # old missions
            return Mission.objects.old().wb_order()
        else:
            # active missions
            return Mission.objects.active().wb_order(True)


@permission_classes((IsAuthenticated, ))
class MissionDetail(generics.RetrieveAPIView):
    model = Mission
    serializer_class = MissionSerializer
