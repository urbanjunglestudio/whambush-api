from rest_framework import generics
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import permission_classes

from ..serializers import APNSDeviceSerializer, GCMDeviceSerializer
from whambush.libs.utils import clone_REST_request

from push_notifications.models import APNSDevice, GCMDevice


@permission_classes((IsAuthenticated, ))
class CreateDestroyDevice(generics.CreateAPIView, generics.DestroyAPIView):
    def get_serializer_class(self):
        if self.request.DATA.get('type') == 'apns':
            return APNSDeviceSerializer
        elif self.request.DATA.get('type') == 'gcm':
            return GCMDeviceSerializer

    def post(self, request, *args, **kwargs):
        if request.DATA.get('type') not in ['apns', 'gcm']:
            return Response(
                {'detail': 'Invalid type'},
                status=status.HTTP_400_BAD_REQUEST
            )

        if (
            request.DATA.get('type') == 'gcm'
            and GCMDevice.objects.filter(
                user=request.user,
                registration_id=request.DATA.get('registration_id')
            )
        ):
            return Response(
                {'detail': 'GCM device with Registration ID already exists.'},
                status=status.HTTP_400_BAD_REQUEST
            )

        req = clone_REST_request(request)
        req._data.update({
            'user': reverse(
                'api_v1_3:whamuser-detail', kwargs={'pk': request.user.id}
            )
        })
        return super(CreateDestroyDevice, self).post(req, args, kwargs)

    def delete(self, request):
        d_type = request.DATA.get('type')
        reg_id = request.DATA.get('registration_id')
        if d_type not in ['apns', 'gcm'] or not reg_id:
            return Response(
                {'detail': 'Missing or invalid params'},
                status=status.HTTP_400_BAD_REQUEST
            )

        if d_type == 'apns':
            try:
                device = APNSDevice.objects.get(
                    registration_id=reg_id,
                    user=request.user
                )
            except APNSDevice.DoesNotExist:
                return Response(
                    {'detail': 'Device does not exist'},
                    status=status.HTTP_400_BAD_REQUEST
                )
            else:
                device.delete()
        else:
            try:
                device = GCMDevice.objects.get(
                    registration_id=reg_id,
                    user=request.user
                )
            except GCMDevice.DoesNotExist:
                return Response(
                    {'detail': 'Device does not exist'},
                    status=status.HTTP_400_BAD_REQUEST
                )
            else:
                device.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
