from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import login, set_token

from push_notifications.models import APNSDevice, GCMDevice
import json


class ApiDevicesTestcases(TestCase):
    fixtures = ['fixture_api_v1_3.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_create_and_remove_device(self):
        path = reverse('api_v1_3:create-destroy-device')
        # APNS
        resp = self.client.post(path, {
            'type': 'apns',
            'registration_id': 'ios'
        })
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(APNSDevice.objects.count(), 1)

        # GCM
        resp = self.client.post(path, {
            'type': 'gcm',
            'registration_id': 'android'
        })
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(GCMDevice.objects.count(), 1)

        # missing type param
        resp = self.client.post(path, {
            'registration_id': 'fail'
        })
        self.assertEqual(resp.status_code, 400)

        # too long reg ID for APNS
        resp = self.client.post(path, {
            'type': 'apns',
            'registration_id': 'a' * 65
        })
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(APNSDevice.objects.count(), 1)

    def test_remove_device(self):
        path = reverse('api_v1_3:create-destroy-device')
        resp = self.client.post(path, {
            'type': 'apns',
            'registration_id': 'ios'
        })
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(APNSDevice.objects.count(), 1)

        # bad delete request
        resp = self.client.post(path, {
            '_method': 'DELETE',
            'type': 'apns',
            'registration_id': 'lala'
        })
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(APNSDevice.objects.count(), 1)

        # good delete request
        resp = self.client.post(path, {
            '_method': 'DELETE',
            'type': 'apns',
            'registration_id': 'ios'
        })
        self.assertEqual(resp.status_code, 204)
        self.assertEqual(APNSDevice.objects.count(), 0)

        # GCM
        resp = self.client.post(path, {
            'type': 'gcm',
            'registration_id': 'android'
        })
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(GCMDevice.objects.count(), 1)
        resp = self.client.post(path, {
            '_method': 'DELETE',
            'type': 'gcm',
            'registration_id': 'lala'
        })
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(GCMDevice.objects.count(), 1)
        resp = self.client.post(path, {
            '_method': 'DELETE',
            'type': 'gcm',
            'registration_id': 'android'
        })
        self.assertEqual(resp.status_code, 204)
        self.assertEqual(GCMDevice.objects.count(), 0)

    def test_multiple_devices(self):
        path = reverse('api_v1_3:create-destroy-device')

        # ------------- APNS
        resp = self.client.post(path, {
            'type': 'apns',
            'registration_id': 'ios'
        })
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(APNSDevice.objects.count(), 1)

        # same token
        resp = self.client.post(path, {
            'type': 'apns',
            'registration_id': 'ios'
        })
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(APNSDevice.objects.count(), 1)

        # different token
        resp = self.client.post(path, {
            'type': 'apns',
            'registration_id': 'ios2'
        })
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(APNSDevice.objects.count(), 2)

        # ------------- GCM
        resp = self.client.post(path, {
            'type': 'gcm',
            'registration_id': 'android'
        })
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(GCMDevice.objects.count(), 1)

        resp = self.client.post(path, {
            'type': 'gcm',
            'registration_id': 'android'
        })
        self.assertEqual(resp.status_code, 400)
        self.assertEqual(GCMDevice.objects.count(), 1)

        resp = self.client.post(path, {
            'type': 'gcm',
            'registration_id': 'android2'
        })
        self.assertEqual(resp.status_code, 201)
        self.assertEqual(GCMDevice.objects.count(), 2)
