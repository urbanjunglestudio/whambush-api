from django.test import TestCase
from django.core.urlresolvers import reverse
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core import mail

from .services import login, set_token, reverse_follow, reverse_unfollow
from whambush.apps.accounts.models import WhamUser
from whambush.apps.promotions.models import PromotionKey

from constance import config
import json
import os
import time


class ApiGuestFlowTestcases(TestCase):
    fixtures = ['fixture_api_v1_3.json']
    guest_id = None

    def setUp(self):
        config.IS_BETA = False
        resp = self.client.post(reverse('api_v1_3:token-auth'), {})
        json_resp = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(json_resp.get('is_guest'))
        self.guest_id = json_resp.get('guest_id')
        self.assertTrue(self.guest_id is not None)

    def test_guest_auth(self):
        # success
        resp = self.client.post(reverse('api_v1_3:token-auth'), {
            'guest_id': self.guest_id
        })
        json_resp = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(json_resp.get('is_guest'))
        self.assertTrue(json_resp.get('guest_id') is not None)
        self.assertTrue(json_resp.get('token') is not None)

        # fail
        resp = self.client.post(reverse('api_v1_3:token-auth'), {
            'guest_id': 1000
        })
        self.assertEqual(resp.status_code, 400)

    def test_guest_auth_with_credentials(self):
        resp = self.client.post(reverse('api_v1_3:token-auth'), {
            'username': 'test_user_1',
            'password': 'test_user_1',
            'guest_id': self.guest_id
        })
        json_resp = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertFalse(json_resp.get('is_guest'))

        # test linked user
        user_id = json_resp.get('user').get('id')
        guest = WhamUser.objects.get(pk=self.guest_id)
        self.assertEqual(guest.linked_user.id, user_id)

    def test_guest_register_success(self):
        resp = self.client.post(reverse('api_v1_3:list-create-user'), {
            'username': 'test_username',
            'email': 'username@test.com',
            'password1': 'test_password',
            'password2': 'test_password',
            'guest_id': self.guest_id
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('username'), 'test_username')
        self.assertEqual(json_resp.get('id'), self.guest_id)

    def test_guest_register_failure(self):
        resp = self.client.post(reverse('api_v1_3:list-create-user'), {
            'username': 'test_username',
            'email': 'username@test.com',
            'password1': 'test_password',
            'password2': 'test_password',
            'guest_id': 1000
        })
        self.assertEqual(resp.status_code, 400)

    def test_guest_register_twice_with_same_guest_id(self):
        resp = self.client.post(reverse('api_v1_3:list-create-user'), {
            'username': 'test_username1',
            'email': 'username1@test.com',
            'password1': 'test_password',
            'password2': 'test_password',
            'guest_id': self.guest_id
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('username'), 'test_username1')
        self.assertEqual(json_resp.get('id'), self.guest_id)

        # register another user with same guest_id
        resp = self.client.post(reverse('api_v1_3:list-create-user'), {
            'username': 'test_username2',
            'email': 'username2@test.com',
            'password1': 'test_password',
            'password2': 'test_password',
            'guest_id': self.guest_id
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('username'), 'test_username2')
        # it should not update the same user
        self.assertNotEqual(json_resp.get('id'), self.guest_id)


class ApiAccountsViewTestcases(TestCase):
    fixtures = ['fixture_api_v1_3.json']

    def setUp(self):
        # set beta to false
        config.IS_BETA = False

        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_user_detail(self):
        path = reverse('api_v1_3:whamuser-detail', kwargs={'pk': 1})
        self.assertEqual(path, '/v1.3/users/1/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('id'), 1)
        self.assertEqual(json_resp.get('num_videos'), 1)
        self.assertEqual(json_resp.get('num_followers'), 1)
        self.assertEqual(json_resp.get('num_likes'), 2)
        self.assertEqual(json_resp.get('num_dislikes'), 1)
        self.assertEqual(json_resp.get('userscore'), 1)
        self.assertEqual(json_resp.get('activation_state'), 'ACTIVATED')
        self.assertEqual(json_resp.get('date_joined'), '2013-06-06T16:06:39')
        # test userscore is null then default is 0
        path = reverse('api_v1_3:whamuser-detail', kwargs={'pk': 2})
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('userscore'), 0)

    def test_api_non_exist_user(self):
        path = reverse('api_v1_3:whamuser-detail', kwargs={'pk': 50})
        self.assertEqual(path, '/v1.3/users/50/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 404)

    def test_api_follow_create(self):
        resp = self.client.post(reverse_follow(), {
            'to_user': 2
        })
        self.assertEqual(resp.status_code, 201)

    def test_api_follow_delete(self):
        path = reverse_unfollow(1)
        resp = self.client.delete(path)
        self.assertEqual(resp.status_code, 204)

    def test_api_list_follower(self):
        path = reverse_follow() + '?type=followers'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        self.assertEqual(json_resp.get('results')[0].get('id'), 2)

    def test_api_list_following(self):
        path = reverse_follow() + '?type=following'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        self.assertEqual(json_resp.get('results')[0].get('id'), 1)

    def test_api_update_user(self):
        path = reverse('api_v1_3:whamuser-detail', kwargs={'pk': 3})
        # check validate email
        resp = self.client.post(path, {
            "_method": "PUT",
            'email': "wrong"
        })
        self.assertEqual(resp.status_code, 400)
        # check validate new password
        resp = self.client.post(path, {
            "_method": "PUT",
            'new_password': ""
        })
        self.assertEqual(resp.status_code, 400)
        # password too short
        resp = self.client.post(path, {
            "_method": "PUT",
            'new_password': "1"
        })
        self.assertEqual(resp.status_code, 400)
        # check validate birthday
        resp = self.client.post(path, {
            "_method": "PUT",
            'birthday': "123"
        })
        self.assertEqual(resp.status_code, 400)
        # check success
        resp = self.client.post(path, {
            "_method": "PUT",
            "email": "test@tester.com",
            "description": "This is a test",
            'birthday': "2000-01-10",
            "country": "FI"
        })
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('email'), 'test@tester.com')
        # other's user ID
        path = reverse('api_v1_3:whamuser-detail', kwargs={'pk': 1})
        resp = self.client.post(path, {
            "_method": "PUT",
            "email": "test@tester.com",
            "description": "This is a test",
            'birthday': "2000-01-10",
            "country": "FI"
        })
        self.assertEqual(resp.status_code, 400)

    def test_api_update_user_profile_picture(self):
        # TODO: this test has problem that it uploads an
        # actual file to media/profile_picture --> using
        # pk = -1?
        path = reverse('api_v1_3:whamuser-detail', kwargs={'pk': 3})
        img = open(settings.STATICFILES_DIRS[0]+"/img/banana.png", "rb")
        uploaded = SimpleUploadedFile(img.name, img.read())
        # image validation
        resp = self.client.post(path, {
            "_method": "PUT",
            "profile_picture": "123",
            "description": "This is a test",
            "country": "FI",
            'birthday': "2000-01-10"
        }, format="multipart")
        self.assertEqual(resp.status_code, 400)
        # check success
        resp = self.client.post(path, {
            "_method": "PUT",
            "profile_picture": uploaded,
            "description": "This is a test",
            "country": "FI",
            'birthday': "2000-01-10"
        }, format="multipart")
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        profile_pic_1 = json_resp.get('profile_picture')
        self.assertRegexpMatches(
            profile_pic_1, 'img/profile_picture/3-.+.png'
        )
        # uploading a new profile pic
        img = open(settings.STATICFILES_DIRS[0]+"/img/banana.png", "rb")
        uploaded = SimpleUploadedFile(img.name, img.read())
        time.sleep(1)
        resp = self.client.post(path, {
            "_method": "PUT",
            "profile_picture": uploaded
        }, format="multipart")
        self.assertEqual(resp.status_code, 200)
        # check if old file has been deleted
        self.assertFalse(os.path.exists(
            os.path.join(settings.MEDIA_ROOT, profile_pic_1)
        ))
        # new file name should be different to old one
        json_resp = json.loads(resp.content)
        self.assertNotEqual(json_resp.get('profile_picture'), profile_pic_1)
        os.remove(
            os.path.join(settings.MEDIA_ROOT, json_resp.get('profile_picture'))
        )

    def test_api_update_user_email(self):
        path = reverse('api_v1_3:whamuser-detail', kwargs={'pk': 3})
        # check success
        resp = self.client.post(path, {
            "_method": "PUT",
            "email": "test@test.com",
        })
        self.assertEqual(resp.status_code, 200)
        # check if activation email is sent
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(
            mail.outbox[0].subject, 'Welcome to Whambush!'
        )
        # status should be NEW
        self.assertEquals(WhamUser.objects.get(pk=3).activation_state, 'NEW')
        self.assertEquals(WhamUser.objects.get(pk=3).email, 'test@test.com')

    def test_model_default_values(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_default_value',
            'email': 'default@test.com',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('rank'), None)
        self.assertEqual(json_resp.get('birthday'), None)
        self.assertEqual(json_resp.get('country'), 'FI')
        self.assertEqual(json_resp.get('profile_picture'), '')
        self.assertEqual(json_resp.get('description'), None)
        self.assertEqual(json_resp.get('user_type'), 0)

    def test_search_user(self):
        path = reverse('api_v2_0:search-user') + '?query=testuser'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 0)
        path = reverse('api_v2_0:search-user') + '?query=test_user'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 3)

    def test_search_user_by_fields(self):
        # not allowed field
        path = reverse('api_v2_0:search-user') + '?query=user&fields=password'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 0)
        # allowed field
        path = reverse('api_v2_0:search-user') + '?query=user&fields=email'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 0)

        path = (reverse('api_v2_0:search-user') +
                '?query=test1@test.com&fields=email')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        self.assertEqual(
            json_resp.get('results')[0].get('email'),
            'test1@test.com'
        )
        # 2 fields
        path = (reverse('api_v2_0:search-user') +
                '?query=test_user&fields=username,fisrt_name')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        self.assertEqual(
            json_resp.get('results')[0].get('username'),
            'test_user'
        )

    def test_list_all_users(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.get(path)
        # current user is admin so this must be 200
        self.assertEqual(resp.status_code, 200)

        # login as a normal user
        resp = login(self.client, 'test_user', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

        # try again
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.get(path)
        # must not have permission to see
        self.assertEqual(resp.status_code, 403)


class ApiAccountsRegisterTestcases(TestCase):
    fixtures = ['fixture_api_v1_3.json']

    def setUp(self):
        # set beta to false
        config.IS_BETA = False

    def test_set_wrong_token_and_register_or_login(self):
        set_token(self.client, 'blahblah')
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_username',
            'email': 'username@test.com',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 403)
        resp = self.client.post(reverse('api_v1_3:token-auth'), {
            'username': 'test_user_1',
            'password': 'wrong',
        })
        self.assertEqual(resp.status_code, 403)

    def test_api_register_GET_method(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 403)

    def test_api_register_missing_params(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_username',
            'password1': 'test_password',
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('email'), ['This field is required.']
        )
        self.assertEqual(
            json_resp.get('password2'), ['This field is required.']
        )

    def test_api_register_existing_user(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_user_1',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('username'),
            ['User with this Username already exists.']
        )

        # should be sensitive as well
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'Test_user_1',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('username'),
            ['User with this Username already exists.']
        )

    def test_api_register_non_valid_email(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_user_3',
            'email': 'test_user_3',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('email'),
            ['Enter a valid email address.']
        )

    def test_api_register_existing_email(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_user_3',
            'email': 'test1@example.com',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('email'),
            ['User with this Email address already exists.']
        )

    def test_api_register_success(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_username',
            'email': 'username@test.com',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('username'), 'test_username')

    def test_api_register_return_active_user(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_username',
            'email': 'username@test.com',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        user = WhamUser.objects.get(pk=json_resp.get('id'))
        self.assertTrue(user.is_active)

    def test_register_in_beta(self):
        path = reverse('api_v1_3:list-create-user')
        # test register in beta
        config.IS_BETA = True
        post_data = {
            'username': 'test_register_beta',
            'email': 'test_register_beta@tester.com',
            'password1': 'test_register_beta',
            'password2': 'test_register_beta',
        }
        # without promocode
        resp = self.client.post(path, post_data)
        self.assertEqual(resp.status_code, 400)
        # check success
        post_data.update({
            'promocode': '123456'
        })
        resp = self.client.post(path, post_data)
        self.assertEqual(resp.status_code, 201)
        # check key updated
        key = PromotionKey.objects.get(key="123456")
        self.assertEqual(key.is_used, True)

    def test_register_promocode_invalid(self):
        path = reverse('api_v1_3:list-create-user')
        config.IS_BETA = True
        resp = self.client.post(path, {
            'username': 'test_register_beta',
            'email': 'test_register_beta@tester.com',
            'password1': 'test_register_beta',
            'password2': 'test_register_beta',
            'promocode': '123'
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('promocode'), 'Invalid code.')

    def test_register_promocode_used(self):
        path = reverse('api_v1_3:list-create-user')
        config.IS_BETA = True
        resp = self.client.post(path, {
            'username': 'test_register_beta',
            'email': 'test_register_beta@tester.com',
            'password1': 'test_register_beta',
            'password2': 'test_register_beta',
            'promocode': '654321'
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('promocode'), 'Code is already used.')

    def test_register_api_partner_user(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': 'test_register_api_partner_user',
            'email': 'test_register_api_partner_user@tester.com',
            'password1': '123456',
            'password2': '123456',
            'user_type': 1
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('user_type'), 1)

    def test_api_register_reserved_username(self):
        path = reverse('api_v1_3:list-create-user')
        resp = self.client.post(path, {
            'username': settings.RESERVED_USERNAME[0],
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('username'),
            ['REGISTER_ERROR_RESERVED_USERNAME']
        )
