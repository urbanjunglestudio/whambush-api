from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import (
    login, set_token
)

import json


class ApiBananaTransactionTestCase(TestCase):
    fixtures = ['fixture_api_v1_3.json']

    def setUp(self):
        resp = login(self.client, 'test_user', 'test_user_1')
        json_resp = json.loads(resp.content)
        self.user_url = json_resp.get('user').get('url')
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_get_empty_banana_transaction(self):
        path = reverse('api_v1_3:list_bananas_transaction')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 0)

    def test_validation_add_banana_to_user(self):
        path = reverse('api_v1_3:list_bananas_transaction')
        resp = self.client.post(path, {
            'user': -1,
            'amount': 10
        })
        self.assertEqual(resp.status_code, 400)
        resp = self.client.post(path, {
            'user': 2,
            'amount': 'hello'
        })
        self.assertEqual(resp.status_code, 400)

    def test_result_add_banana_to_user(self):
        path = reverse('api_v1_3:list_bananas_transaction')
        # add banana successful
        resp = self.client.post(path, {
            'user': 1,
            'amount': 10
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('amount'), 10)
        self.assertEqual(json_resp.get('user'), self.user_url)
        # get banana transaction successful
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        self.assertEqual(
            json_resp.get('results')[0].get('user'), self.user_url
        )
        # get correct userscore
        path = reverse('api_v1_3:whamuser-detail', kwargs={'pk': 1})
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('userscore'), 11)

        # then add some more banana
        resp = self.client.post(reverse('api_v1_3:list_bananas_transaction'), {
            'user': 1,
            'amount': 20
        })
        self.assertEqual(resp.status_code, 201)
        # check userscore again
        resp = self.client.get(
            reverse('api_v1_3:whamuser-detail', kwargs={'pk': 1})
        )
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('userscore'), 31)
