from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import login, set_token
from whambush.apps.missions.models import Mission
from whambush.apps.accounts.models import WhamUser

import json
from datetime import datetime, timedelta


class ApiMissionsViewTestcases(TestCase):
    fixtures = ['fixture_api_v1_3.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

        # create 2 missions for testing
        Mission.objects.create(
            id=1,
            name='Test active mission',
            start_at=datetime.now() - timedelta(days=1),  # 1 day before
            end_at=datetime.now() + timedelta(days=2),  # 2 days after
            added_by=WhamUser.objects.get(pk=1),
            description=''
        )
        Mission.objects.create(
            id=2,
            name='Test inactive mission',
            start_at=datetime.strptime('2012-01-01', '%Y-%m-%d'),
            end_at=datetime.strptime('2012-01-10', '%Y-%m-%d'),
            added_by=WhamUser.objects.get(pk=1),
            description=''
        )

    def test_api_mission_detail_return_default_values(self):
        path = reverse('api_v1_3:mission-detail', kwargs={'pk': 2})
        self.assertEqual(path, '/v1.3/missions/2/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('id'), 2)
        self.assertEqual(json_resp.get('start_at'), "2012-01-01T00:00:00")
        self.assertEqual(json_resp.get('end_at'), "2012-01-10T00:00:00")
        self.assertFalse(json_resp.get('is_videos_ranked'))
        self.assertFalse(json_resp.get('has_submitted'))

    def test_api_mission_list_active(self):
        resp = self.client.get(reverse('api_v1_3:list-missions'))
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        result = json_resp.get('results')[0]
        self.assertEqual(json_resp.get('count'), 1)
        self.assertTrue('has_submitted' in result)

    def test_api_mission_list_old(self):
        resp = self.client.get(reverse('api_v1_3:list-missions') + '?type=old')
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 3)
        # end time of the missions should be before now
        end_at = json_resp.get('results')[0]['end_at']
        end_at = datetime.strptime(end_at, "%Y-%m-%dT%H:%M:%S")
        self.assertEqual(end_at < datetime.now(), True)
        # order should be latest first
        self.assertEqual(json_resp.get('results')[0].get('id'), 4)
        self.assertEqual(json_resp.get('results')[1].get('id'), 3)
        self.assertTrue('has_submitted' in json_resp.get('results')[0])

    def test_api_mission_detail_has_submitted(self):
        path = reverse('api_v1_3:list-create-video')
        resp = self.client.post(path, {
            'name': 'test submit video',
            'description': '',
            'external_id': 123,
            'mission_id': 1
        })
        self.assertEqual(resp.status_code, 201)
        path = reverse('api_v1_3:mission-detail', kwargs={'pk': 1})
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(json_resp.get('has_submitted'))
