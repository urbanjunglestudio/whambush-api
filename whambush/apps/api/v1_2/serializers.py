from django.conf import settings
from django.utils import timezone
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.contrib.comments.models import Comment
from django.contrib.auth.models import AnonymousUser
from django.contrib.auth import authenticate
from rest_framework import serializers
from rest_framework.reverse import reverse

from whambush.apps.accounts.models import WhamUser, UserScore, Follow
from whambush.apps.accounts.services import create_and_send_activation_key
from whambush.apps.videos.models import Video, Flag, VideoFeed
from whambush.apps.likes.models import Like
from whambush.apps.missions.models import Mission
from whambush.apps.hashtags.models import Hashtag

from push_notifications.models import APNSDevice, GCMDevice


class UserSerializer(serializers.HyperlinkedModelSerializer):
    _default_view_name = 'api_v1_2:whamuser-detail'
    is_guest = serializers.Field(source='is_guest')
    activation_state = serializers.Field(source='activation_state')

    class Meta:
        model = WhamUser
        fields = (
            'id',
            'username',
            'email',
            'url',
            'profile_picture',
            'rank',
            'description',
            'birthday',
            'country',
            'user_type',
            'activation_state',
            'date_joined',
            'is_guest'
        )

    def to_native(self, obj):
        request = self.context['request']
        data = super(UserSerializer, self).to_native(obj)
        if isinstance(obj, AnonymousUser):
            return data
        elif obj is not None:
            try:
                data['userscore'] = obj.userscore.score
            except UserScore.DoesNotExist:
                data['userscore'] = 0
            data['num_videos'] = Video.objects.filter(
                added_by=obj
            ).active().count()
            data['num_followers'] = Follow.objects.filter(to_user=obj).count()
            data['num_followings'] = (
                Follow.objects.filter(from_user=obj).count()
            )
            data['num_likes'] = Like.objects.filter(
                point=1,
                video__in=(
                    Video.objects.filter(added_by=obj)
                    .values_list('id', flat=True)
                )
            ).count()
            data['num_dislikes'] = Like.objects.filter(
                point=-1,
                video__in=(
                    Video.objects.filter(added_by=obj)
                    .values_list('id', flat=True)
                )
            ).count()
            if isinstance(request.user, AnonymousUser):
                is_following = False
            else:
                is_following = Follow.objects.filter(
                    from_user=request.user, to_user=obj
                ).count() == 1
            data['is_following'] = (
                is_following if request.user != obj else True
            )

        return data


class WhamAuthTokenSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def validate(self, attrs):
        username = attrs.get('username')
        password = attrs.get('password')

        if username and password:
            user = authenticate(username=username, password=password)

            if user:
                active, status = user.activation_status
                if not active:
                    if status == WhamUser.STATUS_INACTIVE:
                        raise serializers.ValidationError(
                            'LOGIN_ERROR_INACTIVE_USER'
                        )
                    elif status == WhamUser.STATUS_DEACTIVATED:
                        raise serializers.ValidationError(
                            'LOGIN_ERROR_BANNED_USER'
                        )
                attrs['user'] = user
                return attrs
            else:
                raise serializers.ValidationError(
                    'LOGIN_ERROR_WRONG_CREDENTIALS'
                )
        else:
            raise serializers.ValidationError(
                'LOGIN_ERROR_MISSING_PARAMS'
            )


# TODO: remove this serializer, and use user serializer instead
class UserUpdateSerializer(serializers.ModelSerializer):
    new_password = serializers.CharField(
        max_length=128,
        min_length=4,
        required=False
    )
    new_password_value = None
    new_email_value = None
    country = serializers.CharField(max_length=2, required=False)

    class Meta:
        model = WhamUser
        fields = (
            'email',
            'new_password',
            'description',
            'birthday',
            'country'
        )

    def validate_email(self, attrs, source):
        if source in attrs:
            value = attrs[source]
            if value == '':
                raise serializers.ValidationError(
                    'Email can not be empty.')
        return attrs

    def validate_new_password(self, attrs, source):
        if source in attrs:
            value = attrs[source]
            if value == '':
                raise serializers.ValidationError(
                    'New password can not be empty.')
        return attrs

    def validate_country(self, attrs, source):
        if source in attrs:
            value = attrs[source]
            if value not in dict(settings.COUNTRY_CHOICES):
                raise serializers.ValidationError(
                    '{0} is not a valid choice.'.format(value)
                )
        return attrs

    def to_native(self, obj):
        if 'new_password' in self.fields:
            del self.fields['new_password']
        return super(UserUpdateSerializer, self).to_native(obj)

    def restore_object(self, attrs, instance=None):
        if 'new_password' in attrs:
            self.new_password_value = attrs['new_password']
            del attrs['new_password']
        if 'email' in attrs:
            self.new_email_value = attrs['email']
            del attrs['email']
        obj = super(UserUpdateSerializer, self).restore_object(attrs, instance)
        if self.new_email_value:
            if self.new_email_value != obj.email:
                obj.email = self.new_email_value
            else:
                self.new_email_value = None
        return obj

    def save_object(self, obj, **kwargs):
        if self.new_password_value:
            obj.set_password(self.new_password_value)
        super(UserUpdateSerializer, self).save_object(obj, **kwargs)
        # send validation email
        if self.new_email_value:
            create_and_send_activation_key(obj)


class HashtagSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Hashtag


class VideoSerializer(serializers.HyperlinkedModelSerializer):
    mission_id = serializers.IntegerField(required=False)
    external_url = serializers.Field(source='external_url')
    thumbnail_url = serializers.Field(source='thumbnail_url')
    web_url = serializers.Field(source='web_url')

    class Meta:
        model = Video
        fields = (
            'id',
            'name',
            'description',
            'external_id',
            'is_processed',
            'created_at',
            'modified_at',
            'view_count',
            'rank',
            'video_type',
            'is_active',
            'mission_id',
            'external_url',
            'thumbnail_url',
            'web_url',
            'published_at',
            'is_mission_winner',
        )

    def to_native(self, obj):
        request = self.context.get('request')
        data = super(VideoSerializer, self).to_native(obj)
        if obj is not None:
            data['added_by'] = UserSerializer(
                obj.added_by,
                context=self.context
            ).data
            data['comment_count'] = Comment.objects.filter(
                content_type=ContentType.objects.get_for_model(obj),
                object_pk=obj.id,
                is_removed=False
            ).count()
            data['like_count'] = Like.objects.filter(
                video=obj, point=1
            ).count()
            data['dislike_count'] = Like.objects.filter(
                video=obj, point=-1
            ).count()
            data['tags'] = ','.join([t.name for t in obj.tags.all()])
            data['has_liked'] = Like.objects.filter(
                video=obj, user=request.user, point=1
            ).count() > 0
            data['has_disliked'] = Like.objects.filter(
                video=obj, user=request.user, point=-1
            ).count() > 0

        return data


class MissionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Mission
        fields = (
            'id',
            'name',
            'description',
            'start_at',
            'end_at',
            'mission_type',
            'is_videos_ranked',
            'created_at',
            'modified_at',
        )

    def to_native(self, obj):
        request = self.context.get('request')
        data = super(MissionSerializer, self).to_native(obj)
        if obj is not None:
            data['added_by'] = UserSerializer(
                obj.added_by,
                context=self.context
            ).data
            data['has_submitted'] = Video.objects.filter(
                is_active=True,
                mission=obj,
                added_by=request.user
            ).count() > 0
            if obj.linked_video:
                data['linked_video'] = VideoSerializer(
                    obj.linked_video,
                    context=self.context
                ).data
            else:
                data['linked_video'] = None

        return data


class LikesSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HyperlinkedRelatedField(
        view_name='api_v1_2:whamuser-detail'
    )
    video = serializers.HyperlinkedRelatedField(
        view_name='api_v1_2:video-detail'
    )

    class Meta:
        model = Like
        fields = (
            'id',
            'user',
            'video',
            'created_at',
            'modified_at',
        )


# TODO: need improvement, and subclass HyperlinkedModelSerializer?
class CommentVideoSerializer(serializers.ModelSerializer):
    video_id = serializers.IntegerField()
    post_date = serializers.DateTimeField(required=False)

    class Meta:
        model = Comment
        fields = (
            'id',
            'video_id',
            'user',
            'comment',
            'post_date'
        )
        view_name = 'api_v1_2:video-comment-detail'

    def validate_video_id(self, attrs, source):
        try:
            Video.objects.get(pk=attrs[source])
        except Video.DoesNotExist:
            raise serializers.ValidationError('Video does not exits')
        return attrs

    def restore_object(self, attrs, instance=None):
        video = Video.objects.get(pk=attrs['video_id'])
        attrs['object_pk'] = video.id
        attrs['content_type'] = ContentType.objects.get_for_model(video)
        del attrs['video_id']
        attrs['site'] = Site.objects.get_current()
        attrs['submit_date'] = timezone.now()
        if 'post_date' in attrs:
            if attrs['post_date'] is not None:
                attrs['submit_date'] = attrs['post_date']
            del attrs['post_date']

        return super(CommentVideoSerializer, self).restore_object(
            attrs, instance
        )

    def to_native(self, obj):
        if 'video_id' in self.fields:
            del self.fields['video_id']
        if 'post_date' in self.fields:
            del self.fields['post_date']
        data = super(CommentVideoSerializer, self).to_native(obj)
        if obj is not None:
            data['video_id'] = obj.object_pk
            data['post_date'] = obj.submit_date
            data['user'] = UserSerializer(
                obj.user,
                context=self.context
            ).data

        return data


class FlagsSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HyperlinkedRelatedField(
        view_name='api_v1_2:whamuser-detail'
    )
    video = serializers.IntegerField(
        source='object_pk'
    )

    class Meta:
        model = Flag
        fields = (
            'id',
            'user',
            'video',
            'created_at',
            'modified_at',
            'reason_code'
        )

    def to_native(self, obj):
        data = super(FlagsSerializer, self).to_native(obj)
        if obj is not None:
            data['video'] = reverse(
                'api_v1_2:video-detail', kwargs={'pk': obj.object_pk},
                request=self.context.get('request', None)
            )
        return data


class VideoFeedSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = VideoFeed
        fields = (
            'id',
            'name',
            'icon_url',
            'default_icon',
            'endpoint_url',
            'order',
        )


class FollowSerializer(serializers.HyperlinkedModelSerializer):
    from_user = serializers.HyperlinkedRelatedField(
        view_name='api_v1_2:whamuser-detail'
    )
    to_user = serializers.HyperlinkedRelatedField(
        view_name='api_v1_2:whamuser-detail'
    )

    class Meta:
        model = Follow
        fields = (
            'id',
            'from_user',
            'to_user'
        )

    def to_native(self, obj):
        if 'from_user' in self.fields:
            del self.fields['from_user']
        return super(FollowSerializer, self).to_native(obj)


class APNSDeviceSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HyperlinkedRelatedField(
        view_name='api_v1_2:whamuser-detail'
    )

    class Meta:
        model = APNSDevice
        fields = (
            'id',
            'user',
            'registration_id',
        )


class GCMDeviceSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.HyperlinkedRelatedField(
        view_name='api_v1_2:whamuser-detail'
    )

    class Meta:
        model = GCMDevice
        fields = (
            'id',
            'user',
            'registration_id',
        )
