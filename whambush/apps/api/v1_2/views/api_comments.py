from django.contrib.contenttypes.models import ContentType
from django.contrib.comments.models import Comment
from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import permission_classes

from ..serializers import CommentVideoSerializer
from ..services import delete_comment
from whambush.apps.videos.models import Video

from whambush.libs.utils import clone_REST_request


@permission_classes((IsAuthenticated, ))
class ListCreateVideoComment(generics.ListCreateAPIView):
    model = Comment
    serializer_class = CommentVideoSerializer

    def get_queryset(self):
        '''
        6.3 List comment
        '''
        video_id = self.kwargs.pop('video_id')
        try:
            video = Video.objects.get(pk=video_id)
        except Video.DoesNotExist:
            return Comment.objects.filter(content_type=0, object_pk=0)
        return Comment.objects.filter(
            content_type=ContentType.objects.get_for_model(video),
            object_pk=video.id
        ).order_by('submit_date').reverse()

    def post(self, request, *args, **kwargs):
        '''
        6.1 Create comment
        '''
        video_id = kwargs.get('video_id')
        req = clone_REST_request(request)
        req._data.update({
            'video_id': video_id,
            'user': request.user.id
        })
        return super(ListCreateVideoComment, self).post(req, args, kwargs)


@permission_classes((IsAuthenticated, ))
class VideoCommentDetail(generics.RetrieveDestroyAPIView):
    '''
    6.2 Delete comment
    '''
    model = Comment
    serializer_class = CommentVideoSerializer

    def delete(self, request, *args, **kwargs):
        comment = self.get_object()
        if delete_comment(comment, request.user):
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(
                {'detail': "User does not have permission."},
                status=status.HTTP_400_BAD_REQUEST
            )
