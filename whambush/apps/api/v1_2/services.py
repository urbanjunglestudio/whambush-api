from django.http import Http404
from rest_framework import status
from rest_framework.response import Response

from whambush.apps.accounts.models import WhamUser
from whambush.apps.likes.models import Like
from whambush.apps.videos.models import Video
from whambush.apps.promotions.models import PromotionKey

import warnings


def response_invalid_args():
    return Response(
        {'success': False, 'detail': 'Invalid arguments'},
        status=status.HTTP_400_BAD_REQUEST
    )


def user_likes_video(user, video_id, point=1):
    try:
        like = Like.objects.get(user=user, video=video_id)
        if like.point == point*-1:
            like.delete()
            raise Like.DoesNotExist()
    except Like.DoesNotExist:
        try:
            video = Video.objects.get(pk=video_id)
        except Video.DoesNotExist:
            return False
        Like.objects.create(user=user, video=video, point=point)

    return get_likes_on_video(video_id)


def user_unlikes_video(user, video_id, point=1):
    try:
        like = Like.objects.get(user=user, video=video_id, point=point)
    except Like.DoesNotExist:
        return get_likes_on_video(video_id)
    like.delete()

    return get_likes_on_video(video_id)


def get_likes_on_video(video_id):
    if video_id is None or not video_id.isdigit():
        return response_invalid_args()
    likes = Like.objects.filter(video=video_id, point=1).count()
    dislikes = Like.objects.filter(video=video_id, point=-1).count()

    return [likes, dislikes]


def user_owns(user, obj):
    if isinstance(user, WhamUser):
        if obj.user is not None:
            return obj.user == user
        else:
            return True
    else:
        return False


def delete_comment(comment, user):
    video = comment.content_object
    if user_owns(user, comment) or user_owns(user, video):
        comment.is_removed = True
        comment.save()
        return True
    else:
        return False


def get_serializer_listview(cls):
    cls.object_list = cls.filter_queryset(cls.get_queryset())

    # Default is to allow empty querysets.  This can be altered by setting
    # `.allow_empty = False`, to raise 404 errors on empty querysets.
    if not cls.allow_empty and not cls.object_list:
        warnings.warn(
            'The `allow_empty` parameter is due to be deprecated. '
            'To use `allow_empty=False` style behavior, You should override '
            '`get_queryset()` and explicitly raise a 404 on empty querysets.',
            PendingDeprecationWarning
        )
        class_name = cls.__class__.__name__
        error_msg = cls.empty_error % {'class_name': class_name}
        raise Http404(error_msg)

    # Switch between paginated or standard style responses
    page = cls.paginate_queryset(cls.object_list)
    if page is not None:
        return cls.get_pagination_serializer(page)
    else:
        return cls.get_serializer(cls.object_list, many=True)


def verify_promo_code(code):
    if not code:
        return (
            False,
            {'promocode': 'Promocode is required.'},
        )
    try:
        key = PromotionKey.objects.get(key=code)
    except PromotionKey.DoesNotExist:
        return (False, {'promocode': 'Invalid code.'})
    else:
        if key.is_used:
            return (
                False, {'promocode': 'Code is already used.'}
            )
        else:
            key.is_used = True
            key.save()
            return (True, None)
