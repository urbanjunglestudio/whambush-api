from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils.timezone import now as dj_now

from .services import (
    login, set_token, reverse_like_video, reverse_dislike_video,
    reverse_unlike_video, reverse_undislike_video
)
from whambush.apps.likes.models import Like

import json


class ApiLikesViewTestcases(TestCase):
    fixtures = ['fixture_api_v1_2.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def _like_video(self, video_id):
        path = reverse_like_video()
        return self.client.post(path, {
            'video_id': video_id,
        })

    def _unlike_video(self, video_id):
        path = reverse_unlike_video()
        return self.client.post(path, {
            'video_id': video_id
        })

    def _dislike_video(self, video_id):
        path = reverse_dislike_video()
        return self.client.post(path, {
            'video_id': video_id,
        })

    def _undislike_video(self, video_id):
        path = reverse_undislike_video()
        return self.client.post(path, {
            'video_id': video_id
        })

    def test_api_like_video_invalid_id(self):
        resp = self._like_video('ee1')
        self.assertEqual(resp.status_code, 400)

    def test_api_like_video_not_exist(self):
        resp = self._like_video(100)
        self.assertEqual(resp.status_code, 400)

    def test_api_like_video_success(self):
        resp = self._like_video(1)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        # number of likes on this video should be 1
        likes = json_resp.get('likes')
        self.assertEqual(likes, 1)

    def test_api_dislike_video(self):
        resp = self._dislike_video(1)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        # number of dislikes on this video should be 1
        dislikes = json_resp.get('dislikes')
        self.assertEqual(dislikes, 1)

    def test_api_like_then_dislike_video(self):
        resp = self._like_video(1)
        self.assertEqual(resp.status_code, 200)
        resp = self._dislike_video(1)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        # number of dislikes on this video should be 1
        dislikes = json_resp.get('dislikes')
        self.assertEqual(dislikes, 1)

    def test_api_dislike_then_like_video(self):
        resp = self._dislike_video(1)
        self.assertEqual(resp.status_code, 200)
        resp = self._like_video(1)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        # number of likes on this video should be 1
        likes = json_resp.get('likes')
        self.assertEqual(likes, 1)

    def test_api_unlike_video_invalid_id(self):
        resp = self._unlike_video('ee1')
        self.assertEqual(resp.status_code, 400)

    def test_api_unlike_video_success(self):
        resp = self._like_video(1)
        json_resp = json.loads(resp.content)
        likes = json_resp.get('likes')
        self.assertEqual(likes, 1)
        resp = self._unlike_video(1)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        likes = json_resp.get('likes')
        self.assertEqual(likes, 0)

    def test_api_undislike_video_success(self):
        resp = self._dislike_video(1)
        json_resp = json.loads(resp.content)
        dislikes = json_resp.get('dislikes')
        self.assertEqual(dislikes, 1)
        resp = self._undislike_video(1)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        dislikes = json_resp.get('dislikes')
        self.assertEqual(dislikes, 0)

    def test_api_get_likes_video(self):
        path = reverse('api_v1_2:get-likes', args=[1])
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)

    def test_model_default_values(self):
        now = dj_now().replace(minute=0, second=0, microsecond=0)
        self.client.post(reverse_like_video(), {
            'video_id': '1',
        })
        created_at = Like.objects.latest(field_name='id').created_at
        created_at = created_at.replace(minute=0, second=0, microsecond=0)
        self.assertEqual(now, created_at)
