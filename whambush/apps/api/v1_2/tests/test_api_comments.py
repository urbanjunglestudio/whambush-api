from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils.timezone import now as dj_now

from .services import login, set_token, reverse_create_comment
from whambush.apps.accounts.models import WhamUser
from whambush.libs.utils import db_strptime

import json


class ApiCommentsViewTestcases(TestCase):
    fixtures = ['fixture_api_v1_2.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def _create_comment(self, video_id, comment):
        path = reverse_create_comment(video_id)
        resp = self.client.post(path, {
            'comment': comment
        })
        self.assertEqual(resp.status_code, 201)
        return json.loads(resp.content)

    def test_api_create_comment(self):
        # create comment
        json_resp = self._create_comment(1, 'test comment 1')
        self.assertEqual(
            json_resp.get('user')['url'],
            'http://testserver' + reverse(
                'api_v1_2:whamuser-detail', kwargs={'pk': 3}
            )
        )
        # verify username in comment details
        self.assertEqual(json_resp.get('user')['username'], 'test_user_1')

    def test_api_list_comment(self):
        self._create_comment(1, 'test comment 1')
        second_comment = self._create_comment(1, 'test comment 2')
        second_comment_id = second_comment.get('id')
        resp = self.client.get(reverse_create_comment(1))
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)
        # verify the order is latest first
        self.assertEqual(json_resp['results'][0]['id'], second_comment_id)

    def test_api_delete_comment(self):
        first_comment_id = self._create_comment(1, 'first comment').get('id')
        # delete comment
        path = reverse(
            'api_v1_2:video-comment-detail', kwargs={'pk': first_comment_id}
        )
        resp = self.client.delete(path)
        self.assertEqual(resp.status_code, 204)

    def test_api_create_comment_with_form_header(self):
        path = reverse_create_comment(1)
        resp = self.client.post(
            path,
            'comment=test%20comment%20with%20form%20header',
            content_type='application/x-www-form-urlencoded'
        )
        self.assertEqual(resp.status_code, 201)

        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('user')['url'],
            'http://testserver' + reverse(
                'api_v1_2:whamuser-detail', kwargs={'pk': 3}
            )
        )

    def test_api_user_comment_no_profile_picture(self):
        json_resp = self._create_comment(1, 'test comment 1')
        self.assertEqual(json_resp.get('user')['profile_picture'], '')

    def test_api_user_comment_with_profile_picture(self):
        WhamUser.objects.filter(username='test_user_1').update(
            profile_picture='img/profile.jpg'
        )
        json_resp = self._create_comment(1, 'test comment 1')
        self.assertEqual(
            json_resp.get('user')['profile_picture'],
            'img/profile.jpg'
        )

    def test_model_default_values(self):
        now = dj_now()
        path = reverse_create_comment(1)
        resp = self.client.post(path, {
            'comment': 'test comment default'
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        created_at = db_strptime(json_resp.get('post_date'))
        self.assertEqual(created_at.tm_year, now.year)
        self.assertEqual(created_at.tm_mon, now.month)
        self.assertEqual(created_at.tm_mday, now.day)
        self.assertEqual(created_at.tm_hour, now.hour)
