# -*- coding: utf-8 -*-
from django.test import TestCase
from django.core.urlresolvers import reverse
from django.conf import settings
from django.utils.timezone import now as dj_now
from django.core import mail

from .services import (
    login, set_token, reverse_like_video, reverse_dislike_video,
    reverse_list_videos
)
from whambush.apps.accounts.models import Follow, WhamUser
from whambush.apps.videos.models import Video
from whambush.libs.utils import db_strptime

from constance import config
import json
import os


class ApiVideosViewTestcases(TestCase):
    fixtures = ['fixture_api_v1_2.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_get_video_detail_success(self):
        path = reverse('api_v1_2:video-detail', kwargs={'pk': 1})
        self.assertEqual(path, '/v1.2/videos/1/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('id'), 1)
        self.assertEqual(json_resp.get('view_count'), 10)
        self.assertEqual(json_resp.get('rank'), 2)
        self.assertFalse(json_resp.get('has_liked'))
        self.assertFalse(json_resp.get('has_disliked'))
        self.assertEqual(
            json_resp.get('external_url'),
            'https://view.vzaar.com/1372557/video'
        )

    def test_api_video_detail_has_liked_and_disliked(self):
        path = reverse('api_v1_2:video-detail', kwargs={'pk': 1})
        # like
        self.client.post(reverse_like_video(), {'video_id': 1})
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertTrue(json_resp.get('has_liked'))
        self.assertFalse(json_resp.get('has_disliked'))
        # dislike
        self.client.post(reverse_dislike_video(), {'video_id': 1})
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertFalse(json_resp.get('has_liked'))
        self.assertTrue(json_resp.get('has_disliked'))

    def test_api_video_detail_like_count(self):
        path = reverse('api_v1_2:video-detail', kwargs={'pk': 4})
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('like_count'), 2)
        self.assertEqual(json_resp.get('dislike_count'), 1)
        self.assertTrue(json_resp.get('has_liked'))
        self.assertFalse(json_resp.get('has_disliked'))

    def test_api_get_video_detail_not_exist(self):
        path = reverse('api_v1_2:video-detail', kwargs={'pk': '100'})
        self.assertEqual(path, '/v1.2/videos/100/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 404)

        path = reverse('api_v1_2:video-detail', kwargs={'pk': '0'})
        self.assertEqual(path, '/v1.2/videos/0/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 404)

    def test_api_latest_video(self):
        self.client.post(reverse_dislike_video(), {'video_id': 2})
        resp = self.client.get(reverse_list_videos())
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 4)

        # test has_liked and has_disliked in list API
        # has_liked should be true
        video = json_resp.get('results')[0]
        self.assertEqual(video.get('id'), 4)
        self.assertTrue(video.get('has_liked'))
        self.assertFalse(video.get('has_disliked'))
        # has_disliked should be true
        video = json_resp.get('results')[1]
        self.assertEqual(video.get('id'), 2)
        self.assertFalse(video.get('has_liked'))
        self.assertTrue(video.get('has_disliked'))

    def test_api_create_video_missing_param(self):
        path = reverse('api_v1_2:list-create-video')
        resp = self.client.post(path)
        self.assertEqual(resp.status_code, 400)

    def test_api_my_video(self):
        resp = self.client.get(reverse_list_videos('my'))
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)
        self.assertEqual(resp.status_code, 200)

    def test_api_tv_video(self):
        resp = self.client.get(reverse_list_videos('tv'))
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        self.assertEqual(resp.status_code, 200)

    def test_api_flag_video(self):
        path = reverse(
            'api_v1_2:list-create-flag-video', kwargs={'video_id': '2'}
        )
        # create flag
        resp = self.client.post(path, {'reason_code': 1})
        self.assertEqual(resp.status_code, 201)
        # recreate flag
        self.client.post(path, {'reason_code': 2})
        # then get the flag of current user and this video
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        # the first flag should be deleted
        self.assertEqual(json_resp.get('count'), 1)
        flag = json_resp.get('results')[0]
        self.assertEqual(flag['reason_code'], 2)

    def test_api_flag_video_trigger_send_email(self):
        config.FLAG_LIMIT = '1'
        self.client.post(reverse(
            'api_v1_2:list-create-flag-video', kwargs={'video_id': '2'}
        ), {'reason_code': 1})
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(
            mail.outbox[0].subject, '[ALERT] Video gets many flagged'
        )

    def test_api_video_flag_detail(self):
        path = reverse('api_v1_2:flag-detail', kwargs={'pk': '1'})
        # get the flag's details
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        # delete
        resp = self.client.delete(path)
        self.assertEqual(resp.status_code, 204)

    def test_api_video_feed(self):
        resp = self.client.get(reverse('api_v1_2:list-video-feed'))
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)
        self.assertEqual(json_resp.get('results')[0].get('id'), 2)
        self.assertEqual(json_resp.get('default'), 3)

    def test_api_get_featured_video(self):
        resp = self.client.get(reverse_list_videos('featured'))
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)

    def test_api_get_my_following_video(self):
        resp = self.client.get(reverse_list_videos('following'))
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        # results should be from the users whom current user is following
        user = WhamUser.objects.get(username='test_user_1')
        following_users = Follow.objects.filter(
            from_user=user
        ).values_list('to_user__username', flat=True)
        self.assertIn(
            json_resp.get('results')[0]['added_by']['username'],
            following_users
        )
        video = json_resp.get('results')[0]
        self.assertTrue('has_liked' in video)
        self.assertTrue('has_disliked' in video)

    def test_api_delete_video(self):
        path = reverse('api_v1_2:video-detail', kwargs={'pk': 1})
        resp = self.client.delete(path)
        self.assertEqual(resp.status_code, 204)
        # no video was actually deleted
        self.assertEqual(Video.objects.count(), 5)
        # but in my video list, 1 should disappear
        resp = self.client.get(reverse_list_videos('my'))
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)

        # should not have permission to remove video added by other user
        path = reverse('api_v1_2:video-detail', kwargs={'pk': 4})
        resp = self.client.delete(path)
        self.assertEqual(resp.status_code, 403)
        self.assertEqual(Video.objects.count(), 5)


class ApiSearchVideoTestcases(TestCase):
    fixtures = ['fixture_api_v1_2.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_search_video_by_user(self):
        path = reverse('api_v1_2:search-video') + '?user=1,2'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)
        # results should be from user id 1 or 2
        user_url = u'http://testserver' + reverse(
            'api_v1_2:whamuser-detail', kwargs={'pk': 1}
        )
        self.assertEqual(
            json_resp.get('results')[0]['added_by']['url'], user_url
        )

    def test_api_search_video_invalid_user_id(self):
        path = reverse('api_v1_2:search-video')
        resp = self.client.get(path + '?user=a,b')
        self.assertEqual(resp.status_code, 400)

    def test_api_search_video_by_mission(self):
        path = reverse('api_v1_2:search-video') + '?mission=3'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)
        # results should be from mission id 3
        self.assertEqual(json_resp.get('results')[0]['mission_id'], 3)
        # results should be sorted by rank
        self.assertEqual(json_resp.get('results')[0]['id'], 1)
        self.assertEqual(json_resp.get('results')[1]['id'], 2)

    def test_api_search_video_invalid_mission_id(self):
        path = reverse('api_v1_2:search-video')
        resp = self.client.get(path + '?mission=somemission')
        self.assertEqual(resp.status_code, 400)

    def test_api_search_video_by_query(self):
        path = reverse('api_v1_2:search-video') + '?query=test'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 4)

    def test_api_search_video_by_tags(self):
        path = reverse('api_v1_2:search-video') + '?query=hashtag1'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        self.assertEqual(json_resp.get('results')[0]['id'], 1)

    def test_api_search_video_no_param(self):
        resp = self.client.get(reverse('api_v1_2:search-video'))
        self.assertEqual(resp.status_code, 400)

    def test_api_search_video_have_fields(self):
        path = reverse('api_v1_2:search-video') + '?query=test'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        video = json_resp.get('results')[0]
        self.assertTrue('has_liked' in video)
        self.assertTrue('has_disliked' in video)

    def test_api_search_order(self):
        # default order
        path = reverse('api_v1_2:search-video') + '?query=test'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 4)
        self.assertEqual(json_resp.get('results')[0]['id'], 4)
        self.assertEqual(json_resp.get('results')[1]['id'], 2)
        self.assertEqual(json_resp.get('results')[2]['id'], 1)
        # oldest first
        path = reverse('api_v1_2:search-video') + '?query=test&order=oldest'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 4)
        self.assertEqual(json_resp.get('results')[0]['id'], 6)
        self.assertEqual(json_resp.get('results')[1]['id'], 1)
        self.assertEqual(json_resp.get('results')[2]['id'], 2)
        # rank
        path = reverse('api_v1_2:search-video') + '?query=test&order=rank'
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 4)
        self.assertEqual(json_resp.get('results')[0]['id'], 4)
        self.assertEqual(json_resp.get('results')[1]['id'], 1)
        self.assertEqual(json_resp.get('results')[2]['id'], 2)


class ApiCreateVideoTestcases(TestCase):
    fixtures = ['fixture_api_v1_2.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_create_video_success(self):
        path = reverse('api_v1_2:list-create-video')
        resp = self.client.post(path, {
            'name': 'test_video_123',
            'description': 'desp',
            'external_id': 123456,
            'mission_id': 3
        })
        self.assertEqual(resp.status_code, 201)

    def test_model_default_values(self):
        now = dj_now()
        path = reverse('api_v1_2:list-create-video')
        resp = self.client.post(path, {
            'name': 'test video default',
            'external_id': 3456789,
        })
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('is_active'), True)
        self.assertEqual(json_resp.get('is_processed'), False)
        self.assertEqual(json_resp.get('view_count'), 0)
        self.assertEqual(json_resp.get('rank'), 0)
        self.assertEqual(
            json_resp.get('thumbnail_url'),
            'http://view.vzaar.com/3456789/image'
        )
        self.assertEqual(json_resp.get('tags'), '')
        self.assertEqual(json_resp.get('mission'), None)
        self.assertEqual(json_resp.get('video_type'), 0)
        self.assertEqual(json_resp.get('added_by')['username'], 'test_user_1')
        created_at = db_strptime(json_resp.get('created_at'))
        self.assertEqual(created_at.tm_year, now.year)
        self.assertEqual(created_at.tm_mon, now.month)
        self.assertEqual(created_at.tm_mday, now.day)
        self.assertEqual(created_at.tm_hour, now.hour)

    def test_log_exist(self):
        self.assertEqual(os.path.exists(
            os.path.abspath(os.path.join(settings.ROOT_PATH, 'logs/whambush.log'))
        ), True)

    def test_api_create_video_partner(self):
        path = reverse('api_v1_2:list-create-video')
        resp = self.client.post(path, {
            'name': 'test partner video',
            'description': '',
            'external_id': 123456,
            'video_type': 1
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('video_type'), 1)
        # this video should not be active
        self.assertFalse(json_resp.get('is_active'))

    def test_api_create_video_with_tags_success(self):
        path = reverse('api_v1_2:list-create-video')
        resp = self.client.post(path, {
            'name': 'test partner video',
            'external_id': 123456,
            'tags': 'abc1,abc2'
        })
        json_resp = json.loads(resp.content)
        self.assertEqual(resp.status_code, 201)
        self.assertSequenceEqual(json_resp.get('tags'), 'abc1,abc2')

    def test_api_create_video_with_tags_mixed_string(self):
        path = reverse('api_v1_2:list-create-video')
        resp = self.client.post(path, {
            'name': 'test partner video',
            'external_id': 123456,
            'tags': 'abc1 ,cbää, ö#€a,âăhiếu'
        })
        json_resp = json.loads(resp.content)
        self.assertEqual(resp.status_code, 201)
        self.assertSequenceEqual(json_resp.get('tags'), u'abc1,cbää,öa,âăhiếu')

    def test_api_create_tv_video(self):
        path = reverse('api_v1_2:list-create-video')
        resp = self.client.post(path, {
            'name': 'test tv video',
            'description': '',
            'external_id': 123456,
            'video_type': 2
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('video_type'), 2)
        self.assertFalse(json_resp.get('is_active'))
