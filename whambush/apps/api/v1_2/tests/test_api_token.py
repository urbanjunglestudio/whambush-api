from django.test import TestCase
from django.core.urlresolvers import reverse
from django.utils.timezone import now as django_now

from .services import login, register, set_token, reverse_list_videos
from whambush.apps.missions.models import Mission
from whambush.apps.accounts.models import WhamUser

from constance import config
from datetime import datetime, timedelta
import json


class ApiTokenTestcases(TestCase):
    fixtures = ['fixture_api_v1_2.json']

    def setUp(self):
        Mission.objects.create(
            id=1,
            name='Test active mission',
            start_at=datetime.now() - timedelta(days=1),  # 1 day before
            end_at=datetime.now() + timedelta(days=2),  # 2 days after
            added_by=WhamUser.objects.get(pk=1),
            description=''
        )
        config.IS_BETA = False

    def _check_last_login_updated(self, username):
        now = django_now().replace(second=0, microsecond=0)
        now = (now - datetime(1970, 1, 1)).total_seconds()
        last_login = WhamUser.objects.get(username=username).last_login
        last_login = last_login.replace(second=0, microsecond=0)
        last_login = (last_login - datetime(1970, 1, 1)).total_seconds()
        self.assertEqual(last_login, now)

    def _check_user_active(self, user_id):
        user = WhamUser.objects.get(pk=user_id)
        self.assertTrue(user.is_active)

    def test_get_token_success(self):
        resp = login(self.client, 'test_user_1', 'test_user_1')
        self.assertEqual(resp.status_code, 200)
        # check if user.last_login was updated
        self._check_last_login_updated('test_user_1')
        # check if response has token
        json_resp = json.loads(resp.content)
        self.assertNotEqual(json_resp.get('token'), None)
        # check number of active mission (not submitted video)
        self.assertEqual(json_resp.get('num_active_missions'), 1)
        # check if response has user detail
        json_user = json_resp.get('user')
        self.assertNotEqual(json_user, None)
        self.assertEqual(json_user.get('username'), 'test_user_1')
        # check if response has default video feed settings
        json_settings = json_resp.get('settings')

    def test_get_token_failure(self):
        resp = self.client.post(reverse('api_v1_2:token-auth'), {
            'username': 'test_user_1',
            'password': 'wrong',
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        error = json_resp.get('non_field_errors')[0]
        self.assertEqual(error, 'LOGIN_ERROR_WRONG_CREDENTIALS')

    def test_request_without_token(self):
        resp = self.client.get(reverse_list_videos('my'))
        self.assertEqual(resp.status_code, 403)
        json_resp = json.loads(resp.content)
        error = json_resp.get('detail')
        self.assertEqual(
            error, 'Authentication credentials were not provided.'
        )

    def test_request_with_inactive_user(self):
        # register the user
        resp = register(
            self.client,
            'test_username',
            'test_password',
            'test_password',
            'username@test.com'
        )
        self.assertEqual(resp.status_code, 201)
        # at this moment the user should be active
        json_resp = json.loads(resp.content)
        user_id = json_resp.get('id')
        self._check_user_active(user_id)
        # now change the date joined to long ago
        WhamUser.objects.filter(pk=user_id).update(
            date_joined=datetime.now() - timedelta(days=30)
        )

        # and login with that account
        resp = login(self.client, 'test_username', 'test_password')
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        set_token(self.client, token=json_resp.get('token'))
        self.assertEqual(json_resp.get(
            'non_field_errors'), [u'LOGIN_ERROR_INACTIVE_USER']
        )
        # requests from this user should not be allowed
        resp = self.client.get(reverse_list_videos('my'))
        self.assertEqual(resp.status_code, 403)
        json_resp = json.loads(resp.content)
        error = json_resp.get('detail')
        self.assertEqual(error, 'TOKEN_ERROR_INVALID_TOKEN')

    def test_request_with_banned_user(self):
        # register the user
        resp = register(
            self.client,
            'test_username',
            'test_password',
            'test_password',
            'username@test.com'
        )
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        user_id = json_resp.get('id')
        user = WhamUser.objects.get(pk=user_id)
        user.is_active = False
        user.save()

        # and login with that account
        resp = login(self.client, 'test_username', 'test_password')
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        set_token(self.client, token=json_resp.get('token'))
        self.assertEqual(json_resp.get(
            'non_field_errors'), [u'LOGIN_ERROR_BANNED_USER']
        )

        # requests from this user should not be allowed
        resp = self.client.get(reverse_list_videos('my'))
        self.assertEqual(resp.status_code, 403)
        json_resp = json.loads(resp.content)
        error = json_resp.get('detail')
        self.assertEqual(error, 'TOKEN_ERROR_INVALID_TOKEN')

    def test_request_success(self):
        resp = self.client.post(reverse('api_v1_2:token-auth'), {
            'username': 'test_user_1',
            'password': 'test_user_1',
        })
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token %s' % token

        resp = self.client.get(reverse_list_videos('my'))
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)
        self.assertEqual(resp.status_code, 200)

    def test_permission_success(self):
        resp = self.client.post(reverse('api_v1_2:token-auth'), {
            'username': 'test_user_1',
            'password': 'test_user_1',
        })
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token %s' % token

    def test_permission_failure(self):
        # request to view has admin permission, using token from normal user
        resp = self.client.post(reverse('api_v1_2:token-auth'), {
            'username': 'test2',
            'password': 'abc12345',
        })
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        self.client.defaults['HTTP_AUTHORIZATION'] = 'Token %s' % token
