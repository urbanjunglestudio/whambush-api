from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import (
    login, set_token, reverse_create_comment, get_latest_actionstream,
    reverse_like_video, reverse_follow, reverse_unfollow
)
from whambush.apps.accounts.models import WhamUser

from constance import config
import json


class ActionStreamTest(TestCase):
    fixtures = ['fixture_api_v1_2.json']

    def setUp(self):
        # set beta to false
        config.IS_BETA = False

        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_login(self):
        actstr = get_latest_actionstream()
        self.assertEqual(actstr.actor.username, 'test_user_1')
        self.assertEqual(actstr.verb, 'logged in')
        # check if user.last_login was equal action stream timestamp
        last_login = WhamUser.objects.get(username='test_user_1').last_login
        last_login = last_login.replace(second=0, microsecond=0)
        timestamp = actstr.created_at.replace(second=0, microsecond=0)
        self.assertEqual(last_login, timestamp)

    def test_login_without_device_info(self):
        login(self.client, 'test2', 'abc12345')
        actstr = get_latest_actionstream()
        # check default device values
        self.assertEqual(actstr.data['device'], '')
        self.assertEqual(actstr.data['manufacturer'], '')
        self.assertEqual(actstr.data['os'], '')
        self.assertEqual(actstr.data['os_version'], '')
        self.assertEqual(actstr.data['whambush_version'], '')

    def test_login_with_device_info(self):
        device_info = {
            'device': 'a',
            'manufacturer': 'b',
            'os': 'c',
            'os_version': 'd',
            'whambush_version': '10'
        }
        login(self.client, 'test2', 'abc12345', device_info)
        actstr = get_latest_actionstream()
        # check default device values
        self.assertEqual(actstr.data['device'], 'a')
        self.assertEqual(actstr.data['manufacturer'], 'b')
        self.assertEqual(actstr.data['os'], 'c')
        self.assertEqual(actstr.data['os_version'], 'd')
        self.assertEqual(actstr.data['whambush_version'], '10')

    def test_comment(self):
        path = reverse_create_comment(1)
        resp = self.client.post(path, {
            'comment': 'test action stream'
        })
        self.assertEqual(resp.status_code, 201)
        actstr = get_latest_actionstream()
        self.assertEqual(actstr.actor.username, 'test_user_1')
        self.assertEqual(actstr.verb, 'commented on')
        self.assertEqual(actstr.target.id, 1)

    def test_like(self):
        path = reverse_like_video()
        self.client.post(path, {
            'video_id': 1,
        })
        actstr = get_latest_actionstream()
        self.assertEqual(actstr.actor.username, 'test_user_1')
        self.assertEqual(actstr.verb, 'liked')
        self.assertEqual(actstr.target.id, 1)

    def test_follow(self):
        path = reverse_follow()
        self.client.post(path, {
            'to_user': 2
        })
        actstr = get_latest_actionstream()
        self.assertEqual(actstr.actor.username, 'test_user_1')
        self.assertEqual(actstr.verb, 'followed')
        self.assertEqual(actstr.target.id, 2)

    def test_unfollow(self):
        self.client.delete(reverse_unfollow(1))
        actstr = get_latest_actionstream()
        self.assertEqual(actstr.actor.id, 3)
        self.assertEqual(actstr.verb, 'unfollowed')
        self.assertEqual(actstr.target.id, 1)

    def test_delete_video(self):
        path = reverse('api_v1_2:video-detail', kwargs={'pk': 1})
        resp = self.client.delete(path)
        self.assertEqual(resp.status_code, 204)
        actstr = get_latest_actionstream()
        self.assertEqual(actstr.actor.id, 3)
        self.assertEqual(actstr.verb, 'deleted')
        self.assertEqual(actstr.target.id, 1)
