from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import (
    login, set_token,
)
import json


class ApiCountriesViewTestcases(TestCase):
    fixtures = ['fixture_api_v2_1.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_list_all_countries(self):
        resp = self.client.get(reverse('api_v2_1:list_countries'))
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 196)
        # supported countries should be on top
        for i in range(0, 1):
            self.assertTrue(json_resp.get('results')[i].get('supported'))

    def test_api_list_supported_countries(self):
        resp = self.client.get(
            reverse('api_v2_1:list_countries') + '?type=supported'
        )
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)
