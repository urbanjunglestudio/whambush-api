from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import (
    login, set_token
)

import json


class ApiUserSettingsViewTestcases(TestCase):
    fixtures = ['fixture_api_v2_1.json']

    current_username = 'test_user_1'
    current_userpassword = 'test_user_1'

    def setUp(self):
        # test_user_1 is admin user
        resp = login(
            self.client,
            self.current_username,
            self.current_userpassword
        )
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_create_setting_without_params(self):
        path = reverse('api_v2_1:list_create_notification_settings')
        self.assertEqual(path, '/v2.1/settings/notifications/')
        resp = self.client.post(path)
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        # key should be required
        self.assertEqual(json_resp.get('key')[0], 'This field is required.')

    def test_api_create_setting_success(self):
        path = reverse('api_v2_1:list_create_notification_settings')
        resp = self.client.post(path, {
            'key': 'new_email',
            'value': 1
        })
        self.assertEqual(resp.status_code, 201)

    def test_api_update_setting_success(self):
        path = reverse('api_v2_1:list_create_notification_settings')
        resp = self.client.post(path, {
            'key': 'user setting 1',
            'value': 1
        })
        self.assertEqual(resp.status_code, 201)
        # new value should be returned
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        settings = json_resp.get('results')
        self.assertEqual(settings[0].get('value'), '1')

    def test_api_get_setting_success(self):
        path = reverse('api_v2_1:list_create_notification_settings')
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        settings = json_resp.get('results')
        self.assertEqual(settings[0].get('value'), 'some test 2')
