from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import login, set_token
from whambush.apps.missions.models import Mission
from whambush.apps.accounts.models import WhamUser

import json
from datetime import datetime, timedelta


class ApiMissionsViewTestcases(TestCase):
    fixtures = ['fixture_api_v2_1.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        self.user = WhamUser.objects.get(username='test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

        # Create 3 missions for testing active mission.
        # There are 4 old missions in the fixture, 2 FI, 1 ZZ and 1 VN
        # so there should be 4 FI, 1 ZZ and 1 VN
        Mission.objects.create(
            id=1,
            name='Test active mission',
            start_at=datetime.now() - timedelta(days=2),  # 2 day before
            end_at=datetime.now() + timedelta(days=2),  # 2 days after
            added_by=WhamUser.objects.get(pk=1),
            description='',
            country='FI'
        )
        Mission.objects.create(
            id=2,
            name='Test inactive mission',
            start_at=datetime.strptime('2012-01-01', '%Y-%m-%d'),
            end_at=datetime.strptime('2012-01-10', '%Y-%m-%d'),
            added_by=WhamUser.objects.get(pk=1),
            description='',
            country='FI'
        )
        # icon mission
        Mission.objects.create(
            id=99,
            name='Test active mission type 1',
            start_at=datetime.now() - timedelta(days=1),  # 1 day before
            end_at=datetime.now() + timedelta(days=2),  # 2 days after
            added_by=WhamUser.objects.get(pk=1),
            description='',
            country='FI',
            mission_type=1
        )

    def test_api_mission_detail_return_default_values(self):
        path = reverse('api_v2_1:mission-detail', kwargs={'pk': 2})
        self.assertEqual(path, '/v2.1/missions/2/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('id'), 2)
        self.assertEqual(json_resp.get('start_at'), "2012-01-01T00:00:00")
        self.assertEqual(json_resp.get('end_at'), "2012-01-10T00:00:00")
        self.assertFalse(json_resp.get('is_videos_ranked'))
        self.assertFalse(json_resp.get('has_submitted'))
        self.assertEqual(json_resp.get('bananas'), 0)
        self.assertEqual(json_resp.get('prize'), '')
        self.assertEqual(json_resp.get('max_submissions'), 1000)
        self.assertEqual(json_resp.get('max_video_length'), 30)
        self.assertEqual(json_resp.get('max_user_submissions'), 1)
        self.assertEqual(json_resp.get('mission_image_1'), '')
        self.assertEqual(json_resp.get('mission_image_2'), '')
        self.assertFalse(json_resp.get('has_liked'))
        self.assertFalse(json_resp.get('has_disliked'))
        self.assertEqual(json_resp.get('icon_name'), '')
        self.assertEqual(json_resp.get('status'), 'old')

    def test_api_mission_list_active(self):
        resp = self.client.get(reverse('api_v2_1:list-missions'))
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        result = json_resp.get('results')[0]
        self.assertEqual(json_resp.get('count'), 2)
        self.assertTrue('has_submitted' in result)
        # icon mission should be on top
        self.assertEqual(result.get('mission_type'), 1)

    def test_api_mission_list_old(self):
        resp = self.client.get(reverse('api_v2_1:list-missions') + '?type=old')
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 6)
        # end time of the missions should be before now
        end_at = json_resp.get('results')[0]['end_at']
        end_at = datetime.strptime(end_at, "%Y-%m-%dT%H:%M:%S")
        self.assertTrue(end_at < datetime.now())
        self.assertTrue('has_submitted' in json_resp.get('results')[0])

    def test_api_mission_list_new(self):
        Mission.objects.create(
            name="Test new",
            start_at=datetime.now() + timedelta(days=1),
            end_at=datetime.now() + timedelta(days=3),
            country="FI",
            added_by=self.user
        )
        resp = self.client.get(reverse('api_v2_1:list-missions') + '?type=new')
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 1)
        # start time of the missions should be after now
        start_at = json_resp.get('results')[0]['start_at']
        start_at = datetime.strptime(start_at, "%Y-%m-%dT%H:%M:%S.%f")
        self.assertTrue(start_at > datetime.now())

    def test_api_mission_list_all(self):
        resp = self.client.get(reverse('api_v2_1:list-missions') + '?type=all')
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 8)

    def test_api_mission_list_order(self):
        # active list => icon mission first
        resp = self.client.get(reverse('api_v2_1:list-missions'))
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('results')[0].get('id'), 99)
        self.assertEqual(json_resp.get('results')[1].get('id'), 1)

        # old list => latest first
        resp = self.client.get(reverse('api_v2_1:list-missions') + '?type=old')
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('results')[0].get('id'), 7)
        self.assertEqual(json_resp.get('results')[1].get('id'), 6)

    def test_api_mission_list_old_filtered_by_country(self):
        resp = self.client.get(
            reverse('api_v2_1:list-missions') + '?type=old&country=FI'
        )
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 3)

    def test_api_mission_list_filter_by_user(self):
        resp = self.client.get(reverse('api_v2_1:list-missions') + '?user=1')
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 2)
        results = json_resp.get('results')
        for m in results:
            self.assertEqual(m.get('added_by').get('id'), 1)

    def test_api_mission_detail_has_submitted(self):
        path = reverse('api_v2_1:list-create-video')
        resp = self.client.post(path, {
            'name': 'test submit video',
            'description': '',
            'external_id': 123,
            'mission_id': 1
        })
        self.assertEqual(resp.status_code, 201)
        path = reverse('api_v2_1:mission-detail', kwargs={'pk': 1})
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertEqual(resp.status_code, 200)
        self.assertTrue(json_resp.get('has_submitted'))

    def test_api_create_mission_no_param(self):
        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {})
        self.assertEqual(resp.status_code, 400)
        # name and country should be required
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('name')[0], "This field is required.")
        self.assertEqual(json_resp.get('country')[0], "This field is required.")
        self.assertEqual(
            json_resp.get('description')[0], "This field is required."
        )
        self.assertEqual(
            json_resp.get('mission_type')[0], "This field is required."
        )
        self.assertEqual(
            json_resp.get('start_at')[0], "This field is required."
        )
        self.assertEqual(json_resp.get('end_at')[0], "This field is required.")

    def test_api_create_mission_wrong_country(self):
        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {
            'name': 'Test api',
            'country': 'Fi'
        })
        self.assertEqual(resp.status_code, 400)
        # country should be invalid
        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('country')[0],
            "Select a valid choice. Fi is not one of the available choices."
        )

    def test_api_create_mission_wrong_added_by_user(self):
        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {
            'name': 'Test api',
            'country': 'FI',
            'added_by': 10000
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertIn("Invalid pk", json_resp.get('added_by')[0])

    def test_api_create_mission_normal_user(self):
        resp = login(self.client, 'test_user', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {
            'name': 'Test api',
            'country': 'FI'
        })
        # normal user should not have permission to create missions
        self.assertEqual(resp.status_code, 403)
        json_resp = json.loads(resp.content)
        self.assertEqual(
            json_resp.get('detail'),
            "You do not have permission to perform this action."
        )

    def test_api_create_mission_success_without_added_by_param(self):
        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {
            'name': 'Test api',
            'country': 'FI',
            'description': 'A test mission',
            'mission_type': 1,
            'start_at': datetime.now() + timedelta(days=2),
            'end_at': datetime.now() + timedelta(days=5)
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('name'), "Test api")
        # added_by should be currently logged in user
        added_by = json_resp.get('added_by')
        self.assertEqual(added_by.get('username'), 'test_user_1')

    def test_api_create_mission_success_with_added_by_param(self):
        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {
            'name': 'Test api',
            'country': 'FI',
            'added_by': 1,
            'description': 'A test mission',
            'mission_type': 1,
            'start_at': datetime.now() + timedelta(days=2),
            'end_at': datetime.now() + timedelta(days=5)
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('name'), "Test api")
        # added_by should be the one provided instead of
        # currently logged in user
        added_by = json_resp.get('added_by')
        self.assertEqual(added_by.get('username'), 'test_user')

    def test_api_create_mission_success_with_linked_video_param(self):
        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {
            'name': 'Test api',
            'country': 'FI',
            'linked_video': 1,
            'description': 'A test mission',
            'mission_type': 1,
            'start_at': datetime.now() + timedelta(days=2),
            'end_at': datetime.now() + timedelta(days=5)
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('name'), "Test api")
        linked_video = json_resp.get('linked_video')
        self.assertEqual(linked_video.get('id'), 1)

    def test_api_create_mission_success_with_icon_name_param(self):
        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {
            'name': 'Test api',
            'country': 'FI',
            'icon_name': 'foo bar',
            'description': 'A test mission',
            'mission_type': 1,
            'start_at': datetime.now() + timedelta(days=2),
            'end_at': datetime.now() + timedelta(days=5)
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('name'), "Test api")
        self.assertEqual(json_resp.get('icon_name'), 'foo bar')

    def test_api_create_mission_success_by_user_type_3(self):
        self.user.user_type = 3
        self.user.save()

        path = reverse('api_v2_1:list-missions')
        resp = self.client.post(path, {
            'name': 'Test api',
            'country': 'FI',
            'icon_name': 'foo bar',
            'description': 'A test mission',
            'mission_type': 1,
            'start_at': datetime.now() + timedelta(days=2),
            'end_at': datetime.now() + timedelta(days=5)
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('name'), "Test api")
        self.assertEqual(json_resp.get('icon_name'), 'foo bar')

    def test_api_get_mission_by_slug_fail(self):
        path = reverse('api_v2_1:mission-detail-slug', kwargs={
            'slug': 1
        })
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 404)

    def test_api_get_mission_by_slug(self):
        path = reverse('api_v2_1:mission-detail-slug', kwargs={
            'slug': 'test-active-mission'
        })
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('name'), "Test active mission")

    def test_api_get_mission_has_liked_field(self):
        path = reverse('api_v2_1:mission-detail', kwargs={'pk': 4})
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('id'), 4)
        self.assertTrue(json_resp.get('has_liked'))
        self.assertFalse(json_resp.get('has_disliked'))

    def test_api_get_mission_with_status(self):
        mission = Mission.objects.create(
            name="Test status",
            start_at=datetime.now() + timedelta(days=1),
            end_at=datetime.now() + timedelta(days=3),
            country="FI",
            added_by=self.user
        )
        path = reverse('api_v2_1:mission-detail', kwargs={'pk': mission.id})
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('status'), 'new')

        mission.start_at = datetime.now() - timedelta(days=3)
        mission.save()
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('status'), 'active')

        mission.end_at = datetime.now() - timedelta(days=1)
        mission.save()
        resp = self.client.get(path)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('status'), 'old')

    def test_api_update_mission(self):
        path = reverse('api_v2_1:mission-detail', kwargs={'pk': 1})
        resp = self.client.post(path, {
            "_method": "PUT",
            "description": "Test update"
        })
        self.assertEqual(resp.status_code, 200)
        m = Mission.objects.get(pk=1)
        self.assertEqual(m.description, "Test update")
