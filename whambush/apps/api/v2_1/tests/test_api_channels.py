from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import login, set_token

from constance import config
import json


class ApiChannelsViewTestcases(TestCase):
    fixtures = ['fixture_api_v2_1.json']

    def setUp(self):
        # set beta to false
        config.IS_BETA = False

        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_channel_detail(self):
        path = reverse('api_v2_1:retrieve_channel', kwargs={'pk': 1})
        self.assertEqual(path, '/v2.1/channels/1/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('id'), 1)
        self.assertEqual(json_resp.get('name'), 'Test channel 1')
        self.assertEqual(len(json_resp.get('children')), 2)
        self.assertIn(
            'Test channel 4', "{} {}".format(
                json_resp.get('children')[0].get('name'),
                json_resp.get('children')[1].get('name')
            )
        )

    def test_api_channel_list(self):
        path = reverse('api_v2_1:list_channels')
        self.assertEqual(path, '/v2.1/channels/')
        resp = self.client.get(path)
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('count'), 4)
        # video channel should have 'user_id' field if it has an artist
        for c in json_resp.get('results'):
            if c.get('channel_type') == 1 and c.get('videos_endpoint'):
                self.assertIn('user_id', c)
