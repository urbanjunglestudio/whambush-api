from django.test import TestCase
from django.core.urlresolvers import reverse

from .services import (
    login, set_token,
)
from whambush.apps.general_data.models import GeneralData

import json


class ApiGeneralDataViewTestcases(TestCase):
    fixtures = ['fixture_api_v2_1.json']

    def setUp(self):
        # test_user_1 is admin user
        resp = login(self.client, 'test_user_1', 'test_user_1')
        json_resp = json.loads(resp.content)
        token = json_resp.get('token')
        set_token(self.client, token)

    def test_api_create_general_data_success(self):
        test_key = 'test_key_1'
        test_value = 'some test value here'
        path = reverse('api_v2_1:create_general_data')
        resp = self.client.post(path, {
            'key': test_key,
            'value': test_value
        })
        self.assertEqual(resp.status_code, 201)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp.get('key'), test_key)
        self.assertEqual(
            GeneralData.objects.get(key='test_key_1').value, test_value
        )

    def test_api_create_without_params(self):
        path = reverse('api_v2_1:create_general_data')
        # without key
        resp = self.client.post(path, {
            'value': 'a failure test'
        })
        self.assertEqual(resp.status_code, 400)
        # without value
        resp = self.client.post(path, {
            'key': 'test_key'
        })
        self.assertEqual(resp.status_code, 400)
        # create duplicate keys
        resp = self.client.post(path, {
            'key': 'a',
            'value': 'a'
        })
        self.assertEqual(resp.status_code, 201)
        resp = self.client.post(path, {
            'key': 'a',
            'value': 'b'
        })
        self.assertEqual(resp.status_code, 400)
        json_resp = json.loads(resp.content)
        self.assertIn('already exists', json_resp.get('key')[0])

    def test_api_update_general_data_success(self):
        path = reverse('api_v2_1:create_general_data')
        resp = self.client.post(path, {
            'key': 'a',
            'value': 'b'
        })
        self.assertEqual(resp.status_code, 201)
        path = reverse('api_v2_1:retrieve_update_general_data', kwargs={
            'key': 'a'
        })
        resp = self.client.post(path, {
            '_method': 'PUT',
            'value': 'c'
        })
        self.assertEqual(resp.status_code, 200)
        self.assertEqual(GeneralData.objects.get(key='a').value, 'c')
