from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from ..serializers import (
    GeneralDataSerializer
)
from whambush.apps.general_data.models import GeneralData


@permission_classes((IsAuthenticated, ))
class CreateGeneralData(generics.CreateAPIView):
    model = GeneralData
    serializer_class = GeneralDataSerializer


@permission_classes((IsAuthenticated, ))
class RetrieveUpdateGeneralData(generics.RetrieveUpdateAPIView):
    model = GeneralData
    serializer_class = GeneralDataSerializer
    lookup_field = 'key'

    # TODO: not allow updating key
    def get_serializer(
            self, instance=None, data=None, files=None,
            many=False, partial=False
    ):
        if instance is not None:
            data['key'] = instance.key
        return super(RetrieveUpdateGeneralData, self).get_serializer(
            instance=instance, data=data, files=files,
            many=many, partial=partial
        )
