from django.contrib.contenttypes.models import ContentType
from django.contrib.comments.models import Comment
from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.decorators import permission_classes

from ..services import response_invalid_args
from ..serializers import (
    CommentVideoSerializer, CommentFlagsSerializer
)
from ..services import delete_comment
from whambush.apps.videos.models import (
    Video, Flag
)

from whambush.libs.utils import clone_REST_request, send_alert_email

from constance import config


@permission_classes((IsAuthenticated, ))
class ListCreateVideoComment(generics.ListCreateAPIView):
    model = Comment
    serializer_class = CommentVideoSerializer

    def get_queryset(self):
        '''
        6.3 List comment
        '''
        video_id = self.kwargs.pop('video_id')
        try:
            video = Video.objects.get(pk=video_id)
        except Video.DoesNotExist:
            return Comment.objects.filter(content_type=0, object_pk=0)
        return Comment.objects.filter(
            content_type=ContentType.objects.get_for_model(video),
            object_pk=video.id,
            is_removed=False
        ).order_by('submit_date').reverse()

    def post(self, request, *args, **kwargs):
        '''
        6.1 Create comment
        '''
        video_id = kwargs.get('video_id')
        req = clone_REST_request(request)
        req._data.update({
            'video_id': video_id,
            'user': request.user.id
        })
        return super(ListCreateVideoComment, self).post(req, args, kwargs)


@permission_classes((IsAuthenticated, ))
class VideoCommentDetail(generics.RetrieveDestroyAPIView):
    '''
    6.2 Delete comment
    '''
    model = Comment
    serializer_class = CommentVideoSerializer

    def delete(self, request, *args, **kwargs):
        comment = self.get_object()
        if delete_comment(comment, request.user):
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(
                {'detail': "User does not have permission."},
                status=status.HTTP_400_BAD_REQUEST
            )


@permission_classes((IsAuthenticated, ))
class ListCreateCommentFlag(generics.ListCreateAPIView):
    model = Flag
    serializer_class = CommentFlagsSerializer

    def get_queryset(self):
        return Flag.objects.filter(
            object_pk=self.kwargs.pop('comment_id'),
            flag_type=1,
            user=self.request.user
        )

    def post(self, request, *args, **kwargs):
        # validate request
        comment_id = kwargs.pop('comment_id')
        if comment_id is None or not comment_id.isdigit():
            return response_invalid_args()
        try:
            comment = Comment.objects.get(pk=comment_id)
        except Comment.DoesNotExist:
            return response_invalid_args()

        # remove previous flag of same user
        flag = Flag.objects.filter(
            object_pk=comment_id,
            flag_type=1,
            user=request.user
        )
        flag.delete()

        # update the request and save
        req = clone_REST_request(request)
        req._data.update({
            'comment': comment_id,
            'user': reverse(
                'api_v2_1:whamuser-detail', kwargs={'pk': request.user.id}
            ),
            'flag_type': 1
        })
        result = super(ListCreateCommentFlag, self).post(req, args, kwargs)

        # send email to support if needed
        limit = int(config.FLAG_LIMIT)
        flag_count = Flag.objects.filter(
            object_pk=comment.pk, flag_type=1
        ).count()
        if flag_count == limit:
            send_alert_email('FLAG_COMMENT', {
                'comment': comment,
                'request': request,
                'limit': limit
            })

        return result
