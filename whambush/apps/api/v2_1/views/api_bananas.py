from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAdminUser
from rest_framework.reverse import reverse

from ..serializers import BananaSerializer

from ..services import response_invalid_args
from whambush.apps.accounts.models import (
    BananaTransaction, UserScore, WhamUser
)
from whambush.libs.utils import clone_REST_request


@permission_classes((IsAdminUser, ))
class ListCreateBananaTransaction(generics.ListCreateAPIView):
    queryset = BananaTransaction.objects.all()
    serializer_class = BananaSerializer

    def post(self, request, *args, **kwargs):
        user_id = request.DATA.get('user')
        amount = request.DATA.get('amount')

        # validate request
        if user_id is None:
            return response_invalid_args()
        try:
            amount = int(amount)
            user_id = int(user_id)
            user = WhamUser.objects.get(pk=user_id)
        except ValueError:
            return response_invalid_args()
        except WhamUser.DoesNotExist:
            return response_invalid_args()

        # modify userscore banana
        user_score, created = UserScore.objects.get_or_create(user=user)
        user_score.banana += amount
        user_score.save()

        # update request data
        req = clone_REST_request(request)
        req._data.update({
            'user': reverse(
                'api_v2_1:whamuser-detail',
                kwargs={'pk': user_id}
            )
        })
        return super(ListCreateBananaTransaction, self).post(
            req, *args, **kwargs
        )
