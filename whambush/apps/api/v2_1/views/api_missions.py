from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from ..serializers import MissionSerializer
from ..services import (
    get_country_filter
)
from ...permissions import CanCreateMissionOrReadOnly
from whambush.apps.missions.models import Mission


@permission_classes((IsAuthenticated, CanCreateMissionOrReadOnly))
class ListMission(generics.ListCreateAPIView):
    model = Mission
    serializer_class = MissionSerializer

    def get_queryset(self):
        filter_type = self.request.GET.get('type')
        user_id = self.request.GET.get('user')
        countries = get_country_filter(self.request)
        if filter_type == 'old':
            # old missions
            return (
                Mission.objects.old().added_by(user_id)
                .country_filter(countries).wb_order()
            )
        elif filter_type == 'new':
            # not yet started missions
            return (
                Mission.objects.new().added_by(user_id)
                .country_filter(countries).wb_order()
            )
        elif filter_type == 'all':
            return (
                Mission.objects.all().added_by(user_id)
                .country_filter(countries).wb_order()
            )
        else:
            # active missions
            return (
                Mission.objects.active().added_by(user_id)
                .country_filter(countries).wb_order(True)
            )

    def pre_save(self, obj):
        if (
            not self.request.DATA.get('added_by') or
            self.request.DATA.get('added_by') and
            not self.request.user.is_superuser
        ):
            obj.added_by = self.request.user


@permission_classes((IsAuthenticated, ))
class MissionDetail(generics.RetrieveUpdateAPIView):
    model = Mission
    serializer_class = MissionSerializer

    def put(self, request, *args, **kwargs):
        kwargs.update({
            "partial": True
        })
        return super(MissionDetail, self).put(request, *args, **kwargs)
