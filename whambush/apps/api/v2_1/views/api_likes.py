from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes

from ..services import (
    response_invalid_args,
    user_likes_video,
    user_unlikes_video,
    get_likes_on_video,
    user_likes_mission,
    user_unlikes_mission,
    get_likes_on_mission
)


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def like_video(request, point):
    '''
    5.1 Like video
    '''
    video_id = request.DATA.get('video_id')
    if video_id is None or not video_id.isdigit():
        return response_invalid_args()
    likes = user_likes_video(user=request.user, video_id=video_id, point=point)
    if not likes:
        return Response(
            {"success": False, 'detail': "Invalid video ID"},
            status=status.HTTP_400_BAD_REQUEST
        )

    return Response(
        {"success": True, "likes": likes[0], "dislikes": likes[1]},
        status=status.HTTP_200_OK
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def unlike_video(request, point):
    '''
    5.2 Unlike video
    '''
    video_id = request.DATA.get('video_id')
    if video_id is None or not video_id.isdigit():
        return response_invalid_args()
    likes = user_unlikes_video(
        user=request.user, video_id=video_id, point=point
    )

    return Response(
        {"success": True, "likes": likes[0], "dislikes": likes[1]},
        status=status.HTTP_200_OK
    )


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def likes_on_video(request, pk):
    '''
    5.3 Get likes on video
    '''
    likes = get_likes_on_video(pk)

    return Response(
        {"likes": likes[0], "dislikes": likes[1]},
        status=status.HTTP_200_OK
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def like_mission(request, point):
    '''
    5.4 Like mission
    '''
    mission_id = request.DATA.get('mission_id')
    if mission_id is None or not mission_id.isdigit():
        return response_invalid_args()
    likes = user_likes_mission(
        user=request.user, mission_id=mission_id, point=point
    )
    if not likes:
        return Response(
            {"success": False, 'detail': "Invalid mission ID"},
            status=status.HTTP_400_BAD_REQUEST
        )

    return Response(
        {"success": True, "likes": likes[0], "dislikes": likes[1]},
        status=status.HTTP_200_OK
    )


@api_view(['POST'])
@permission_classes((IsAuthenticated, ))
def unlike_mission(request, point):
    '''
    5.5 Unlike mission
    '''
    mission_id = request.DATA.get('mission_id')
    if mission_id is None or not mission_id.isdigit():
        return response_invalid_args()
    likes = user_unlikes_mission(
        user=request.user, mission_id=mission_id, point=point
    )

    return Response(
        {"success": True, "likes": likes[0], "dislikes": likes[1]},
        status=status.HTTP_200_OK
    )


@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def likes_on_mission(request, pk):
    '''
    5.6 Get likes on mission
    '''
    likes = get_likes_on_mission(pk)

    return Response(
        {"likes": likes[0], "dislikes": likes[1]},
        status=status.HTTP_200_OK
    )
