from django.conf import settings
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.decorators import permission_classes

from ..serializers import (
    VideoSerializer, VideoFlagsSerializer, VideoFeedSerializer,
    CommentFlagsSerializer
)
from ..services import (
    get_serializer_listview,
    response_invalid_args,
    get_country_filter,
    get_country_object
)
from whambush.apps.api.permissions import IsOwnerOrReadOnly
from whambush.apps.videos.models import (
    Video, VideoFeed, FeaturedVideos, Flag
)
from whambush.apps.accounts.models import Follow, WhamUser
from whambush.apps.hashtags.models import Hashtag
from whambush.libs import utils

from constance import config
from random import random
import re, datetime


@permission_classes((IsAuthenticated, ))
class ListCreateVideo(generics.ListCreateAPIView):
    model = Video
    serializer_class = VideoSerializer

    def post(self, request, *args, **kwargs):
        req = utils.clone_REST_request(request)

        if not request.user.is_staff:
            req._data.pop('published_at', None)
            req._data.pop('video_type', None)
            req._data.update({'added_by': request.user.id})
            req._data.update({'country': request.user.country})
        else:
            creator = req._data.get('added_by')
            if creator is not None and not creator.isdigit() or creator is None:
                req._data.update({'added_by': request.user.id})
                creator = None
            country = req._data.get('country')
            if creator is None and country is None:
                req._data.update({'country': request.user.country})
            elif creator and country is None:
                req._data.update({
                    'country': WhamUser.objects.get(pk=creator).country
                })
            planned_publish_time = req._data.get('published_at')
            if planned_publish_time:
                planned_publish_time = datetime.datetime.strptime(
                    planned_publish_time, '%Y-%m-%d %H:%M:%S'
                )
                if planned_publish_time > datetime.datetime.now():
                    req._data.update({'is_active': False})

        return super(ListCreateVideo, self).post(req, args, kwargs)

    def pre_save(self, obj):
        if obj.video_type != 0:
            obj.is_active = False

    def post_save(self, obj, created=True):
        tags_request = self.request.DATA.get('tags')
        tags = []
        if tags_request:
            for tag in tags_request.strip().split(','):
                tag = unicode(re.sub('[^\w]+', '', tag, flags=re.UNICODE))
                qs = Hashtag.objects.filter(name__iexact=tag)
                if qs:
                    tags.append(qs[0])
                else:
                    tags.append(Hashtag.objects.create(name=tag))
        obj.tags = tags

    def get_queryset(self):
        filter_type = self.request.GET.get('type')
        countries = get_country_filter(self.request)
        if filter_type == 'my':
            # my videos
            return Video.objects.filter(
                added_by=self.request.user,
                is_active=True,
                deleted_at=None
            ).country_filter(countries).wb_order()
        elif filter_type == 'featured':
            # featured videos
            user_country = get_country_object(self.request.user.country)
            if not countries:
                if user_country.supported:
                    countries = [self.request.user.country]
            return (
                FeaturedVideos.objects.last().videos.active()
                .country_filter(countries)
            )
        elif filter_type == 'following':
            # videos from user I'm following
            return Video.objects.active().filter(
                added_by__in=(
                    Follow.objects
                    .filter(from_user=self.request.user)
                    .values_list('to_user', flat=True)
                )
            ).country_filter(countries).wb_order()
        elif filter_type == 'random':
            # 10 random videos
            return Video.objects.country_filter(countries).random(10)
        elif filter_type == 'tv':
            # tv videos
            return Video.objects.active().filter(
                video_type=2
            ).country_filter(countries).wb_order()
        else:
            # latest videos
            user_country = get_country_object(self.request.user.country)
            if not countries:
                # if user country is not supported, show all videos
                # except TV shows; otherwise show all videos from global
                # missions and user country, and global videos
                if user_country.supported:
                    countries = [
                        self.request.user.country,
                        settings.GLOBAL_COUNTRY_CODE
                    ]
                    return Video.objects.active().exclude(
                        video_type=2
                    ).latest_filter(countries).wb_order()
                else:
                    return Video.objects.active().exclude(
                        video_type=2
                    ).wb_order()
            else:
                return Video.objects.active().exclude(
                    video_type=2
                ).latest_filter(countries, with_global=False).wb_order()


@permission_classes((IsAuthenticated, IsOwnerOrReadOnly))
class RetrieveDestroyVideo(generics.RetrieveUpdateDestroyAPIView):
    model = Video
    serializer_class = VideoSerializer

    def update(self, request, *args, **kwargs):
        attrs = request.DATA.keys()
        attrs.remove('_method')
        allowed_update_attr = ['is_active', 'published_at']
        filtered = [attr for attr in attrs if attr not in allowed_update_attr]
        if filtered:
            errors = {}
            for field in filtered:
                errors.update({
                    field: ["This field can not be updated."]
                })
            return Response(errors, status=status.HTTP_400_BAD_REQUEST)
        kwargs.update({'partial': True})

        return super(RetrieveDestroyVideo, self).update(
            request, *args, **kwargs
        )


@permission_classes((IsAuthenticated, ))
class ListCreateVideoFlag(generics.ListCreateAPIView):
    model = Flag
    serializer_class = VideoFlagsSerializer

    def get_queryset(self):
        return Flag.objects.filter(
            object_pk=self.kwargs.pop('video_id'),
            flag_type=0,
            user=self.request.user
        )

    def post(self, request, *args, **kwargs):
        # validate request
        video_id = kwargs.pop('video_id')
        if video_id is None or not video_id.isdigit():
            return response_invalid_args()
        try:
            video = Video.objects.get(pk=video_id)
        except Video.DoesNotExist:
            return response_invalid_args()

        # remove previous flag of same user
        flag = Flag.objects.filter(
            object_pk=video_id,
            flag_type=0,
            user=request.user
        )
        flag.delete()

        # update the request and save
        req = utils.clone_REST_request(request)
        req._data.update({
            'video': video_id,
            'user': reverse(
                'api_v2_1:whamuser-detail', kwargs={'pk': request.user.id}
            )
        })
        result = super(ListCreateVideoFlag, self).post(req, args, kwargs)

        # send email to support if needed
        limit = int(config.FLAG_LIMIT)
        flag_count = Flag.objects.filter(
            object_pk=video.pk, flag_type=0
        ).count()
        if flag_count == limit:
            utils.send_alert_email('FLAG_VIDEO', {
                'video': video,
                'request': request,
                'limit': limit
            })

        return result


@permission_classes((IsAuthenticated, ))
class RetrieveDestroyFlag(generics.RetrieveDestroyAPIView):
    model = Flag

    def get_serializer_class(self):
        self.object = Flag.objects.get(pk=self.kwargs.get('pk'))
        if self.object.flag_type == 0:
            return VideoFlagsSerializer
        else:
            return CommentFlagsSerializer


@permission_classes((IsAuthenticated, ))
class ListVideoFeed(generics.ListAPIView):
    model = VideoFeed
    serializer_class = VideoFeedSerializer

    def get_queryset(self):
        version = utils.get_api_version(self.request)
        return VideoFeed.objects.filter(version=version).order_by('order')

    def get(self, request, *args, **kwargs):
        version = utils.get_api_version(request)
        default = 1
        qs = VideoFeed.objects.filter(version=version, is_default=True)
        if qs:
            default = qs[0].id
        serializer = get_serializer_listview(self)  # TODO: better idea?
        serializer.data.update({'default': default})
        return Response(serializer.data)


@permission_classes((IsAuthenticated, ))
class SearchVideo(generics.ListAPIView):
    model = Video
    serializer_class = VideoSerializer

    def get_queryset(self):
        order = self.request.GET.get('order')
        countries = get_country_filter(self.request)
        video_type = self.request.GET.get('video_type')
        if self.request.GET.get('user', ''):
            user_ids = [
                int(user_id.strip())
                for user_id in self.request.GET.get('user').split(',')
            ]
            return Video.objects.active().type_filter(video_type).filter(
                added_by__in=user_ids
            ).country_filter(countries).wb_order(order)
        elif self.request.GET.get('mission', ''):
            # default in mission should be rank
            if not order:
                order = 'rank'
            return Video.objects.active().type_filter(video_type).filter(
                mission=self.request.GET.get('mission')
            ).country_filter(countries).wb_order(order)
        elif self.request.GET.get('query', ''):
            search_query = self.request.GET.get('query', '')
            return (
                Video.objects.search(search_query).type_filter(video_type)
                .country_filter(countries).wb_order(order)
            )
        else:
            return Video.objects.none()

    def paginate_queryset(self, queryset, pagesize=None):
        page = super(SearchVideo, self).paginate_queryset(queryset)
        # we need to handle the random order here after the queryset is sliced
        # so new request (for next page) will not intefere with the result
        if self.request.GET.get('order') == 'random':
            page.object_list = sorted(page.object_list, key=lambda x: random())
        return page

    def get(self, request, *args, **kwargs):
        if self.request.GET.get('user', ''):
            try:
                [
                    int(user_id.strip())
                    for user_id in self.request.GET.get('user').split(',')
                ]
            except ValueError:
                return Response(
                    {'detail': 'Invalid user ids'},
                    status=status.HTTP_400_BAD_REQUEST
                )
        elif self.request.GET.get('mission', ''):
            try:
                int(self.request.GET.get('mission'))
            except ValueError:
                return Response(
                    {'detail': 'Invalid mission id'},
                    status=status.HTTP_400_BAD_REQUEST
                )
        elif not self.request.GET.get('query', ''):
            return Response(
                {
                    'detail': 'Please specify a parameter'
                              ' (user, mission or query)'
                }, status=status.HTTP_400_BAD_REQUEST
            )
        return super(SearchVideo, self).get(request, *args, **kwargs)
