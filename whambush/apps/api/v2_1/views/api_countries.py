from django.conf import settings

from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from ..serializers import (
    CountrySerializer
)
from ..services import get_country_object
from whambush.apps.country.models import SupportedCountry

import operator


@permission_classes((IsAuthenticated, ))
class ListCountry(generics.ListAPIView):
    model = SupportedCountry
    serializer_class = CountrySerializer

    def get_queryset(self):
        country_type = self.request.GET.get('type', 'all')
        if country_type == 'supported':
            return SupportedCountry.objects.all()
        else:
            countries = []
            for country in settings.COUNTRY_CHOICES:
                countries.append(get_country_object(country[0]))
            return sorted(
                countries,
                key=operator.attrgetter('supported'),
                reverse=True
            )
