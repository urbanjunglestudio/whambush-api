from rest_framework.authentication import TokenAuthentication
from rest_framework import exceptions

from whambush.apps.accounts.models import WhamUser

import datetime


class ExpiringTokenAuthentication(TokenAuthentication):
    def authenticate_credentials(self, key):
        try:
            token = self.model.objects.get(key=key)
        except self.model.DoesNotExist:
            raise exceptions.AuthenticationFailed(
                'TOKEN_ERROR_INVALID_TOKEN'
            )

        active, status = token.user.activation_status
        if not active:
            if status == WhamUser.STATUS_INACTIVE:
                if token.user.linked_user != token.user:
                    raise exceptions.AuthenticationFailed(
                        'TOKEN_ERROR_INACTIVE_USER'
                    )
            elif status == WhamUser.STATUS_DEACTIVATED:
                raise exceptions.AuthenticationFailed(
                    'TOKEN_ERROR_BANNED_USER'
                )

        utc_now = datetime.datetime.utcnow()
        if token.created < (utc_now - datetime.timedelta(hours=48)):
            raise exceptions.AuthenticationFailed(
                'TOKEN_ERROR_EXPIRED_TOKEN'
            )

        return (token.user, token)
