from rest_framework import permissions


class IsOwnerOrReadOnly(permissions.BasePermission):
    '''
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `added_by` attribute.
    '''

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return True

        # Instance must have an attribute named `added_by`.
        return obj.added_by == request.user


class ListCreateUserPermission(permissions.BasePermission):
    '''
    Permissions for User model
    '''

    def has_permission(self, request, view):
        # only Admin can use GET (list users)
        if request.method == 'GET':
            return request.user.is_superuser
        # anyone can POST (register user)
        elif request.method == 'POST':
            return True
        else:
            return False


class IsAdminUserOrReadOnly(permissions.BasePermission):
    '''
    The request is authenticated as an admin user, or is a read-only request.
    '''

    def has_permission(self, request, view):
        return (request.method in permissions.SAFE_METHODS or
            request.user and
            request.user.is_authenticated() and
            request.user.is_superuser)


class CanCreateMissionOrReadOnly(permissions.BasePermission):

    def has_permission(self, request, view):
        return (
            request.method in permissions.SAFE_METHODS or
            request.user and
            request.user.is_authenticated() and
            (
                request.user.is_superuser or
                request.user.user_type == 3
            )
        )

