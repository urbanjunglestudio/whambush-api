from django.utils.timezone import now as django_now

from rest_framework import status
from rest_framework.decorators import (
    api_view, parser_classes, renderer_classes
)
from rest_framework.parsers import XMLParser
from rest_framework.renderers import XMLRenderer
from rest_framework.response import Response

from whambush.apps.videos.models import Video
from whambush.libs.utils import get_VZAAR_video_detail, send_alert_email

from whambush.apps.activities.signals import action
from xml.dom.minidom import parseString
import logging


@api_view(['POST'])
@parser_classes((XMLParser,))
@renderer_classes((XMLRenderer,))
def vzaar_webhook(request):
    '''
    Receive notification call from vzaar
    '''
    logger = logging.getLogger('whambush_log')
    logger.debug('{} - vzaar request:\n{}'.format(
        django_now(), request.body)
    )
    if request.META.get('CONTENT_TYPE') == 'application/xml':
        video_id = request.DATA.get('video').get('id')
    else:
        req_xml = parseString(request.body)
        video_id = (
            req_xml.getElementsByTagName('id').item(0).firstChild.nodeValue
        )

    try:
        video = Video.objects.get(external_id=video_id)
    except Video.DoesNotExist:
        logger.debug(
            '{} - video id {} not found. Returning 400.'.format(
                django_now(), video_id
            )
        )
        send_alert_email('VZAAR_ERROR', {
            'vzaar_request': request.body,
            'error': '{} - video id {} not found. Returning 400.'.format(
                django_now(), video_id
            )
        })

        return Response(
            {"detail": "Video not found."},
            status=status.HTTP_400_BAD_REQUEST
        )

    if video.published_at:
        logger.debug(
            'Video is already published - Returning 200.'.format(
                django_now()
            )
        )
        if not video.is_processed:
            video.is_processed = True
            video.save()
            action.send(
                video.added_by,
                verb='uploaded',
                target=video
            )
        return Response(status=status.HTTP_200_OK)

    video_detail_xml = get_VZAAR_video_detail(video_id)
    if video_detail_xml.getElementsByTagName('error').item(0) is None:
        video.is_processed = True
        video.published_at = django_now()
        video.save()

        # save action uploaded
        action.send(
            video.added_by,
            verb='uploaded',
            target=video
        )
    else:
        logger.debug(
            '{} - error getting video details from vzaar. '
            'Video will not be updated:{}'.format(
                django_now(),
                video_detail_xml.toxml()
            )
        )
        send_alert_email('VZAAR_ERROR', {
            'vzaar_request': request.body,
            'error': '{} - error getting video {} from vzaar.'.format(
                django_now(), video_id
            )
        })

    logger.debug(' - Returning 200.'.format(django_now()))
    return Response(status=status.HTTP_200_OK)
