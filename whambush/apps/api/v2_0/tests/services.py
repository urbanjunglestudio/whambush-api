from django.core.urlresolvers import reverse

from whambush.apps.activities.models import Activity


def register(client, username, password1, password2, email, country):
    return client.post(reverse('api_v2_0:list-create-user'), {
        'username': username,
        'email': email,
        'password1': password1,
        'password2': password2,
        'country': country,
    })


def login(client, username, password, device_info=None, guest_id=''):
    data = {
        'username': username,
        'password': password,
        'guest_id': guest_id,
    }
    if device_info:
        data.update(device_info)
    return client.post(reverse('api_v2_0:token-auth'), data)


def set_token(client, token):
    client.defaults['HTTP_AUTHORIZATION'] = 'Token %s' % token


def reverse_create_comment(video_id):
    return reverse(
        'api_v2_0:list-create-video-comment',
        kwargs={'video_id': video_id}
    )


def reverse_like_video():
    return reverse('api_v2_0:like-video')


def reverse_unlike_video():
    return reverse('api_v2_0:unlike-video')


def reverse_dislike_video():
    return reverse('api_v2_0:dislike-video')


def reverse_undislike_video():
    return reverse('api_v2_0:undislike-video')


def reverse_follow():
    return reverse('api_v2_0:whamuser_follow')


def reverse_unfollow(user_id):
    return reverse('api_v2_0:whamuser_unfollow', kwargs={'user_id': user_id})


def reverse_list_videos(feed_type='', country=''):
    return '{}?type={}&country={}'.format(
        reverse('api_v2_0:list-create-video'), feed_type, country
    )


def get_latest_activity():
    return Activity.objects.latest('id')
