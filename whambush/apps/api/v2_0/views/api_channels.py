from rest_framework import generics
from rest_framework.decorators import permission_classes
from rest_framework.permissions import IsAuthenticated

from ..serializers import ChannelSerializer

from whambush.apps.channels.models import Channel


@permission_classes((IsAuthenticated, ))
class ListChannel(generics.ListAPIView):
    model = Channel
    serializer_class = ChannelSerializer
    queryset = Channel.objects.filter(parent=None)


@permission_classes((IsAuthenticated, ))
class RetrieveChannel(generics.RetrieveAPIView):
    model = Channel
    serializer_class = ChannelSerializer
