from rest_framework import generics
from rest_framework.permissions import IsAuthenticated
from rest_framework.decorators import permission_classes

from ..serializers import MissionSerializer
from ..services import (
    get_country_filter
)
from ...permissions import IsAdminUserOrReadOnly
from whambush.apps.missions.models import Mission


@permission_classes((IsAuthenticated, IsAdminUserOrReadOnly))
class ListMission(generics.ListCreateAPIView):
    model = Mission
    serializer_class = MissionSerializer

    def get_queryset(self):
        filter_type = self.request.GET.get('type')
        countries = get_country_filter(self.request)
        if filter_type == 'old':
            # old missions
            return (
                Mission.objects.old().country_filter(countries).wb_order()
            )
        else:
            # active missions
            return (
                Mission.objects.active().country_filter(countries)
                .wb_order(True)
            )

    def pre_save(self, obj):
        if not self.request.DATA.get('added_by'):
            obj.added_by = self.request.user


@permission_classes((IsAuthenticated, ))
class MissionDetail(generics.RetrieveAPIView):
    model = Mission
    serializer_class = MissionSerializer
