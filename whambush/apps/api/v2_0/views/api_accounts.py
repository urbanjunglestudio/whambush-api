from django.utils.timezone import now as django_now
from django.conf import settings
from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.decorators import permission_classes
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authentication import Token

from ..serializers import (
    UserSerializer, UserUpdateSerializer, FollowSerializer,
    WhamAuthTokenSerializer, SectionSerializer
)
from ..services import verify_promo_code
from whambush.apps.api.permissions import ListCreateUserPermission
from whambush.apps.accounts.models import WhamUser, Follow
from whambush.apps.accounts.forms import UserCreationForm
from whambush.apps.missions.models import Mission
from whambush.apps.videos.models import Video
from whambush.apps.activities.models import Activity
from whambush.apps.settings.models import Section
from whambush.libs.utils import clone_REST_request

from constance import config
from whambush.apps.activities.signals import action

from datetime import datetime


class ObtainExpiringAuthToken(ObtainAuthToken):
    '''
    1.0 Authentication
    '''
    serializer_class = WhamAuthTokenSerializer

    def _get_token_and_update_stream(self, user, request):
        token, created = Token.objects.get_or_create(user=user)
        if not created:
            # update the created time of the token to keep it valid
            token.created = datetime.utcnow()
            token.save()

        # update last login time
        user.last_login = django_now()
        user.save()

        # update action stream
        action.send(
            user,
            verb='logged in',
            device=request.DATA.get('device', ''),
            manufacturer=request.DATA.get('manufacturer', ''),
            os=request.DATA.get('os', ''),
            os_version=request.DATA.get('os_version', ''),
            whambush_version=request.DATA.get('whambush_version', ''),
        )

        return token

    def post(self, request):
        username = request.DATA.get('username', '')
        password = request.DATA.get('password', '')
        guest_id = request.DATA.get('guest_id', '')

        vzaar_settings = {
            'vzaar_key': settings.VZAAR_API['secret'],
            'vzaar_secret': settings.VZAAR_API['key'],
        }
        general_settings = []
        for st in Section.objects.all():
            general_settings.append(SectionSerializer(
                st, context={'request': request}
            ).data)
        vzaar_settings.update({'general': general_settings})

        if username == password == '':
            if guest_id == '':
                # if all are empty, then create guest
                user = WhamUser.objects.create_guest()
            else:
                # if there is guest ID, then return token
                try:
                    user = WhamUser.objects.get(pk=guest_id)
                except WhamUser.DoesNotExist:
                    return Response({
                        'guest_id': 'This guest ID does not exist'
                    }, status=status.HTTP_400_BAD_REQUEST)
                # if guest_id = user.id
                user.linked_user = user

            token = self._get_token_and_update_stream(user, request)
            return Response({
                'token': token.key,
                'guest_id': user.id,
                'is_guest': True,
                'settings': vzaar_settings
            }, status=status.HTTP_200_OK)
        else:
            serializer = self.serializer_class(data=request.DATA)
            if serializer.is_valid():
                user = serializer.object['user']
                token = self._get_token_and_update_stream(user, request)

                # link guest to user if there is multiple guest_id for 1 user
                if guest_id != '' and guest_id != user.id:
                    try:
                        guest = WhamUser.objects.get(pk=guest_id)
                    except:
                        pass
                    else:
                        guest.linked_user = user
                        guest.save()

                # get numbers of active missions for current user
                # TODO: possible slow query
                num_active_missions = Mission.objects.active().country_filter(
                    [user.country, settings.GLOBAL_COUNTRY_CODE]
                ).exclude(
                    id__in=Video.objects.exclude(mission=None).filter(
                        added_by=user,
                        deleted_at=None,
                        is_active=True
                    ).values_list(
                        'mission_id',
                        flat=True
                    )
                ).count()

                # check incomplete variables in user profile
                incomplete = 0
                if not user.description:
                    incomplete += 1
                if not user.birthday:
                    incomplete += 1

                # get unread activities
                unread = Activity.objects.wb_stream(user).filter(
                    is_read=False
                ).count()

                return Response({
                    'token': token.key,
                    'guest_id': guest_id or user.id,
                    'is_guest': False,
                    'user': UserSerializer(
                        user, context={'request': request}
                    ).data,
                    'num_active_missions': num_active_missions,
                    'incomplete': incomplete,
                    'unread': unread,
                    'settings': vzaar_settings
                }, status=status.HTTP_200_OK)
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )


@permission_classes((ListCreateUserPermission, ))
class ListCreateUser(generics.ListCreateAPIView):
    model = WhamUser
    serializer_class = UserSerializer

    def post(self, request, *args, **kwargs):
        form = UserCreationForm(request.DATA)
        if form.is_valid():
            # verify promo code if in beta phase
            if config.IS_BETA:
                code = request.DATA.get('promocode')
                is_ok, error_detail = verify_promo_code(code)
                if not is_ok:
                    return Response(
                        error_detail, status=status.HTTP_400_BAD_REQUEST
                    )
            # save user
            form.save()
            # get the new user detail and return
            new_user = WhamUser.objects.get(
                username=form.cleaned_data['username']
            )
            return Response(
                UserSerializer(new_user, context={'request': request}).data,
                status=status.HTTP_201_CREATED
            )
        else:  # return the form errors along with 400 status
            return Response(form._errors, status=status.HTTP_400_BAD_REQUEST)


@permission_classes((IsAuthenticated, ))
class RetrieveUpdateUser(generics.RetrieveUpdateAPIView):
    model = WhamUser

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return UserSerializer
        elif self.request.method == 'PUT':
            return UserUpdateSerializer


@permission_classes((IsAuthenticated, ))
class ListCreateUserFollow(generics.ListCreateAPIView):
    def get_serializer_class(self):
        if self.request.method == 'GET':
            return UserSerializer
        elif self.request.method == 'POST':
            return FollowSerializer

    def get_queryset(self):
        f_type = self.request.GET.get('type')
        user_id = self.request.GET.get('user')
        if user_id is not None and user_id:
            f_user = user_id
        else:
            f_user = self.request.user
        if f_type == 'following':
            return WhamUser.objects.filter(
                pk__in=Follow.objects.filter(from_user=f_user)
                .values_list('to_user', flat=True)
            )
        elif f_type == 'followers':
            return WhamUser.objects.filter(
                pk__in=Follow.objects.filter(to_user=f_user)
                .values_list('from_user', flat=True)
            )
        else:
            return WhamUser.objects.none()

    def post(self, request, *args, **kwargs):
        req = clone_REST_request(request)
        req._data.update({
            'from_user': reverse(
                'api_v2_0:whamuser-detail', kwargs={'pk': request.user.id}
            ),
            'to_user': reverse(
                'api_v2_0:whamuser-detail',
                kwargs={'pk': request.DATA.get('to_user')}
            )
        })
        return super(ListCreateUserFollow, self).post(req, *args, **kwargs)


@permission_classes((IsAuthenticated, ))
class RemoveUserFollow(generics.DestroyAPIView):
    model = Follow
    serializer_class = FollowSerializer

    def delete(self, request, *args, **kwargs):
        user_id = kwargs.pop('user_id')
        try:
            f = Follow.objects.get(from_user=request.user, to_user=user_id)
            f.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Follow.DoesNotExist:
            return Response(
                {'detail': 'Not found'},
                status=status.HTTP_404_NOT_FOUND
            )


@permission_classes((IsAuthenticated, ))
class SearchUser(generics.ListAPIView):
    model = WhamUser
    serializer_class = UserSerializer

    def get_queryset(self):
        if self.request.GET.get('query', ''):
            search_query = self.request.GET.get('query', '')
            fields = self.request.GET.get('fields', '')
            if fields == '':
                return WhamUser.objects.search(search_query)
            else:
                return WhamUser.objects.search_by_field(search_query, fields)
        else:
            return WhamUser.objects.none()

    def get(self, request, *args, **kwargs):
        if not self.request.GET.get('query', ''):
            return Response({
                'detail': 'Please specify your query'
            }, status=status.HTTP_400_BAD_REQUEST
            )
        return super(SearchUser, self).get(request, *args, **kwargs)
