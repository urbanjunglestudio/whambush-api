from rest_framework import generics
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.decorators import permission_classes

from ..serializers import SectionSerializer, UserSettingSerializer
from whambush.apps.settings.models import Section, UserSetting


@permission_classes((IsAdminUser, ))
class ListGeneralSetting(generics.ListAPIView):
    model = Section
    serializer_class = SectionSerializer


@permission_classes((IsAuthenticated, ))
class ListCreateUserNotificationSetting(generics.ListCreateAPIView):
    model = UserSetting
    serializer_class = UserSettingSerializer

    def get_queryset(self):
        qs = super(ListCreateUserNotificationSetting, self).get_queryset()
        qs = qs.filter(setting_type=0, user=self.request.user)
        return qs

    def post(self, request, *args, **kwargs):
        settings = UserSetting.objects.filter(
            user=request.user, setting_type=0, key=request.DATA.get('key')
        )
        if settings:
            settings.delete()
        return super(ListCreateUserNotificationSetting, self).post(
            request, *args, **kwargs
        )

    def pre_save(self, obj, created=False):
        if obj:
            obj.user = self.request.user
