from django.conf.urls import patterns, url, include

from .views import (
    api_accounts, api_videos, api_missions, api_comments, api_devices,
    api_activities, api_bananas, api_channels, api_countries, api_settings,
    api_general_data
)


urlpatterns = patterns(
    '',
    url(r'^auth/',
        include('rest_framework.urls', namespace='rest_framework')),

    # auth and register
    url(r'^login/$',
        api_accounts.ObtainExpiringAuthToken.as_view(),
        name='token-auth'),

    # user
    url(r'^users/$',
        api_accounts.ListCreateUser.as_view(),
        name='list-create-user'),
    url(r'^users/(?P<pk>\d+)/$',
        api_accounts.RetrieveUpdateUser.as_view(),
        name='whamuser-detail'),

    # follow
    url(r'^follows/$',
        api_accounts.ListCreateUserFollow.as_view(),
        name='whamuser_follow'),
    url(r'^follows/(?P<user_id>\d+)/$',
        api_accounts.RemoveUserFollow.as_view(),
        name='whamuser_unfollow'),

    # video
    url(r'^videos/$',
        api_videos.ListCreateVideo.as_view(),
        name='list-create-video'),
    url(r'^videos/(?P<pk>\d+)/$',
        api_videos.RetrieveDestroyVideo.as_view(),
        name='video-detail'),
    url(r'^videos/slug/(?P<slug>[A-Za-z0-9-_]{1,50})/$',
        api_videos.RetrieveDestroyVideo.as_view(),
        name='video-detail-slug'),

    # flag video
    url(r'^flags/video/(?P<video_id>\d+)/$',
        api_videos.ListCreateVideoFlag.as_view(),
        name='list-create-flag-video'),
    url(r'^flags/(?P<pk>\d+)/$',
        api_videos.RetrieveDestroyFlag.as_view(),
        name='flag-detail'),

    url(r'^feeds/$',
        api_videos.ListVideoFeed.as_view(),
        name='list-video-feed'),

    # mission
    url(r'^missions/$',
        api_missions.ListMission.as_view(),
        name='list-missions'),
    url(r'^missions/(?P<pk>\d+)/$',
        api_missions.MissionDetail.as_view(),
        name='mission-detail'),

    # search
    url(r'^search/videos/$',
        api_videos.SearchVideo.as_view(),
        name='search-video'),
    url(r'^search/users/$',
        api_accounts.SearchUser.as_view(),
        name='search-user'),

    # like
    url(r'^like/video/$',
        'whambush.apps.api.v2_0.views.api_likes.like_video',
        {'point': 1}, name='like-video'),
    url(r'^unlike/video/$',
        'whambush.apps.api.v2_0.views.api_likes.unlike_video', {'point': 1},
        name='unlike-video'),
    url(r'^dislike/video/$',
        'whambush.apps.api.v2_0.views.api_likes.like_video', {'point': -1},
        name='dislike-video'),
    url(r'^undislike/video/$',
        'whambush.apps.api.v2_0.views.api_likes.unlike_video', {'point': -1},
        name='undislike-video'),
    url(r'^likes/video/(?P<pk>\d+)/$',
        'whambush.apps.api.v2_0.views.api_likes.likes_on_video',
        name='get-likes'),

    # comment
    url(r'^comments/video/(?P<video_id>\d+)/$',
        api_comments.ListCreateVideoComment.as_view(),
        name='list-create-video-comment'),
    url(r'^comments/(?P<pk>\d+)/$',
        api_comments.VideoCommentDetail.as_view(),
        name='video-comment-detail'),

    # flag comment
    url(r'^flags/comment/(?P<comment_id>\d+)/$',
        api_comments.ListCreateCommentFlag.as_view(),
        name='list-create-flag-comment'),

    # devices
    url(r'^devices/$',
        api_devices.CreateDestroyDevice.as_view(),
        name='create-destroy-device'),

    # activities
    url(r'^activities/$',
        api_activities.ListActivity.as_view(),
        name='list-activities'),
    url(r'^activities/read/$',
        'whambush.apps.api.v2_0.views.api_activities.read',
        name='read-activities'),
    url(r'^kpi/$',
        api_activities.ListAllActivity.as_view(),
        name='list-all-activities'),

    # bananas
    url(r'^bananas/$', api_bananas.ListCreateBananaTransaction.as_view(),
        name='list_bananas_transaction'),

    # channels
    url(r'^channels/$', api_channels.ListChannel.as_view(),
        name='list_channels'),
    url(r'^channels/(?P<pk>\d+)/$', api_channels.RetrieveChannel.as_view(),
        name='retrieve_channel'),

    # countries
    url(r'^countries/$', api_countries.ListCountry.as_view(),
        name='list_countries'),

    # settings (for testing purpose)
    url(r'^settings/$', api_settings.ListGeneralSetting.as_view(),
        name='list_settings'),

    # user notification settings
    url(r'^settings/notifications/$',
        api_settings.ListCreateUserNotificationSetting.as_view(),
        name='list_create_notification_settings'),

    # general data
    url(r'^general-data/$', api_general_data.CreateGeneralData.as_view(),
        name='create_general_data'),
    url(r'^general-data/(?P<key>\w+)/$',
        api_general_data.RetrieveUpdateGeneralData.as_view(),
        name='retrieve_update_general_data'),
)
