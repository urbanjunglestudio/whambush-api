from django.contrib import admin

from .models import Section, GeneralSetting


class SectionAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'created_at',
        'modified_at'
    )


class GeneralSettingAdmin(admin.ModelAdmin):
    list_display = (
        '__str__',
        'section',
        'created_at',
        'modified_at'
    )
    list_filter = ('section',)
    search_fields = ['key', 'value']


admin.site.register(Section, SectionAdmin)
admin.site.register(GeneralSetting, GeneralSettingAdmin)
