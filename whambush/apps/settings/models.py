from django.db import models

from whambush.apps.accounts.models import WhamUser
from whambush.libs.models import TimeStampedModel


class Section(TimeStampedModel):
    name = models.CharField(max_length=255, unique=True)

    def __unicode__(self):
        return self.name


class GeneralSetting(TimeStampedModel):
    section = models.ForeignKey(Section, related_name='settings')
    key = models.CharField(max_length=255, unique=True)
    value = models.TextField(blank=True, default='')

    def __unicode__(self):
        return u'{0}: {1}'.format(self.key, self.value)


class UserSetting(TimeStampedModel):
    user = models.ForeignKey(WhamUser)
    key = models.CharField(max_length=255, unique=True)
    value = models.TextField(blank=True, default='')
    setting_type = models.IntegerField(blank=True, default=0)

    def __unicode__(self):
        return u'{0}: {1}'.format(self.key, self.value)
