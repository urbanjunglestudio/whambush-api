# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PromotionKey.created_at'
        db.add_column(u'promotions_promotionkey', 'created_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.datetime(2014, 3, 29, 0, 0), blank=True),
                      keep_default=False)

        # Adding field 'PromotionKey.modified_at'
        db.add_column(u'promotions_promotionkey', 'modified_at',
                      self.gf('django.db.models.fields.DateTimeField')(auto_now=True, default=datetime.datetime(2014, 3, 29, 0, 0), blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PromotionKey.created_at'
        db.delete_column(u'promotions_promotionkey', 'created_at')

        # Deleting field 'PromotionKey.modified_at'
        db.delete_column(u'promotions_promotionkey', 'modified_at')


    models = {
        u'promotions.promotionkey': {
            'Meta': {'object_name': 'PromotionKey'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_used': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'modified_at': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['promotions']