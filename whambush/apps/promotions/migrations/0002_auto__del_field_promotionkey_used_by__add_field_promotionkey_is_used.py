# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'PromotionKey.used_by'
        db.delete_column(u'promotions_promotionkey', 'used_by_id')

        # Adding field 'PromotionKey.is_used'
        db.add_column(u'promotions_promotionkey', 'is_used',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'PromotionKey.used_by'
        db.add_column(u'promotions_promotionkey', 'used_by',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['accounts.WhamUser'], null=True, blank=True),
                      keep_default=False)

        # Deleting field 'PromotionKey.is_used'
        db.delete_column(u'promotions_promotionkey', 'is_used')


    models = {
        u'promotions.promotion': {
            'Meta': {'object_name': 'Promotion'},
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'end_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '180'}),
            'start_at': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'})
        },
        u'promotions.promotionkey': {
            'Meta': {'object_name': 'PromotionKey'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_used': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'promotion': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['promotions.Promotion']"})
        }
    }

    complete_apps = ['promotions']