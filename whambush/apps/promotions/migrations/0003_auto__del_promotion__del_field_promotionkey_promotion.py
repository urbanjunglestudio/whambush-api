# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Promotion'
        db.delete_table(u'promotions_promotion')

        # Deleting field 'PromotionKey.promotion'
        db.delete_column(u'promotions_promotionkey', 'promotion_id')


    def backwards(self, orm):
        # Adding model 'Promotion'
        db.create_table(u'promotions_promotion', (
            ('start_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('end_at', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=180)),
        ))
        db.send_create_signal(u'promotions', ['Promotion'])

        # Adding field 'PromotionKey.promotion'
        db.add_column(u'promotions_promotionkey', 'promotion',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['promotions.Promotion']),
                      keep_default=False)


    models = {
        u'promotions.promotionkey': {
            'Meta': {'object_name': 'PromotionKey'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_used': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['promotions']