import string
from django.db import models

from .services import generate_random_key
from whambush.libs.models import TimeStampedModel


class PromotionKey(TimeStampedModel):
    key = models.CharField(max_length=20)
    is_used = models.BooleanField(default=False)

    def __unicode__(self):
        return self.key

    def delete(self, using=None):
        if self.is_used is not None:
            raise Exception("Can not delete an used key!")
        else:
            super(PromotionKey, self).delete(using=using)

    def generate_key(self, size=6, chars=string.digits):
        try:
            keys = PromotionKey.objects.all()
            new_key = generate_random_key(size, chars)
            while True:
                exists = False
                for old_key in keys:
                    if new_key == old_key.key:
                        exists = True
                        new_key = generate_random_key(size, chars)
                        break
                if not exists:
                    break
            self.key = new_key
        except PromotionKey.DoesNotExist:
            self.key = generate_random_key(size, chars)
