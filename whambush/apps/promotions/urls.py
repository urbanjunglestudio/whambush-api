from django.conf.urls import patterns, url


''' Promotions URL patterns '''
urlpatterns = patterns(
    '',
    url(r'^export_keys/$',
        'whambush.apps.promotions.views.export_keys', name='export_keys'),
)
