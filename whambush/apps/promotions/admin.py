from django.contrib import admin, messages
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext as _

from .models import PromotionKey


class PromotionKeyAdmin(admin.ModelAdmin):
    list_display = (
        'key', 'is_used'
    )
    fields = (
        'key', 'is_used'
    )
    search_fields = ['key']
    actions = [
        'generate_ten_keys',
        'generate_twenty_keys',
        'generate_fifty_keys',
        'generate_onehundred_keys',
        'export_selected_keys',
    ]

    def response_action(self, request, queryset):
        selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
        data = request.POST.copy()
        if not selected and data['action'] in self.actions:
            action_form = self.action_form(data, auto_id=None)
            action_form.fields['action'].choices =\
                self.get_action_choices(request)
            if action_form.is_valid():
                action = action_form.cleaned_data['action']
                func, name, description = self.get_actions(request)[action]
                response = func(self, request, queryset)
                if isinstance(response, HttpResponse):
                    return response
                else:
                    return HttpResponseRedirect(request.get_full_path())
        return super(PromotionKeyAdmin, self).response_action(request,
                                                              queryset)

    def changelist_view(self, request, extra_context=None):
        if request.method == "POST":
            selected = request.POST.getlist(admin.ACTION_CHECKBOX_NAME)
            data = request.POST.copy()
            if not selected and data['action'] in self.actions\
                    and data['action'] != 'export_selected_keys':
                return self.response_action(request, queryset=None)
        return super(
            PromotionKeyAdmin, self
        ).changelist_view(request, extra_context)

    def generate_keys(self, request, queryset, number):
        try:
            count = 0
            for i in range(number):
                new_key = PromotionKey()
                new_key.generate_key()
                new_key.save()
                count += 1
            self.message_user(
                request,
                '%s keys has been generated successful' % count
            )
        except Exception as ex:
            self.message_user(request, ex.message, level=messages.ERROR)

    def generate_ten_keys(self, request, queryset):
        self.generate_keys(request, queryset, 10)

    def generate_twenty_keys(self, request, queryset):
        self.generate_keys(request, queryset, 20)

    def generate_fifty_keys(self, request, queryset):
        self.generate_keys(request, queryset, 50)

    def generate_onehundred_keys(self, request, queryset):
        self.generate_keys(request, queryset, 100)

    def export_selected_keys(self, request, queryset):
        selected = []
        for key in queryset:
            selected.append(key.key)
        return HttpResponseRedirect(
            reverse('export_keys') + "?keys=%s" % (",".join(selected)))

    generate_ten_keys.short_description = _('Generate 10 keys')
    generate_twenty_keys.short_description = _('Generate 20 keys')
    generate_fifty_keys.short_description = _('Generate 50 keys')
    generate_onehundred_keys.short_description = _('Generate 100 keys')
    export_selected_keys.short_description = _('Export selected keys')

admin.site.register(PromotionKey, PromotionKeyAdmin)
