from annoying.decorators import render_to


@render_to('promotions/export_keys.html')
def export_keys(request):
    keys = request.GET.get('keys').split(',')
    return {
        'keys': keys
    }
