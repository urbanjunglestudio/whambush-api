import string
import random


def generate_random_key(size=6, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))
