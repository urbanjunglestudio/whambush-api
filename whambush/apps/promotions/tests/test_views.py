from django.test import TestCase
from django.core.urlresolvers import reverse


class PromotionViewTestCases(TestCase):
    def _test_export_keys(self):
        path = reverse('export_keys') + '?keys=123,456'
        resp = self.client.get(path)
        self.assertIn('123', resp.content)
