from django.contrib import admin
from django.contrib.comments.models import Comment
from django.contrib.comments.admin import CommentsAdmin
from django.utils.html import strip_tags

from .models import Video, VideoFeed, FeaturedVideos, Flag
from .filters import FlagCountListFilter


def deactivate_videos(model_admin, request, queryset):
    for video in queryset:
        video.is_active = False
        video.save()
    model_admin.message_user(
        request, '%s video(s) are deactivated.' % queryset.count()
    )


def activate_videos(model_admin, request, queryset):
    for video in queryset:
        video.is_active = True
        video.save()
    model_admin.message_user(
        request, '%s video(s) are activated.' % queryset.count()
    )


class VideoAdmin(admin.ModelAdmin):
    list_display = [
        'name',
        'hashtags',
        'external_id',
        'public_url',
        'mission',
        'rank',
        'country',
        'is_processed',
        'is_active',
        'is_mission_winner',
        'added_by',
        'video_type',
        'is_deleted'
    ]
    search_fields = ['name', 'description', 'is_processed']
    actions = [activate_videos, deactivate_videos]
    list_filter = [
        'video_type', 'is_active', 'is_processed', 'deleted_at', 'country'
    ]

    def public_url(self, obj):
        return ('<a href="{}" target="_blank">View</a>'.format(
            obj.web_url
        ))
    public_url.allow_tags = True

    def hashtags(self, obj):
        return ', '.join([t.name for t in obj.tags.all()])


class VideoFeedAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'endpoint_url',
        'order',
        'id',
        'version',
        'is_default',
    )
    readonly_fields = ['id']
    search_fields = ['name', 'endpoint_url']


class FeaturedVideosAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class FlaggedVideoAdmin(admin.ModelAdmin):
    def flag_count(self, obj):
        return obj.flag_count()

    def public_url(self, obj):
        return ('<a href="{}" target="_blank">View</a>'.format(
            obj.web_url
        ))
    public_url.allow_tags = True

    list_display = (
        'name',
        'description',
        'public_url',
        'is_active',
        'added_by',
        'created_at',
        'flag_count'
    )
    search_fields = ['name', 'description']
    list_filter = (FlagCountListFilter,)
    actions = (activate_videos, deactivate_videos,)

    def queryset(self, request):
        qs = super(FlaggedVideoAdmin, self).queryset(request)
        qs = qs.extra(
            where=[
                '(SELECT COUNT(*) FROM videos_flag WHERE '
                'videos_flag.object_pk = videos_video.id AND '
                'videos_flag.flag_type = 0) > 0'
            ]
        )
        return qs

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class FlaggedVideo(Video):
    def flag_count(self):
        return Flag.objects.filter(
            object_pk=self.id,
            flag_type=0
        ).count()

    class Meta:
        proxy = True


class FlaggedCommentAdmin(CommentsAdmin):
    def flag_count(self, obj):
        return obj.flag_count()

    def comment_content(self, obj):
        return strip_tags(obj.comment)

    list_display = (
        'name', 'comment_content', 'content_type', 'object_pk', 'ip_address',
        'submit_date', 'is_public', 'is_removed', 'flag_count'
    )

    '''
    In this view user should only be able to approve or remove comments
    '''
    def get_actions(self, request):
        actions = super(CommentsAdmin, self).get_actions(request)
        if 'delete_selected' in actions:
            actions.pop('delete_selected')
        if 'flag_comments' in actions:
            actions.pop('flag_comments')
        if not request.user.has_perm('comments.can_moderate'):
            if 'approve_comments' in actions:
                actions.pop('approve_comments')
            if 'remove_comments' in actions:
                actions.pop('remove_comments')
        return actions

    def queryset(self, request):
        qs = super(FlaggedCommentAdmin, self).queryset(request)
        qs = qs.extra(
            where=[
                '(SELECT COUNT(*) FROM videos_flag WHERE '
                'videos_flag.object_pk = django_comments.id AND '
                'videos_flag.flag_type = 1) > 0'
            ]
        )
        return qs

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class FlaggedComment(Comment):
    def flag_count(self):
        return Flag.objects.filter(
            object_pk=self.id,
            flag_type=1
        ).count()

    class Meta:
        proxy = True

admin.site.register(Video, VideoAdmin)
admin.site.register(VideoFeed, VideoFeedAdmin)
admin.site.register(FeaturedVideos, FeaturedVideosAdmin)
admin.site.register(FlaggedVideo, FlaggedVideoAdmin)
admin.site.register(FlaggedComment, FlaggedCommentAdmin)
