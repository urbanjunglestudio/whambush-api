# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Video.created_at'
        db.alter_column(u'videos_video', 'created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True))

        # Changing field 'Video.video'
        db.alter_column(u'videos_video', 'video', self.gf('django.db.models.fields.CharField')(max_length=255))

    def backwards(self, orm):

        # Changing field 'Video.created_at'
        db.alter_column(u'videos_video', 'created_at', self.gf('django.db.models.fields.DateTimeField')())

        # Changing field 'Video.video'
        db.alter_column(u'videos_video', 'video', self.gf('django.db.models.fields.files.FileField')(max_length=100))

    models = {
        u'videos.video': {
            'Meta': {'object_name': 'Video'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'video': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['videos']