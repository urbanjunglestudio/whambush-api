from __future__ import absolute_import
from django.db.models import Sum, Count
from django.utils.timezone import now
from django.conf import settings
from django.contrib.comments.models import Comment
from django.contrib.contenttypes.models import ContentType

from whambush.apps.videos.models import Video
from whambush.apps.missions.models import Mission
from whambush.apps.likes.models import Like
from whambush.libs.utils import get_oauth_session, find_dict_with_val

from celery import shared_task
from operator import itemgetter
import json


@shared_task
def update_video_view_count():
    session = get_oauth_session()
    page = 1
    while True:
        url = 'http://vzaar.com/api/{}/videos.json?page={}'.format(
            settings.VZAAR_API['key'], page
        )
        videos = json.loads(session.get(url).content)
        if not videos:
            break
        for video in videos:
            ext_id = video.get('id')
            view_count = video.get('play_count', 0)
            Video.objects.filter(external_id=ext_id).update(
                view_count=view_count
            )
        page += 1


@shared_task
def update_videos_rank(mission_queryset=None):
    # get unranked mission
    qs = mission_queryset
    if qs is None:
        qs = Mission.objects.filter(
            end_at__lte=now(),
            is_videos_ranked=False
        )
    unranked_missions = qs.fetch_reverse_relations(
        'videos', Video, 'mission', is_active=True
    )
    for mission in unranked_missions:
        # get a dict of mission videos
        vid_dict = dict([(video.id, video) for video in mission.videos])

        # construct a new list to sort later
        unsorted_videos = [{
            'id': video.id,
            'point_sum': 0,
            'like_count': 0,
            'dislike_count': 0,
            'comment_count': 0,
            'created_at': video.created_at
        } for video in mission.videos]

        # get likes and dislikes count
        likes_qs = list(
            Like.objects
            .filter(
                video__in=vid_dict.keys(),
                created_at__gte=mission.start_at,
                created_at__lte=mission.end_at
            )
            .values('video', 'point')
            .annotate(count=Count('id'), sum=Sum('point'))
        )
        # get comment count
        comments_qs = list(
            Comment.objects
            .filter(
                object_pk__in=vid_dict.keys(),
                content_type=ContentType.objects.get_for_model(Video),
                submit_date__gte=mission.start_at,
                submit_date__lte=mission.end_at,
                is_removed=False
            )
            .values('object_pk')
            .annotate(count=Count('id'))
        )

        # loop through unsorted_videos to update the counts based on queryset
        for v in unsorted_videos:
            # find the likes and dislikes out of queryset
            result = find_dict_with_val(likes_qs, 'video', v['id'])
            for r in result:
                # update like count and sum
                if r['point'] == 1:
                    v['like_count'] += r['count']
                    v['point_sum'] += r['sum']
                # update dislike count and sum
                else:
                    v['dislike_count'] -= r['count']  # use - to get negative
                    v['point_sum'] += r['sum']
            # find comments out of queryset
            result = find_dict_with_val(
                comments_qs, 'object_pk', unicode(v['id'])
            )
            for r in result:
                v['comment_count'] += r['count']

        # sort the list
        unsorted_videos.sort(key=itemgetter(
            'point_sum', 'comment_count', 'dislike_count', 'created_at'
        ))

        # update rank for each video
        for i, vid_point in enumerate(unsorted_videos):
            current_video = vid_dict[vid_point['id']]
            current_video.rank = i + 1
            current_video.save()

            # mission winner
            if i == len(unsorted_videos) - 1:
                current_video.is_mission_winner = True
                current_video.save()

        # update mission as ranked
        mission.is_videos_ranked = True
        mission.save()


@shared_task
def publish_videos():
    for video in Video.objects.filter(
        is_active=False,
        published_at__lte=now()
    ):
        video.is_active = True
        video.save()
