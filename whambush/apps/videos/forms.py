from django import forms
from django.contrib.admin.widgets import FilteredSelectMultiple

from .models import Video


class FeaturedVideosAdminForm(forms.ModelForm):
    videos = forms.ModelMultipleChoiceField(
        queryset=Video.objects.filter(is_processed=True),
        widget=FilteredSelectMultiple('Video', False, attrs={'rows': '10'})
    )
