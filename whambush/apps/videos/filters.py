from django.contrib.admin import SimpleListFilter
from django.db.models import Count
from django.utils.translation import ugettext as _

from whambush.apps.videos.models import Flag


class FlagCountListFilter(SimpleListFilter):
    title = _('Flag count')
    parameter_name = 'flag'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        list_tuple = []
        for flagged_count in (
            Flag.objects.filter(flag_type=0).values('object_pk')
            .annotate(flag_count=Count('id'))
            .order_by('flag_count')
            .values('flag_count')
            .distinct()
        ):
            list_tuple.append(
                (flagged_count['flag_count'], flagged_count['flag_count'])
            )
        return list_tuple

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value():
            return (
                queryset.extra(
                    where=[
                        '(SELECT COUNT(*) FROM videos_flag WHERE '
                        'videos_flag.object_pk = videos_video.id AND '
                        'videos_flag.flag_type = 0) = {}'.format(self.value())
                    ]
                )
            )
        else:
            return queryset
