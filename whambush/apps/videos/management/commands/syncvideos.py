from django.core.management.base import BaseCommand
from django.conf import settings

from whambush.libs.utils import get_oauth_session
from whambush.apps.videos.models import Video
from whambush.apps.accounts.models import WhamUser

import json


class Command(BaseCommand):
    def handle(self, *args, **options):
        session = get_oauth_session()
        page = 1
        admin_user = WhamUser.objects.get(pk=1)
        while True:
            url = 'http://vzaar.com/api/{}/videos.json?page={}'.format(
                settings.VZAAR_API['key'], page
            )
            videos = json.loads(session.get(url).content)
            if not videos:
                break
            for video in videos:
                ext_id = video.get('id')
                if Video.objects.filter(external_id=ext_id).count() == 0:
                    v = Video()
                    v.name = video.get('title').replace(
                        '- WHAMBUSH VIDEO', ''
                    ).strip()
                    v.description = video.get('description', '').replace(
                        'www.WHAMBUSH.com', ''
                    ).strip()
                    v.external_id = ext_id
                    v.hosting = 'vzaar'
                    v.thumbnail_url = video.get('thumbnail', '')
                    v.view_count = video.get('play_count', 0)
                    # TODO: check how can we sync "labels" -tags in Whambush
                    v.is_processed = True
                    v.is_active = True
                    v.video_type = 0
                    v.added_by = admin_user
                    v.save()
            page += 1
        self.stdout.write('Successfully synced.')
