from django.test import TestCase
from django.contrib.comments.models import Comment

from ..models import Video
from .. import tasks
from whambush.apps.missions.models import Mission
from whambush.apps.accounts.models import WhamUser
from whambush.apps.likes.models import Like

from datetime import timedelta, datetime


class VideoRankingTestCases(TestCase):
    def setUp(self):
        # clean all objects of previous tests
        WhamUser.objects.all().delete()
        Video.objects.all().delete()

        # make test objects
        for i in range(20):
            WhamUser.objects.create_user(
                id=(i+1),
                username='tester%s' % str(i+1),
                email='tester%s@testserver.com' % str(i+1)
            )
        mission = Mission.objects.create(
            name="Test mission",
            added_by=WhamUser.objects.get(pk=1),
            start_at=datetime.now() - timedelta(days=2),
            end_at=datetime.now() + timedelta(days=2)
        )
        for i in range(8):
            Video.objects.create(
                id=(i+1),
                name="Video %s" % str(i+1),
                external_id=i,
                added_by=WhamUser.objects.get(pk=(i+1)),
                is_processed=True,
                mission=mission,
                view_count=(i+1)*3,
                is_active=True
            )
        Video.objects.filter(pk=6).update(is_active=False)
        # give video #1, #2 & #3 10 likes => video #1 has 10 points
        for i in range(10):
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=1),
                point=1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=2),
                point=1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=3),
                point=1
            )
        # give video #2 & #3 2 dislikes => video #2, #3 have 8 points
        for i in range(2):
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+10)),
                video=Video.objects.get(pk=2),
                point=-1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+10)),
                video=Video.objects.get(pk=3),
                point=-1
            )
        # give video #4 & #5 9 likes
        for i in range(9):
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=4),
                point=1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=5),
                point=1
            )
        # give video #4 & #5 1 dislike => video #4, #5 have 8 points
        Like.objects.create(
            user=WhamUser.objects.get(pk=10),
            video=Video.objects.get(pk=4),
            point=-1
        )
        Like.objects.create(
            user=WhamUser.objects.get(pk=11),
            video=Video.objects.get(pk=5),
            point=-1
        )
        # give video #2 a comment
        Comment.objects.create(
            content_object=Video.objects.get(pk=2),
            user=WhamUser.objects.get(pk=1),
            comment='Test',
            site_id=1
        )
        # give video #7 a comment
        Comment.objects.create(
            content_object=Video.objects.get(pk=7),
            user=WhamUser.objects.get(pk=1),
            comment='Test',
            site_id=1
        )

    def test_ranking_video(self):
        tasks.update_videos_rank(mission_queryset=Mission.objects.all())
        # 10 points => 1st rank
        self.assertEqual(Video.objects.get(pk=1).rank, 7)
        self.assertTrue(Video.objects.get(pk=1).is_mission_winner)
        # 10 likes & 2 dislikes & 1 comment => 2nd rank
        self.assertEqual(Video.objects.get(pk=2).rank, 6)
        self.assertFalse(Video.objects.get(pk=2).is_mission_winner)
        # 9 likes & 1 dislikes => 4th rank, because submitted before
        self.assertEqual(Video.objects.get(pk=4).rank, 4)
        self.assertFalse(Video.objects.get(pk=4).is_mission_winner)
        # 9 likes & 1 dislikes => 3rd rank because submitted later
        self.assertEqual(Video.objects.get(pk=5).rank, 5)
        self.assertFalse(Video.objects.get(pk=5).is_mission_winner)
        # 10 likes & 2 dislikes => 5th rank
        self.assertEqual(Video.objects.get(pk=3).rank, 3)
        self.assertFalse(Video.objects.get(pk=3).is_mission_winner)
        # no rank because inactive
        self.assertEqual(Video.objects.get(pk=6).rank, 0)
        self.assertFalse(Video.objects.get(pk=6).is_mission_winner)
        # 0 point & 1 comment => rank 6th
        self.assertEqual(Video.objects.get(pk=7).rank, 2)
        self.assertFalse(Video.objects.get(pk=7).is_mission_winner)
        # last rank
        self.assertEqual(Video.objects.get(pk=8).rank, 1)
        self.assertFalse(Video.objects.get(pk=8).is_mission_winner)


class VideoTaskTestCases(TestCase):
    def setUp(self):
        self.user = WhamUser.objects.create_user(
            'test_user_1',
            'test1@example.com',
            'test_user_1'
        )
        Video.objects.create(
            pk=1,
            name='test video view 1',
            added_by=self.user,
            external_id=1374109,
            is_processed=True,
            is_active=False,
            published_at=datetime.now() - timedelta(minutes=20)
        )
        Video.objects.create(
            pk=2,
            name='test video view 2',
            added_by=self.user,
            external_id=1374109,
            is_processed=True,
            is_active=False,
            published_at=datetime.now() - timedelta(minutes=10)
        )
        Video.objects.create(
            pk=3,
            name='test video view 3',
            added_by=self.user,
            external_id=1374109,
            is_processed=True,
            is_active=False,
            published_at=datetime.now() + timedelta(minutes=10)
        )

    def test_publish_videos(self):
        tasks.publish_videos()
        self.assertTrue(Video.objects.get(pk=1).is_active)
        self.assertTrue(Video.objects.get(pk=2).is_active)
        self.assertFalse(Video.objects.get(pk=3).is_active)
