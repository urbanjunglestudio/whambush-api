from django.utils.translation import ugettext as _
from django.db import models
from django.db.models.signals import post_save
from django.contrib.comments.models import Comment
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db.models.query import QuerySet
from django.conf import settings
from django.db.models import Q

from whambush.apps.missions.models import Mission
from whambush.apps.hashtags.models import Hashtag
from whambush.libs.models import TimeStampedModel, SoftDeleteModel
from whambush.apps.activities.signals import action

from sortedm2m.fields import SortedManyToManyField
from django_extensions.db.fields import AutoSlugField

import operator


FLAG_TYPE_CHOICES = (
    (0, _('Video')),
    (1, _('Comment')),
)


class VideoQueryset(QuerySet):
    def wb_order(self, order='latest'):
        if order == 'oldest':
            return self.order_by('published_at')
        elif order == 'rank':
            return self.order_by('-rank', '-published_at')
        else:
            # default is latest
            return self.order_by('-published_at')

    def active(self):
        # get active (and processed) videos
        return self.filter(is_active=True, is_processed=True, deleted_at=None)

    def random(self, length=10):
        # get length numbers of random videos
        return self.active().order_by('?')[:length]

    def country_filter(self, countries):
        if countries:
            return self.filter(country__in=countries)
        else:
            return self

    def type_filter(self, video_type):
        if video_type:
            return self.filter(video_type=video_type)
        else:
            return self

    def latest_filter(self, countries, with_global=True):
        # get global mission ids
        mission_ids = False
        if with_global:
            mission_ids = Mission.objects.filter(
                country=settings.GLOBAL_COUNTRY_CODE
            ).values_list('pk', flat=True)

        if countries and mission_ids:
            return self.filter(
                Q(country__in=countries) | Q(mission__in=mission_ids)
            )
        elif mission_ids:
            return self.filter(mission__in=mission_ids)
        elif countries:
            return self.filter(country__in=countries)
        else:
            return self


class VideoManager(models.Manager):
    def __getattr__(self, name):
        return getattr(self.get_query_set(), name)

    def get_query_set(self):
        return VideoQueryset(self.model)

    def search(self, search_terms):
        terms = [term.strip() for term in search_terms.split()]
        q_objects = []

        for term in terms:
            q_objects.append(Q(name__icontains=term))
            q_objects.append(Q(description__icontains=term))
            q_objects.append(Q(tags__name__icontains=term))
            q_objects.append(Q(added_by__username__icontains=term))

        # Start with a bare QuerySet
        qs = self.get_query_set().active()

        # Use operator's or_ to string together all of your Q objects.
        return qs.filter(reduce(operator.or_, q_objects)).distinct()


class Video(TimeStampedModel, SoftDeleteModel):
    VIDEO_COUNTRY_CHOICES = settings.COUNTRY_CHOICES + (
        (settings.GLOBAL_COUNTRY_CODE, 'Global'),
    )

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    external_id = models.CharField(max_length=20)
    hosting = models.CharField(max_length=30, default='vzaar')
    added_by = models.ForeignKey('accounts.WhamUser', null=True)
    is_processed = models.BooleanField(default=False)
    tags = models.ManyToManyField(Hashtag, blank=True)
    mission = models.ForeignKey(Mission, null=True, blank=True, default=None)
    view_count = models.IntegerField(default=0, blank=True)
    rank = models.IntegerField(default=0, blank=True)
    is_active = models.BooleanField(default=True)
    video_type = models.IntegerField(default=0, blank=True)
    slug = AutoSlugField(populate_from='name', unique=True, null=True)
    is_mission_winner = models.BooleanField(default=False)
    published_at = models.DateTimeField(blank=True, null=True)
    country = models.CharField(
        max_length=2, choices=VIDEO_COUNTRY_CHOICES,
        default='FI', blank=True
    )

    objects = VideoManager()

    class Meta:
        ordering = ['-published_at']

    def __unicode__(self):
        return u'[{}] {}'.format(self.added_by, self.name)

    @property
    def external_url(self):
        return 'https://view.vzaar.com/{}/video'.format(self.external_id)

    @property
    def thumbnail_url(self):
        return 'http://view.vzaar.com/{}/image'.format(self.external_id)

    @property
    def web_url(self):
        return '{0}/v/{1}'.format(settings.WEB_URL, self.slug)

    def clean_fields(self, exclude=None):
        super(Video, self).clean_fields(exclude)
        if (
            self.id == 0
            and self.mission is not None
            and self.mission.has_submitted
        ):
            if Video.objects.filter(
                added_by=self.added_by, mission=self.mission
            ):
                raise ValidationError({'mission': _(
                    'This user already submitted a video for this mission'
                )})

    def get_absolute_url(self):
        """
        Required for sitemap.

        :return: Web URL of the video.
        """
        return self.web_url

    def delete(self):
        # soft delete
        super(Video, self).delete()

        # save activity
        action.send(
            self.added_by,
            verb='deleted',
            target=self
        )


class Flag(TimeStampedModel):
    user = models.ForeignKey('accounts.WhamUser')
    object_pk = models.IntegerField()
    reason_code = models.IntegerField()
    flag_type = models.IntegerField(
        choices=FLAG_TYPE_CHOICES, default=0, blank=True
    )

    def __unicode__(self):
        return '%s flagged %s' % (self.user, self.video)


class VideoFeed(TimeStampedModel):
    name = models.CharField(max_length=30)
    icon_url = models.URLField(null=True, blank=True)
    default_icon = models.IntegerField(default=0, blank=True)
    endpoint_url = models.CharField(max_length=128)
    order = models.IntegerField()
    version = models.CharField(default='1.0', max_length=20)
    is_default = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class FeaturedVideos(TimeStampedModel):
    videos = SortedManyToManyField(Video, null=True, blank=True)

    class Meta:
        verbose_name_plural = "Featured videos"

    def __unicode__(self):
        return 'Featured video list'


def stream_comments(sender, instance, created, **kwargs):
    if created:
        action.send(
            instance.user,
            verb='commented on',
            target=instance.content_object,
            action_object=instance
        )

post_save.connect(stream_comments, Comment)
