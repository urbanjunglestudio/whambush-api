from django.conf.urls import patterns, url

from .views import VideoDetailWebView

''' Videos URL patterns '''

urlpatterns = patterns(
    '',
    url(r'^(?P<slug>[A-Za-z0-9-_]{1,50})/$',
        VideoDetailWebView.as_view(), name='web_video_detail'),
)
