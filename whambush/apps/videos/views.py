from django.shortcuts import get_object_or_404
from django.conf import settings
from django.views.generic.base import RedirectView

from .models import Video


class VideoDetailWebView(RedirectView):
    query_string = True

    def get_redirect_url(self, *args, **kwargs):
        # just redirect to same location on website
        # TODO: this can be removed in several months
        get_object_or_404(
            Video,
            slug=kwargs['slug'],
            is_active=True,
            is_processed=True,
            deleted_at=None
         )
        return '{}/v/{}/'.format(settings.WEB_URL, kwargs['slug'])


# from django.shortcuts import get_object_or_404
# from django.contrib.comments.models import Comment
# from django.contrib.contenttypes.models import ContentType
# from django.utils.html import urlize
# from django.views.generic import DetailView
# from .models import Video
# from whambush.apps.likes.services import get_likes_dislikes_on_video

# class VideoDetailWebView(DetailView):
#     model = Video
#     template_name = 'videos/video_detail.html'

#     def get_object(self):
#         return get_object_or_404(
#             self.model,
#             slug=self.kwargs['slug'],
#             is_processed=True,
#             deleted_at=None
#         )

#     def get_context_data(self, **kwargs):
#         context = super(VideoDetailWebView, self).get_context_data(**kwargs)
#         recent_comments = Comment.objects.filter(
#             object_pk=self.object.id,
#             content_type=ContentType.objects.get_for_model(self.object),
#             is_removed=False
#         ).order_by('-submit_date')[:3]
#         likes, dislikes = get_likes_dislikes_on_video(self.object)
#         context['comments'] = recent_comments
#         context['likes'] = likes
#         context['dislikes'] = dislikes
#         self.object.description_html = urlize(
#             self.object.description, 50, True
#         )
#         return context
