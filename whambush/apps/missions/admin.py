from django.contrib import admin
from django.utils.timezone import now

from .models import Mission
from whambush.apps.videos.tasks import update_videos_rank


def update_video_ranks(model_admin, request, queryset):
    update_videos_rank(mission_queryset=queryset)
    model_admin.message_user(
        request, 'Update completed.'
    )


class MissionAdmin(admin.ModelAdmin):
    actions = (update_video_ranks,)
    list_display = (
        'name', 'description', 'added_by', 'public_url', 'start_at', 'end_at',
        'country', 'mission_type', 'icon_name', 'linked_video', 'video_count',
        'max_user_submissions', 'max_submissions', 'max_video_length',
        'is_videos_ranked', 'is_push_sent',
    )
    search_fields = ['name', 'description', 'country']

    fieldsets = (
        (None, {
            'fields': (
                'name', 'description', 'added_by', 'mission_type', 'country',
                'icon_name', 'is_videos_ranked', 'is_push_sent',
                'linked_video', 'bananas', 'prize',
                'max_user_submissions', 'max_submissions', 'max_video_length',
                'mission_image_1', 'mission_image_2',
            ),
        }),
        (None, {
            'fields': ('start_at', 'end_at'),
            'description': (
                '<div class="system-message" style="font-size: 15px;">'
                '<strong style="color: red;">IMPORTANT</strong>:'
                ' You should enter the time in UTC (+00:00).'
                ' Don\'t trust the "Now" button below since it will use'
                ' your browser timezone!<br />Check the current time in UTC'
                ' <a href="http://time.is/UTC">here</a>.</div>'.format(now())
            )
        }),
    )
    list_filter = ('country',)

    def video_count(self, obj):
        return obj.video_set.active().count()

    def public_url(self, obj):
        return ('<a href="{}" target="_blank">View</a>'.format(
            obj.web_url
        ))
    public_url.allow_tags = True

admin.site.register(Mission, MissionAdmin)
