from django.test import TestCase
from django.core import mail

from ..models import Mission
from .. import tasks
from whambush.apps.accounts.models import WhamUser
from whambush.apps.videos.models import Video
from whambush.apps.likes.models import Like

from mock import patch, ANY
from push_notifications.models import APNSDevice, GCMDevice
from datetime import timedelta, datetime


class MissionTaskTestCase(TestCase):
    def setUp(self):
        # clean all objects of previous tests
        WhamUser.objects.all().delete()
        Video.objects.all().delete()

        # make test objects
        for i in range(10):
            WhamUser.objects.create(
                id=(i+1),
                username='tester%s' % str(i+1),
                email='tester%s@testserver.com' % str(i+1),
                country='FI'
            )
            WhamUser.objects.create(
                id=(i+11),
                username='tester%s' % str(i+11),
                email='tester%s@testserver.com' % str(i+11),
                country='VN'
            )
            APNSDevice.objects.create(
                registration_id='Test%s apns device' % str(i+1),
                user=WhamUser.objects.get(id=(i+1))
            )
            GCMDevice.objects.create(
                registration_id='Test%s gcm device' % str(i+1),
                user=WhamUser.objects.get(id=(i+1))
            )
            APNSDevice.objects.create(
                registration_id='Test%s apns device' % str(i+11),
                user=WhamUser.objects.get(id=(i+11))
            )
            GCMDevice.objects.create(
                registration_id='Test%s gcm device' % str(i+11),
                user=WhamUser.objects.get(id=(i+11))
            )

        normal_mission = Mission.objects.create(
            name="Test mission",
            added_by=WhamUser.objects.get(pk=11),
            start_at=datetime.now() - timedelta(days=4),
            end_at=datetime.now() - timedelta(days=2),
            country='FI'
        )
        icon_mission = Mission.objects.create(
            icon_name="Test icon mission",
            added_by=WhamUser.objects.get(pk=1),
            start_at=datetime.now() - timedelta(days=4),
            end_at=datetime.now() - timedelta(days=2),
            mission_type=1,
            country='ZZ'
        )
        # create videos for normal mission
        for i in range(8):
            Video.objects.create(
                id=(i+1),
                name="Video %s" % str(i+1),
                external_id=i,
                added_by=WhamUser.objects.get(pk=(i+1)),
                is_processed=True,
                mission=normal_mission,
                view_count=(i+1)*3,
                is_active=True
            )
        # create videos for icon mission
        for i in range(10):
            Video.objects.create(
                id=(i+10),
                name="Video %s" % str(i+10),
                external_id=(i+9),
                added_by=WhamUser.objects.get(pk=(i+10)),
                is_processed=True,
                mission=icon_mission,
                view_count=(i+1)*3,
                is_active=True
            )
        # give video #1, #2, #3, #19 10 likes => video #1, #19 has 10 points
        for i in range(10):
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=1),
                point=1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=2),
                point=1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=3),
                point=1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=19),
                point=1
            )

        # give video #2 & #3 2 dislikes => video #2, #3 have 8 points
        for i in range(2):
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+10)),
                video=Video.objects.get(pk=2),
                point=-1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+10)),
                video=Video.objects.get(pk=3),
                point=-1
            )
        # give video #4 #5 #12 9 likes
        for i in range(9):
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=4),
                point=1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=5),
                point=1
            )
            Like.objects.create(
                user=WhamUser.objects.get(pk=(i+1)),
                video=Video.objects.get(pk=18),
                point=1
            )
        # give video #4 & #5 1 dislike => video #4, #5 have 8 points
        Like.objects.create(
            user=WhamUser.objects.get(pk=10),
            video=Video.objects.get(pk=4),
            point=-1
        )
        Like.objects.create(
            user=WhamUser.objects.get(pk=11),
            video=Video.objects.get(pk=5),
            point=-1
        )
        # change all like created_at to 3 days before
        Like.objects.all().update(
            created_at=datetime.now() - timedelta(days=3)
        )

    def test_send_mission_winner_mail_success(self):
        tasks.send_mission_winner_email()
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(
            mail.outbox[0].to, ['rewards@whambush.com']
        )
        self.assertIn(
            'NORMAL:Test mission \n1, tester1', mail.outbox[0].body
        )

        mail_message = 'ICON:Test icon mission '
        for i in range(10):
            mail_message += (
                '\n{0}, tester{1}, {1}, tester{1}@testserver.com, '
                'http://whambush.net/v/video-{1}/, ACTIVATED. '
            ).format(i+1, 19-i)
        self.assertIn(mail_message, mail.outbox[0].body)

    def test_send_winner_mail_fail(self):
        # Mission end before previous week
        Mission.objects.all().update(end_at=datetime.now()-timedelta(days=8))
        tasks.send_mission_winner_email()
        self.assertEquals(len(mail.outbox), 0)
        # Find no mission
        Mission.objects.all().delete()
        tasks.send_mission_winner_email()
        self.assertEquals(len(mail.outbox), 0)

    def test_send_no_winner_mission_mail(self):
        # delete all videos for mission so there no winner
        Video.objects.all().delete()
        tasks.send_mission_winner_email()
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(
            mail.outbox[0].to, ['rewards@whambush.com']
        )
        self.assertIn(
            'NORMAL:Test mission \nno winner. \n', mail.outbox[0].body
        )
        self.assertIn(
            'ICON:Test icon mission \nno winner. \n', mail.outbox[0].body
        )

    @patch('push_notifications.apns.apns_send_bulk_message')
    @patch('push_notifications.gcm.gcm_send_bulk_message')
    def test_normal_mission_push_notifications(self, send_gcm, send_apns):
        Mission.objects.filter(name='Test mission').update(
            start_at=datetime.now() - timedelta(hours=2),
            end_at=datetime.now() + timedelta(days=2),
        )
        apns_FI_registration_ids = []
        gcm_FI_registration_ids = []
        for i in range(10):
            apns_FI_registration_ids.append(
                unicode('Test%s apns device' % str(i+1))
            )
            gcm_FI_registration_ids.append(
                unicode('Test%s gcm device' % str(i+1))
            )
        tasks.push_notification_new_missions()
        # check correct notification message
        send_apns.assert_called_once_with(
            sound='default', registration_ids=ANY,
            alert=None, loc_key='PUSH_NEW_MISSION',
            loc_args=[u'Test mission'], badge=1
        )
        send_gcm.assert_called_once_with(
            registration_ids=ANY,
            data={
                'message': {
                    'loc-args': [u'Test mission'],
                    'loc-key': 'PUSH_NEW_MISSION'
                }
            }
        )
        # check notification is sent to correct device(all FI user's devices)
        self.assertEquals(
            set(send_apns.call_args_list[0][1]['registration_ids']),
            set(apns_FI_registration_ids)
        )
        self.assertEquals(
            set(send_gcm.call_args_list[0][1]['registration_ids']),
            set(gcm_FI_registration_ids)
        )
        # mission is_push_sent
        self.assertEquals(
            Mission.objects.get(name='Test mission').is_push_sent, True
        )

    @patch('push_notifications.apns.apns_send_bulk_message')
    @patch('push_notifications.gcm.gcm_send_bulk_message')
    def test_ICON_mission_push_notifications(self, send_gcm, send_apns):
        Mission.objects.filter(icon_name='Test icon mission').update(
            start_at=datetime.now() - timedelta(hours=2),
            end_at=datetime.now() + timedelta(days=2),
        )
        apns_all_registration_ids = []
        gcm_all_registration_ids = []
        for i in range(10):
            apns_all_registration_ids.extend(
                (
                    unicode('Test%s apns device' % str(i+1)),
                    unicode('Test%s apns device' % str(i+11)),
                )
            )
            gcm_all_registration_ids.extend(
                (
                    unicode('Test%s gcm device' % str(i+1)),
                    unicode('Test%s gcm device' % str(i+11)),
                )
            )
        tasks.push_notification_new_missions()
        send_apns.assert_called_once_with(
            sound='default', registration_ids=apns_all_registration_ids,
            alert=None, loc_key='PUSH_NEW_ICON_MISSION',
            loc_args=[u'Test icon mission'], badge=1
        )
        send_gcm.assert_called_once_with(
            registration_ids=gcm_all_registration_ids,
            data={
                'message': {
                    'loc-args': [u'Test icon mission'],
                    'loc-key': 'PUSH_NEW_ICON_MISSION'
                }
            }
        )
        # mission is_push_sent
        self.assertEquals(
            Mission.objects.get(icon_name='Test icon mission').is_push_sent,
            True
        )

    @patch('push_notifications.apns.apns_send_bulk_message')
    @patch('push_notifications.gcm.gcm_send_bulk_message')
    def test_DARE_mission_push_notifications(self, send_gcm, send_apns):
        Mission.objects.filter(name='Test mission').update(
            name='Test dare mission',
            start_at=datetime.now() - timedelta(hours=2),
            end_at=datetime.now() + timedelta(days=2),
            mission_type=2
        )
        apns_FI_registration_ids = []
        gcm_FI_registration_ids = []
        for i in range(10):
            apns_FI_registration_ids.append(
                unicode('Test%s apns device' % str(i+1))
            )
            gcm_FI_registration_ids.append(
                unicode('Test%s gcm device' % str(i+1))
            )
        tasks.push_notification_new_missions()
        # check correct notification message
        send_apns.assert_called_once_with(
            sound='default', registration_ids=ANY,
            alert=None, loc_key='PUSH_NEW_DARE_MISSION',
            loc_args=[u'Test dare mission'], badge=1
        )
        send_gcm.assert_called_once_with(
            registration_ids=ANY,
            data={
                'message': {
                    'loc-args': [u'Test dare mission'],
                    'loc-key': 'PUSH_NEW_DARE_MISSION'
                }
            }
        )
        # check notification is sent to correct device
        self.assertEquals(
            set(send_apns.call_args_list[0][1]['registration_ids']),
            set(apns_FI_registration_ids)
        )
        self.assertEquals(
            set(send_gcm.call_args_list[0][1]['registration_ids']),
            set(gcm_FI_registration_ids)
        )
        # mission is_push_sent
        self.assertEquals(
            Mission.objects.get(name='Test dare mission').is_push_sent, True
        )
