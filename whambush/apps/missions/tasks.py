from django.conf import settings
from django.core.mail import send_mail
from django.utils.timezone import now

from .models import Mission
from whambush.apps.videos.models import Video
from whambush.apps.videos.tasks import update_videos_rank

from celery import shared_task
from datetime import timedelta
from push_notifications.models import APNSDevice, GCMDevice


@shared_task
def push_notification_new_missions():
    # get the recent active missions
    missions = Mission.objects.filter(
        is_push_sent=False,
        start_at__lte=now(),
        start_at__gt=now() - timedelta(hours=3),  # started recently
        end_at__gte=now(),
    )

    if not missions:
        return False

    for m in missions:
        # prepare push data
        locale_key = 'PUSH_NEW_MISSION'
        locale_args = [m.name]
        if m.mission_type == 1:  # icon mission
            locale_key = 'PUSH_NEW_ICON_MISSION'
            locale_args = [m.icon_name]
        elif m.mission_type == 2:  # dare mission
            locale_key = 'PUSH_NEW_DARE_MISSION'

        # mark as sent anyhow
        m.is_push_sent = True
        m.save()

        if m.country == 'ZZ':
            apns_devices = APNSDevice.objects.all()
            gcm_devices = GCMDevice.objects.all()
        else:
            # find devices has user with user.country = mission.country
            apns_devices = APNSDevice.objects.select_related('user').filter(
                user__country=m.country
            )
            gcm_devices = GCMDevice.objects.select_related('user').filter(
                user__country=m.country
            )

        try:
            apns_devices.send_message(
                None, badge=1, sound='default',
                loc_key=locale_key, loc_args=locale_args
            )
        except Exception, e:
            print 'Error sending APNS push: {}'.format(str(e))
        try:
            gcm_devices.send_message({
                'loc-key': locale_key,
                'loc-args': locale_args
            })
        except Exception, e:
            print 'Error sending GCM push: {}'.format(str(e))


@shared_task
def send_mission_winner_email():
    # update mission rank
    update_videos_rank.apply()
    # get ranked missions in previous week
    qs = Mission.objects.filter(
        end_at__range=(now()-timedelta(days=7), now()),
        is_videos_ranked=True
    )
    if not qs:
        return False
    email_message = ''
    # loop through ranked missions
    for mission in qs:
        # if mission is ICON mission
        if mission.mission_type == 1:
            email_message += 'ICON:{} \n'.format(mission.icon_name)
            try:
                winner_rank = Video.objects.get(
                    mission=mission, is_mission_winner=True
                ).rank
            except Video.DoesNotExist:
                email_message += 'no winner. \n'
            else:
                # Get top 10 videos based on winner video's rank
                videos = Video.objects.filter(
                    mission=mission, rank__range=(winner_rank-9, winner_rank)
                ).order_by('-rank')
                for video in videos:
                    email_message += '{}, {}, {}, {}, {}, {}. \n'.format(
                        winner_rank - video.rank + 1, video.added_by.username,
                        video.added_by.id, video.added_by.email, video.web_url,
                        video.added_by.activation_status[1]
                    )
        else:
            email_message += 'NORMAL:{} \n'.format(mission.name)
            try:
                video = Video.objects.get(
                    mission=mission, is_mission_winner=True
                )
            except Video.DoesNotExist:
                email_message += 'no winner. \n'
            else:
                email_message += '{}, {}, {}, {}, {}, {}. \n'.format(
                    1, video.added_by.username,
                    video.added_by.id, video.added_by.email, video.web_url,
                    video.added_by.activation_status
                )

    email_subject = 'Mission Winner ' + now().strftime('%d %b %Y')
    # send email
    send_mail(
        email_subject,
        email_message,
        settings.DEFAULT_FROM_EMAIL,
        ['rewards@whambush.com']
    )
