from django.db import models
from django.db.models import get_model
from django.utils.timezone import now
from django.conf import settings

from whambush.libs.managers import WBQuerySet
from whambush.libs.models import TimeStampedModel
from whambush.libs.storage import ImageFileStorage

from django_extensions.db.fields import AutoSlugField

from datetime import datetime


class MissionQueryset(WBQuerySet):
    def wb_order(self, is_active_list=False):
        # order by icon mission first
        if is_active_list:
            return self.order_by('-mission_type', '-start_at')
        # order by latest first
        return self.order_by('-start_at')

    def active(self):
        # get active missions
        return self.filter(start_at__lte=now(), end_at__gte=now())

    def old(self):
        # get old missions
        return self.filter(end_at__lt=now())

    def new(self):
        # get not yet started missions
        return self.filter(start_at__gt=now())

    def country_filter(self, countries):
        if countries:
            return self.filter(country__in=countries)
        else:
            return self

    def added_by(self, user_id):
        if user_id:
            return self.filter(added_by=user_id)
        else:
            return self


class MissionManager(models.Manager):
    def __getattr__(self, name):
        return getattr(self.get_query_set(), name)

    def get_query_set(self):
        return MissionQueryset(self.model)


class Mission(TimeStampedModel):
    MISSION_COUNTRY_CHOICES = settings.COUNTRY_CHOICES + (
        (settings.GLOBAL_COUNTRY_CODE, 'Global'),
    )

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    added_by = models.ForeignKey('accounts.WhamUser', null=True)
    start_at = models.DateTimeField(null=True, blank=True)
    end_at = models.DateTimeField(null=True, blank=True)
    is_videos_ranked = models.BooleanField(default=False)
    is_push_sent = models.BooleanField(default=False)
    mission_type = models.IntegerField(default=0)
    icon_name = models.CharField(max_length=255, blank=True)
    linked_video = models.ForeignKey(
        'videos.Video', related_name='linked_video_mission', null=True,
        blank=True
    )
    country = models.CharField(
        max_length=2, choices=MISSION_COUNTRY_CHOICES
    )
    bananas = models.IntegerField(default=0, blank=True)
    prize = models.TextField(default='', blank=True)
    max_user_submissions = models.IntegerField(default=1, blank=True)
    max_submissions = models.IntegerField(default=1000, blank=True)
    max_video_length = models.IntegerField(default=30, blank=True)
    storage = ImageFileStorage(dir='img/mission_image')
    mission_image_1 = models.ImageField(
        upload_to=storage.get_path, storage=storage, blank=True, null=True
    )
    mission_image_2 = models.ImageField(
        upload_to=storage.get_path, storage=storage, blank=True, null=True
    )
    slug = AutoSlugField(populate_from='name', unique=True, null=True)

    objects = MissionManager()

    def __unicode__(self):
        return self.name

    def get_submission_number(self):
        video_model = get_model('videos', 'Video')
        return video_model.objects.filter(is_active=True, mission=self).count()

    def get_user_submission_number(self, user):
        video_model = get_model('videos', 'Video')
        return video_model.objects.filter(
            is_active=True, mission=self, added_by=user
        ).count()

    @property
    def web_url(self):
        return '{0}/m/{1}'.format(settings.WEB_URL, self.slug)

    @property
    def status(self):
        time_now = now()
        if self.start_at > time_now:
            return "new"
        elif self.end_at <= time_now:
            return "old"
        return "active"
