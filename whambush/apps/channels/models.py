from django.db import models
from django.contrib.sites.models import Site
from django.conf import settings

from whambush.libs.models import TimeStampedModel, SoftDeleteModel
from whambush.libs.storage import OverwriteStorage

from django_extensions.db.fields import AutoSlugField


class Channel(TimeStampedModel, SoftDeleteModel):
    TYPE_CHOICES = (
        (0, 'CHANNEL'),
        (1, 'VIDEO_CHANNEL')
    )
    COUNTRY_CHOICES = settings.COUNTRY_CHOICES + (
        (settings.GLOBAL_COUNTRY_CODE, 'Global'),
    )

    name = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    slug = AutoSlugField(populate_from='name', unique=True, null=True)
    channel_type = models.IntegerField(
        default=TYPE_CHOICES[0], choices=TYPE_CHOICES
    )
    storage = OverwriteStorage(dir='img/channel')
    picture = models.ImageField(
        upload_to=storage.get_path, storage=storage, blank=True, null=True
    )
    parent = models.ForeignKey('self', null=True, blank=True, default=None)
    artist = models.ForeignKey(
        'accounts.WhamUser', null=True, blank=True, default=None
    )
    country = models.CharField(
        max_length=2, blank=True, null=True, choices=COUNTRY_CHOICES
    )
    order = models.IntegerField(default=0)

    class Meta:
        ordering = ['order']

    def __unicode__(self):
        return self.name

    @property
    def children(self):
        return Channel.objects.filter(parent=self)

    @property
    def picture_url(self):
        if self.picture:
            current_site = Site.objects.get_current()
            return (
                settings.PROTOCOL + current_site.domain
                + settings.MEDIA_URL + str(self.picture)
            )
        else:
            return ''

    @property
    def channel_country(self):
        if self.country:
            return self.country
        else:
            # recursively go to the root channel (country channel) to fetch
            # the country
            parent = self.parent
            while True:
                if parent.country:
                    return parent.country
                else:
                    parent = parent.parent
