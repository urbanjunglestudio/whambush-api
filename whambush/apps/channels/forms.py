from django import forms
from django.utils.translation import ugettext_lazy as _

from .models import Channel


class ChannelForm(forms.ModelForm):
    class Meta:
        model = Channel

    def clean(self):
        data = self.cleaned_data
        if data.get('country'):
            if data.get('parent'):
                raise forms.ValidationError(
                    _('Channel with country should not have parent channel')
                )
            if data.get('channel_type') == Channel.TYPE_CHOICES[1][0]:
                raise forms.ValidationError(
                    _('Channel with country cannot be VIDEO_CHANNEL type')
                )
        if (
            data.get('channel_type') == Channel.TYPE_CHOICES[0][0]
            and data.get('artist')
        ):
            raise forms.ValidationError(
                _('Normal channel should not have link to artist')
            )
        if (
            data.get('channel_type') == Channel.TYPE_CHOICES[1][0]
            and not data.get('artist')
        ):
            raise forms.ValidationError(
                _('Video channel must have link to artist')
            )

        return data
