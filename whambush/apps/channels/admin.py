from django.contrib import admin

from .models import Channel
from .forms import ChannelForm


class ChannelAdmin(admin.ModelAdmin):
    form = ChannelForm
    list_display = [
        'name',
        'channel_type',
        'parent',
        'artist',
    ]
    # exclude = ['last_login']
    search_fields = ['name']
    list_filter = ['channel_type']

admin.site.register(Channel, ChannelAdmin)
