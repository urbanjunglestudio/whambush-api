from django.contrib.sitemaps import Sitemap
from django.core.urlresolvers import reverse

from whambush.apps.videos.models import Video


class StaticViewSitemap(Sitemap):
    priority = 0.8
    changefreq = 'daily'

    def items(self):
        return ['home']

    def location(self, item):
        return reverse(item)


class VideoSitemap(Sitemap):
    priority = 0.5
    changefreq = 'hourly'

    def items(self):
        return Video.objects.active()

    def lastmod(self, obj):
        return obj.modified_at
