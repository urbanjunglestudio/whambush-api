from annoying.decorators import render_to


''' Whambush root views '''


@render_to('home.html')
def home(request):
    return {}
