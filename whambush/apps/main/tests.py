from django.test import TestCase
from django.core.urlresolvers import reverse


class MainViewTestCases(TestCase):
    def test_home(self):
        resp = self.client.get(reverse('home'))
        self.assertEqual(resp.status_code, 200)

    def test_sitemap(self):
        resp = self.client.get(reverse('sitemap'))
        self.assertEqual(resp.status_code, 200)
