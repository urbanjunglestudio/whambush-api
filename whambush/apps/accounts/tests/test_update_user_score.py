from django.test import TestCase

from ..models import UserScore
from ..tasks import update_user_score


class UserScoreUpdatingTestCases(TestCase):
    fixtures = ['user_score_testdata.json']

    def test_updating_user_score(self):
        update_user_score()
        # user 1's score should be 5
        self.assertEqual(UserScore.objects.get(user=1).score, 5)
        # user 2's score should be 1 because 1 like from video_type==2
        self.assertEqual(UserScore.objects.get(user=2).score, 1)
        # user 3's score should be 0 because his video is not active
        self.assertEqual(UserScore.objects.get(user=3).score, 0)
