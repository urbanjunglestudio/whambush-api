from django.test import TestCase
from django.core.urlresolvers import reverse
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode
from django.conf import settings
from django.core import mail

from whambush.apps.accounts.models import WhamUser, ActivationKey


class AccountViewTestCases(TestCase):
    def setUp(self):
        self.user = WhamUser.objects.create_user(
            'test_user_1',
            'test1@example.com',
            'test_user_1'
        )

    def test_reset_password(self):
        resp = self.client.get(reverse('reset_password'))
        self.assertEqual(resp.status_code, 200)

    def test_reset_password_done(self):
        resp = self.client.get(reverse('reset_password_done'))
        self.assertEqual(resp.status_code, 200)

    def test_reset_password_confirm_failure(self):
        resp = self.client.get(reverse(
            'reset_password_confirm',
            kwargs={'uidb64': 'ABC', 'token': '123DEF456'}
        ))
        self.assertEqual(resp.status_code, 200)

    def test_reset_password_confirm_success(self):
        user = WhamUser.objects.get(username='test_user_1')
        uidb64 = urlsafe_base64_encode('{}'.format(user.id))
        gen = default_token_generator
        token = gen.make_token(user)
        resp = self.client.get(reverse(
            'reset_password_confirm',
            kwargs={'uidb64': uidb64, 'token': token}
        ))
        self.assertEqual(resp.status_code, 200)
        # form should be shown with 2 password fields
        self.assertIn('input id="id_new_password1"', resp.content)
        self.assertIn('input id="id_new_password2"', resp.content)

    def test_reset_password_complete(self):
        resp = self.client.get(reverse('reset_password_complete'))
        self.assertEqual(resp.status_code, 200)

    def test_activate_failure(self):
        resp = self.client.get(reverse('activate', kwargs={'key': 'abc'}))
        self.assertEqual(resp.status_code, 200)

    def test_activate_success(self):
        # set a user to inactive to test
        user = WhamUser.objects.get(username='test_user_1')
        user.is_active = False
        user.save()
        # create the key
        key = ActivationKey.objects.create(
            user=user,
            key='Abc123XYz',
            is_activated=False
        )

        resp = self.client.get(reverse('activate', kwargs={'key': key}))
        self.assertEqual(resp.status_code, 200)
        # key should be activated (expired)
        self.assertTrue(ActivationKey.objects.get(pk=key.id).is_activated)
        # user should be active
        self.assertTrue(WhamUser.objects.get(username='test_user_1').is_active)

    def test_get_resend_activation_view(self):
        resp = self.client.get(reverse('resend_activation'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/resend_activation.html')
        self.assertIn('<input id="id_email" name="email"', resp.content)

    def test_resend_activation_invalid_email(self):
        resp = self.client.post(reverse('resend_activation'), data={
            'email': 'blah'
        })
        self.assertEqual(resp.status_code, 200)
        self.assertIn(
            'Enter a valid email address.', resp.content
        )

    def test_resend_activation_not_exist_email(self):
        resp = self.client.post(reverse('resend_activation'), data={
            'email': 'not@exist.com'
        })
        self.assertEqual(resp.status_code, 200)
        self.assertIn(
            'Your email address does not exist in our system.', resp.content
        )

    def test_resend_activation_case_insensitive_email(self):
        resp = self.client.post(reverse('resend_activation'), data={
            'email': 'test1@Example.com'
        })
        self.assertEqual(resp.status_code, 200)
        self.assertIn('Your account is already activated', resp.content)
        # New user request resend activation key with sensitive email
        user = WhamUser.objects.get(username='test_user_1')
        ActivationKey.objects.create(
            user=user,
            key='Abc123XYz',
            is_activated=False
        )
        resp = self.client.post(reverse('resend_activation'), data={
            'email': 'test1@Example.com'
        })
        self.assertEqual(resp.status_code, 200)
        # check if activation email is sent to user's registered email
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(
            mail.outbox[0].to, ['test1@Example.com']
        )

    def test_resend_activation_with_activated_user(self):
        # make activated user
        user = WhamUser.objects.get(username='test_user_1')
        ActivationKey.objects.create(
            user=user,
            key='Abc123XYz',
            is_activated=True
        )
        resp = self.client.post(reverse('resend_activation'), data={
            'email': 'test1@example.com'
        })
        self.assertEqual(resp.status_code, 200)
        self.assertIn('Your account is already activated', resp.content)

    def test_resend_activation_with_banned_user(self):
        # make banned user
        user = WhamUser.objects.get(username='test_user_1')
        user.is_active = False
        user.save()
        resp = self.client.post(reverse('resend_activation'), data={
            'email': 'test1@example.com'
        })
        self.assertEqual(resp.status_code, 200)
        self.assertIn('Your account is banned', resp.content)

    def test_resend_activation_with_new_user(self):
        # make new user
        user = WhamUser.objects.get(username='test_user_1')
        ActivationKey.objects.create(
            user=user,
            key='Abc123XYz',
            is_activated=False
        )
        resp = self.client.post(reverse('resend_activation'), data={
            'email': 'test1@example.com'
        })
        self.assertEqual(resp.status_code, 200)
        # check if activation email is sent
        self.assertEquals(len(mail.outbox), 1)
        self.assertEquals(
            mail.outbox[0].to, ['test1@example.com']
        )

    def test_register_reserved_username(self):
        resp = self.client.post(reverse('register'), {
            'username': settings.RESERVED_USERNAME[0],
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 200)
        self.assertIn(
            'REGISTER_ERROR_RESERVED_USERNAME', resp.content
        )

    def test_register_case_sensitive_username(self):
        resp = self.client.post(reverse('register'), {
            'username': 'Test_user_1',
            'password1': 'test_password',
            'password2': 'test_password',
        })
        self.assertEqual(resp.status_code, 200)
        self.assertIn(
            'User with this Username already exists.', resp.content
        )
