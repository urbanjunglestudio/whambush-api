from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.db.models.signals import post_save
from django.conf import settings
from django.db.models import Q
from django.utils.crypto import get_random_string
from django.utils.timezone import now as django_now
from django.contrib.sites.models import Site

from whambush.apps.activities.signals import action
from whambush.libs.models import TimeStampedModel
from whambush.libs.storage import OverwriteStorage

import datetime
import operator


class WhamUserManager(UserManager):
    def __getattr__(self, name):
        return getattr(self.get_query_set(), name)

    def search(self, search_terms):
        terms = [term.strip() for term in search_terms.split()]
        q_objects = []

        for term in terms:
            q_objects.append(Q(username__icontains=term))

        # Start with a bare QuerySet
        qs = self.get_query_set()

        # Use operator's or_ to string together all of your Q objects.
        return qs.filter(reduce(operator.or_, q_objects)).distinct()

    def search_by_field(self, search_terms, search_fields):
        allowed_fields = [
            'username', 'first_name', 'last_name', 'email', 'description',
            'country'
        ]
        terms = [term.strip() for term in search_terms.split()]
        fields = [f.strip() if f.strip() in allowed_fields else None
                  for f in search_fields.split(',')]
        fields = filter(None, fields)

        if fields:
            q_objects = []

            for term in terms:
                for field in fields:
                    q_objects.append(Q(**{field+'__exact': term}))

            # Start with a bare QuerySet
            qs = self.get_query_set()

            # Use operator's or_ to string together all of your Q objects.
            return qs.filter(reduce(operator.or_, q_objects)).distinct()
        else:
            return self.get_query_set().none()

    def create_guest(self):
        random_str = get_random_string()
        user = self.model(
            username='guest_{}'.format(random_str),
            email='guest_{}'.format(random_str),
            user_type=10
        )
        user.save(using=self._db)

        return user


class WhamUser(AbstractUser):
    STATUS_ACTIVATED = 'ACTIVATED'
    STATUS_INACTIVE = 'INACTIVE'
    STATUS_NEW = 'NEW'
    STATUS_DEACTIVATED = 'DEACTIVATED'

    rank = models.PositiveIntegerField(null=True, blank=True)
    storage = OverwriteStorage(dir='img/profile_picture')
    profile_picture = models.ImageField(
        upload_to=storage.get_path, storage=storage, blank=True, null=True
    )
    following = models.ManyToManyField(
        'WhamUser', through='Follow', related_name='followed_by'
    )
    description = models.CharField(max_length=100, blank=True, null=True)
    birthday = models.DateField(blank=True, null=True)
    country = models.CharField(
        max_length=2, choices=settings.COUNTRY_CHOICES
    )
    user_type = models.IntegerField(default=0, blank=True)
    linked_user = models.ForeignKey(
        'self', null=True, blank=True, default=None
    )
    changed_email_at = models.DateTimeField(blank=True, null=True)
    info = models.CharField(max_length=255, null=True, blank=True)

    objects = WhamUserManager()

    class Meta:
        verbose_name = 'User'
        ordering = ['username']

    def __unicode__(self):
        return self.username

    @property
    def activation_status(self):
        if self.is_active:
            # check if user has activate the account
            try:
                key = self.activationkey
            except ActivationKey.DoesNotExist:
                return True, self.STATUS_ACTIVATED  # super user?
            else:
                if key.is_activated:
                    return True, self.STATUS_ACTIVATED
                else:
                    # if activation key is not activated
                    # activation_time is the time user change email or join WB
                    activation_time = self.changed_email_at
                    if activation_time is None:
                        activation_time = self.date_joined

                    # if activation period has passed
                    if activation_time + datetime.timedelta(
                        days=settings.ACTIVATION_PERIOD
                    ) < django_now():
                        return False, self.STATUS_INACTIVE
                    else:
                        return True, self.STATUS_NEW
        else:
            return False, self.STATUS_DEACTIVATED

    @property
    def activation_state(self):
        return self.activation_status[1]

    @property
    def is_guest(self):
        return self.user_type == 10

# Email should be unique (Note: username is unique by default)
WhamUser._meta.get_field('email')._unique = True


class ActivationKey(TimeStampedModel):
    user = models.OneToOneField(WhamUser)
    key = models.CharField(max_length=64)
    is_activated = models.BooleanField(default=False)

    def __unicode__(self):
        return self.key


class Follow(TimeStampedModel):
    from_user = models.ForeignKey(WhamUser, related_name='follow_set_from')
    to_user = models.ForeignKey(WhamUser, related_name='follow_set_to')

    class Meta:
        unique_together = ('from_user', 'to_user')

    def __unicode__(self):
        return u'{} followed {}'.format(
            self.from_user.get_full_name(),
            self.to_user.get_full_name()
        )

    def delete(self, using=None):
        super(Follow, self).delete(using=using)
        action.send(
            self.from_user,
            verb='unfollowed',
            target=self.to_user
        )


class UserScore(TimeStampedModel):
    user = models.OneToOneField(WhamUser)
    score = models.IntegerField(default=0)
    banana = models.IntegerField(default=0)

    @property
    def total_score(self):
        return self.score + self.banana

    def __unicode__(self):
        return u'{} has total {} score'.format(self.user, self.total_score)


def stream_follow(sender, instance, created, **kwargs):
    if created:
        action.send(
            instance.from_user,
            verb='followed',
            target=instance.to_user,
            action_object=instance
        )

post_save.connect(stream_follow, Follow)


class BananaTransaction(TimeStampedModel):
    amount = models.IntegerField()
    user = models.ForeignKey(WhamUser)

    def __unicode__(self):
        if self.amount < 0:
            return u'{} has been subtracted {} banana'.format(
                self.user,
                abs(self.amount)
            )
        else:
            return u'{} has been added {} banana'.format(
                self.user,
                abs(self.amount)
            )
