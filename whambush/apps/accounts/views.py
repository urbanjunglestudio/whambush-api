from django.contrib.auth.forms import (
    AuthenticationForm, SetPasswordForm
)
from django.contrib.auth import (
    authenticate, login as auth_login, logout as auth_logout,
)
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.views import (
    password_reset, password_reset_done,
    password_reset_confirm, password_reset_complete
)
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters

from .models import ActivationKey, WhamUser
from .forms import (
    UserCreationForm, ResendActivationForm, UserChangeForm, PasswordResetForm)
from .services import resend_activation_email

from rest_framework.authtoken.models import Token
from annoying.decorators import render_to


''' Accounts views '''


# ------------------------- Authentication ------------------------- #
@render_to('accounts/login.html')
def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = request.POST.get('username')
            password = request.POST.get('password')
            user = authenticate(username=username, password=password)
            if user is not None and user.is_active:
                active, status = user.activation_status
                if active:
                    auth_login(request, user)
                    # Redirect if successful
                    if request.POST['next']:
                        return redirect(request.POST['next'])
                    else:
                        return redirect('home')
                else:
                    form.errors['__all__'] = _('This account is inactive')
    else:
        form = AuthenticationForm()  # An unbound form

    return {'form': form}


def logout(request):
    auth_logout(request)
    return redirect('home')


# ------------------------- Registration & Profile ------------------------- #
@render_to('accounts/register.html')
def register(request):
    activation_required = False
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            activation_required = True
    else:
        form = UserCreationForm()  # An unbound form

    return {
        'form': form,
        'activation_required': activation_required
    }


@render_to('accounts/activate.html')
def activate(request, key):
    error = ''
    try:
        user_key = ActivationKey.objects.select_related('user').get(key=key)
    except ActivationKey.DoesNotExist:
        error = _('Invalid activation key.')
    else:
        if user_key.is_activated:
            error = _('This key is already used.')
        else:
            user_key.is_activated = True
            user_key.save()
            user_key.user.is_active = True
            user_key.user.save()
    return {
        'error_msg': error
    }


@render_to('accounts/resend_activation.html')
def resend_activation(request):
    if request.method == 'POST':
        form = ResendActivationForm(request.POST)
        if form.is_valid():
            resend_activation_email(form.cleaned_data['email'])
            return {'success': True}
    else:
        form = ResendActivationForm()
    return {'form': form}


@login_required(login_url=reverse_lazy('login'))
@render_to('accounts/edit_profile.html')
def edit_profile(request):
    if request.method == 'POST':
        form = UserChangeForm(request.POST)
        if form.is_valid():
            form.save()
            # Redirect to homepage if successful
            return redirect('home')
    else:
        form = UserChangeForm()  # An unbound form

    return {'form': form}


@receiver(post_save, sender=WhamUser)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created and not Token.objects.filter(user=instance):
        Token.objects.create(user=instance)


@csrf_protect
def reset_password(request):
    return password_reset(
        request=request,
        is_admin_site=False,
        template_name='accounts/password_reset_form.html',
        email_template_name='accounts/password_reset_email.html',
        subject_template_name='accounts/password_reset_subject.txt',
        password_reset_form=PasswordResetForm,
        token_generator=default_token_generator,
        post_reset_redirect=reverse_lazy('reset_password_done'),
    )


def reset_password_done(request):
    return password_reset_done(
        request=request,
        template_name='accounts/password_reset_done.html',
    )


@sensitive_post_parameters()
@never_cache
def reset_password_confirm(request, uidb64=None, token=None):
    return password_reset_confirm(
        request=request,
        uidb64=uidb64,
        token=token,
        template_name='accounts/password_reset_confirm.html',
        token_generator=default_token_generator,
        set_password_form=SetPasswordForm,
        post_reset_redirect=reverse_lazy('reset_password_complete'),
    )


def reset_password_complete(request):
    return password_reset_complete(
        request=request,
        template_name='accounts/password_reset_complete.html',
    )
