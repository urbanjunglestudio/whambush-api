from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext as _

from whambush.apps.api.v2_0.services import get_country_object
from .models import WhamUser, ActivationKey


class CountryListFilter(SimpleListFilter):
    title = _('Country')
    parameter_name = 'country'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        list_tuple = []
        for u in (
            WhamUser.objects.values('country').order_by('country').distinct()
        ):
            c = get_country_object(u['country'])
            list_tuple.append(
                (c.country_code, c.name)
            )
        return list_tuple

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value():
            return queryset.filter(country=self.value())
        else:
            return queryset


class UserAdmin(admin.ModelAdmin):
    list_display = [
        'username',
        'email',
        'is_active',
        'is_superuser',
        'is_guest',
        'last_login',
        'country',
    ]
    exclude = ['last_login']
    search_fields = ['username', 'email']
    list_filter = ['is_active', 'user_type', CountryListFilter]


class ActivationKeyAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        'key',
        'is_activated',
    )
    search_fields = ['user__username']

admin.site.register(WhamUser, UserAdmin)
admin.site.register(ActivationKey, ActivationKeyAdmin)
