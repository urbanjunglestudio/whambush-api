from django import forms
from django.contrib.auth.forms import (
    ReadOnlyPasswordHashField, PasswordResetForm as BasePassResetForm
)
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes

from .models import WhamUser
from .services import create_and_send_activation_key
from whambush.libs.utils import send_templated_email


class UserCreationForm(forms.ModelForm):
    '''
    A form for creating new users. Includes all the required
    fields, plus a repeated password.
    '''
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(
        label='Password confirmation', widget=forms.PasswordInput
    )
    guest_id = forms.CharField(required=False)

    PASSWORD_MIN_LENGTH = 4

    class Meta:
        model = WhamUser
        fields = (
            'username', 'email', 'password1', 'password2', 'user_type',
            'guest_id', 'country'
        )

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        # make email a required field when register
        self.fields['email'].required = True

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if username in settings.RESERVED_USERNAME:
            raise forms.ValidationError(
                'REGISTER_ERROR_RESERVED_USERNAME'
            )
        elif WhamUser.objects.filter(username__iexact=username):
            raise forms.ValidationError(
                _('User with this Username already exists.')
            )
        return username

    def clean_email(self):
        email = self.cleaned_data.get('email')
        if WhamUser.objects.filter(email__iexact=email):
            raise forms.ValidationError(
                _('User with this Email address already exists.')
            )
        return email

    def clean_user_type(self):
        user_type = self.cleaned_data.get('user_type')
        return user_type if user_type else 0

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')

        # At least PASSWORD_MIN_LENGTH long
        if len(password1) < self.PASSWORD_MIN_LENGTH:
            raise forms.ValidationError(
                _('The password must be at least %d characters long.')
                % self.PASSWORD_MIN_LENGTH
            )

        # # At least one letter and one non-letter
        # first_isalpha = password1[0].isalpha()
        # if all(c.isalpha() == first_isalpha for c in password1):
        #     raise forms.ValidationError(
        #         _('The password must contain at least one letter and '
        #           'at least one digit or punctuation character.')
        #     )

        return password1

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(_('Passwords don\'t match.'))
        return password2

    def clean_guest_id(self):
        guest_id = self.cleaned_data.get('guest_id')
        if guest_id and not WhamUser.objects.filter(pk=guest_id).exists():
            raise forms.ValidationError(
                'REGISTER_ERROR_GUEST_ID_DOES_NOT_EXIST'
            )
        return guest_id

    def save(self, commit=True):
        guest_id = self.cleaned_data.get('guest_id')
        if guest_id:
            # update the guest to be real user
            user = WhamUser.objects.get(pk=guest_id)
            if user.user_type == 10:  # update only if this is guest
                user.username = self.cleaned_data.get('username')
                user.email = self.cleaned_data.get('email')
                user.user_type = self.cleaned_data.get('user_type')
                user.country = self.cleaned_data.get('country')
            else:  # otherwise create new user
                user = super(UserCreationForm, self).save(commit=False)
        else:
            user = super(UserCreationForm, self).save(commit=False)

        # Save the provided password in hashed format
        user.set_password(self.cleaned_data['password1'])
        user.is_active = True
        if commit:
            user.save()
            create_and_send_activation_key(user)

        return user


class UserChangeForm(forms.ModelForm):
    '''
    A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    '''
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = WhamUser
        fields = ('profile_picture',)

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial['password']


class ResendActivationForm(forms.Form):
    '''
    A form for resending activation code.
    '''
    email = forms.EmailField(label=_('Your email address'))

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            user = WhamUser.objects.get(email__iexact=email)
        except WhamUser.DoesNotExist:
            raise forms.ValidationError(
                _('Your email address does not exist in our system.')
            )
        else:
            active, status = user.activation_status
            if active and status != WhamUser.STATUS_NEW:
                raise forms.ValidationError(
                    _('Your account is already activated.')
                )
            elif status == WhamUser.STATUS_DEACTIVATED:
                raise forms.ValidationError(
                    _(
                        'Your account is banned. Contact '
                        'support@whambush.com for more details.'
                    )
                )
        return email


class PasswordResetForm(BasePassResetForm):
    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            user = WhamUser.objects.get(email__iexact=email)
        except WhamUser.DoesNotExist:
            raise forms.ValidationError(
                _('Your email address does not exist in our system.')
            )
        else:
            active, status = user.activation_status
            if not active:
                if status == WhamUser.STATUS_DEACTIVATED:
                    raise forms.ValidationError(
                        _(
                            'Your account is banned. Contact '
                            'support@whambush.com for more details.'
                        )
                    )
                elif status == WhamUser.STATUS_INACTIVE:
                    raise forms.ValidationError(
                        _(
                            'Your account is inactive. Please follow the '
                            'instruction in the activation email we sent you.'
                        )
                    )
        return email

    def save(
        self, domain_override=None,
        subject_template_name='registration/password_reset_subject.txt',
        email_template_name='registration/password_reset_email.html',
        use_https=False, token_generator=default_token_generator,
        from_email=None, request=None, html_email_template_name=None
    ):
        email = self.cleaned_data["email"]
        active_users = WhamUser.objects.filter(email__iexact=email)
        for user in active_users:
            # Make sure that no email is sent to a user that actually has
            # a password marked as unusable
            if not user.has_usable_password():
                continue

            return send_templated_email(
                template='emails/password_reset.html',
                subject=_('Reset password'),
                to_emails=[user.email],
                context={
                    'title': _('Reset password'),
                    'email': user.email,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                    'user': user,
                    'token': token_generator.make_token(user),
                },
            )
