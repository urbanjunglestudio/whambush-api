from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from .models import WhamUser, ActivationKey
from whambush.libs.utils import send_templated_email

import hashlib
import random


def generate_activation_key():
    new_key = ''
    while True:
        new_key = hashlib.md5(
            '{}'.format(random.randint(100000000, 999999999))
        ).hexdigest()
        try:
            ActivationKey.objects.get(key=new_key)
        except ActivationKey.DoesNotExist:
            break
    return new_key


def send_activation_email(to_email, activation_key):
    '''
    Send activtion email to an user

    :param to_email: Email address of the user
    :param activation_key: activation key
    '''
    return send_templated_email(
        template='emails/activation.html',
        subject=_('Welcome to Whambush!'),
        to_emails=[to_email],
        context={
            'title': _('Welcome to Whambush!'),
            'key': activation_key,
        },
    )


def create_and_send_activation_key(user):
    user_key, created = ActivationKey.objects.get_or_create(
        user=user,
        defaults={'key': generate_activation_key()}
    )
    if not created:
        user_key.key = generate_activation_key()
    user_key.is_activated = False
    user_key.save()
    send_activation_email(user.email, user_key.key)


def resend_activation_email(email):
    try:
        act_key = ActivationKey.objects.get(user__email__iexact=email)
    except ActivationKey.DoesNotExist:
        act_key = ActivationKey(
            user=WhamUser.objects.get(email__iexact=email),
            key=generate_activation_key()
        )
        act_key.save()
    else:
        if act_key.is_activated:
            return False
        else:
            act_key.key = generate_activation_key()
            act_key.save()

    send_activation_email(email, act_key)
