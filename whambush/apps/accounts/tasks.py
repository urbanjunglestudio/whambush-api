from __future__ import absolute_import

from .models import UserScore

from celery import shared_task


@shared_task
def update_user_score():
    users_score = UserScore.objects.raw(
        '''SELECT
            0 AS id, -- raw query must include primary key
            added_by_id AS user_id,
            SUM(point) AS score
        FROM (
            SELECT
                video_id,
                v.added_by_id,
                (CASE WHEN SUM(point) < 0 -- when dislikes are more than likes
                    OR v.video_type = 2 -- when it's a TV video
                    THEN 0
                    ELSE SUM(point)
                END) AS point
            FROM likes_like AS l
                LEFT JOIN videos_video AS v ON l.video_id = v.id
            WHERE v.is_active = True
                AND v.is_processed = True
                AND v.deleted_at IS NULL
            GROUP BY video_id, v.added_by_id, v.video_type
        ) AS subquery -- psql subquery must have an alias
        GROUP BY user_id
        ORDER BY user_id
        '''
    )
    for user in users_score:
        s, created = UserScore.objects.get_or_create(
            user__id=user.user_id,
            defaults={
                'user_id': user.user_id,
                'score': user.score
            }
        )
        if not created:
            s.score = user.score
            s.save()
