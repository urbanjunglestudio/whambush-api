from django.conf.urls import patterns, url

''' Accounts URL patterns '''
urlpatterns = patterns(
    '',
    #url(r'^edit/$',
    #    'whambush.apps.accounts.views.edit_profile', name='edit_profile'),
    url(
        r'^register/$',
        'whambush.apps.accounts.views.register',
        name='register'
    ),
    url(
        r'^reset_password/$',
        'whambush.apps.accounts.views.reset_password',
        name='reset_password'
    ),
    url(
        r'^done_reset_password/$',
        'whambush.apps.accounts.views.reset_password_done',
        name='reset_password_done'
    ),
    url(
        r'^reset_password/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$',
        'whambush.apps.accounts.views.reset_password_confirm',
        name='reset_password_confirm'
    ),
    url(
        r'^finish_reset_password/$',
        'whambush.apps.accounts.views.reset_password_complete',
        name='reset_password_complete'
    ),
    url(
        r'^activate/(?P<key>\w+)/$',
        'whambush.apps.accounts.views.activate',
        name='activate'
    ),
    url(
        r'^resend_activation/$',
        'whambush.apps.accounts.views.resend_activation',
        name='resend_activation'
    ),
)
