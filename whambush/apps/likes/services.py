from .models import Like


def get_likes_dislikes_on_video(video):
    likes = Like.objects.filter(video=video, point=1).count()
    dislikes = Like.objects.filter(video=video, point=-1).count()

    return [likes, dislikes]
