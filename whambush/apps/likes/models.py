from django.db import models
from django.db.models.signals import post_save

from whambush.apps.accounts.models import WhamUser
from whambush.apps.videos.models import Video
from whambush.apps.missions.models import Mission
from whambush.libs.models import TimeStampedModel

from whambush.apps.activities.signals import action


class Like(TimeStampedModel):
    user = models.ForeignKey(WhamUser)
    video = models.ForeignKey(Video)
    point = models.IntegerField(blank=True, default=1)

    def __unicode__(self):
        if self.point == 1:
            return u'%s liked %s' % (self.user, self.video)
        else:
            return u'%s disliked %s' % (self.user, self.video)


def stream_likes(sender, instance, created, **kwargs):
    if created:
        act = 'liked' if instance.point == 1 else 'disliked'
        action.send(
            instance.user,
            verb=act,
            target=instance.video,
            action_object=instance
        )

post_save.connect(stream_likes, Like)


class MissionLike(TimeStampedModel):
    user = models.ForeignKey(WhamUser)
    mission = models.ForeignKey(Mission)
    point = models.IntegerField(blank=True, default=1)

    def __unicode__(self):
        if self.point == 1:
            return u'%s liked mission %s' % (self.user, self.mission)
        else:
            return u'%s disliked mission %s' % (self.user, self.mission)


def stream_mission_likes(sender, instance, created, **kwargs):
    if created:
        act = 'liked' if instance.point == 1 else 'disliked'
        action.send(
            instance.user,
            verb=act,
            target=instance.mission,
            action_object=instance
        )

post_save.connect(stream_mission_likes, MissionLike)
