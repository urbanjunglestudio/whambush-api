from django.contrib import admin

from .models import Activity


class ActivityAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_at'
    list_display = ('__str__', 'actor', 'verb', 'target', 'is_read')
    list_editable = ('verb',)
    list_filter = ('created_at',)
    raw_id_fields = (
        'actor_content_type', 'target_content_type',
        'action_object_content_type'
    )


admin.site.register(Activity, ActivityAdmin)
