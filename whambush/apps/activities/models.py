from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django.utils.timesince import timesince as djtimesince
from django.db.models import get_model
from django.conf import settings
from django.contrib.contenttypes import generic

from .signals import action
from .utils import action_handler
from .managers import ActivityManager
from whambush.libs.models import TimeStampedModel

from jsonfield import JSONField


class Activity(TimeStampedModel):
    """
    Action model describing the actor acting out a verb (on an optional
    target).
    Nomenclature based on http://activitystrea.ms/specs/atom/1.0/

    Generalized Format::

        <actor> <verb> <time>
        <actor> <verb> <target> <time>
        <actor> <verb> <action_object> <target> <time>
    """
    actor_content_type = models.ForeignKey(
        ContentType, related_name='wb_actor'
    )
    actor_object_id = models.CharField(max_length=255)
    actor = generic.GenericForeignKey('actor_content_type', 'actor_object_id')
    verb = models.CharField(max_length=255)
    description = models.TextField(blank=True, null=True)
    target_content_type = models.ForeignKey(
        ContentType, blank=True, null=True, related_name='wb_target'
    )
    target_object_id = models.CharField(max_length=255, blank=True, null=True)
    target = generic.GenericForeignKey(
        'target_content_type', 'target_object_id'
    )
    action_object_content_type = models.ForeignKey(
        ContentType, blank=True, null=True, related_name='wb_action_object'
    )
    action_object_object_id = models.CharField(
        max_length=255, blank=True, null=True
    )
    action_object = generic.GenericForeignKey(
        'action_object_content_type', 'action_object_object_id'
    )
    data = JSONField(blank=True, null=True)
    public = models.BooleanField(default=True)
    is_read = models.BooleanField(default=False)

    objects = ActivityManager()

    class Meta:
        ordering = ('-created_at', )

    def __unicode__(self):
        ctx = {
            'actor': self.actor,
            'verb': self.verb,
            'action_object': self.action_object,
            'target': self.target,
            'timesince': self.timesince()
        }
        if self.target:
            if self.action_object:
                return _(
                    '%(actor)s %(verb)s %(action_object)s '
                    'on %(target)s %(timesince)s ago'
                ) % ctx
            return _('%(actor)s %(verb)s %(target)s %(timesince)s ago') % ctx
        if self.action_object:
            return _(
                '%(actor)s %(verb)s %(action_object)s %(timesince)s ago'
            ) % ctx
        return _('%(actor)s %(verb)s %(timesince)s ago') % ctx

    def timesince(self, now=None):
        """
        Shortcut for the ``django.utils.timesince.timesince`` function of the
        current timestamp.
        """
        return djtimesince(
            self.created_at, now
        ).encode('utf8').replace(b'\xc2\xa0', b' ').decode()


def setup_generic_relations():
    """
    Set up GenericRelations for actionable models.
    """
    for m in settings.ACTIVITIES_MODELS:
        model = get_model(*m.split('.'))
        for field in ('actor', 'target', 'action_object'):
            attr = '%s_actions' % field
            if isinstance(
                getattr(model, attr, None),
                generic.ReverseGenericRelatedObjectsDescriptor
            ):
                break
            generic.GenericRelation(
                Activity,
                content_type_field='%s_content_type' % field,
                object_id_field='%s_object_id' % field,
                related_name='activities_with_%s_%s_as_%s' % (
                    model._meta.app_label, model._meta.module_name, field
                ),
            ).contribute_to_class(model, attr)

            setattr(Activity, 'activities_with_%s_%s_as_%s' % (
                model._meta.app_label, model._meta.module_name, field
            ), None)

setup_generic_relations()
action.connect(action_handler, dispatch_uid='activity.models')
