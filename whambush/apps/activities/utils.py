from django.db.models import get_model
from django.contrib.contenttypes.models import ContentType

from functools import wraps


def action_handler(verb, **kwargs):
    """
    Handler function to create Action instance upon action signal call.
    """

    kwargs.pop('signal', None)
    actor = kwargs.pop('sender')

    # We must store the unstranslated string
    # If verb is an ugettext_lazyed string, fetch the original string
    if hasattr(verb, '_proxy____args'):
        verb = verb._proxy____args[0]

    Activity = get_model('activities', 'Activity')
    newaction = Activity(
        actor_content_type=ContentType.objects.get_for_model(actor),
        actor_object_id=actor.pk,
        verb=unicode(verb),
        public=bool(kwargs.pop('public', True)),
        description=kwargs.pop('description', None),
    )

    for opt in ('target', 'action_object'):
        obj = kwargs.pop(opt, None)
        if obj is not None:
            setattr(newaction, '%s_object_id' % opt, obj.pk)
            setattr(
                newaction, '%s_content_type' % opt,
                ContentType.objects.get_for_model(obj)
            )
    newaction.data = kwargs
    newaction.save()


def stream(func):
    """
    Stream decorator to be applied to methods of an ``ActionManager`` subclass

    Syntax::

        from .decorators import stream
        from .managers import ActionManager

        class MyManager(ActionManager):
            @stream
            def foobar(self, ...):
                ...

    """
    @wraps(func)
    def wrapped(manager, *args, **kwargs):
        offset, limit = kwargs.pop('_offset', None), kwargs.pop('_limit', None)
        qs = func(manager, *args, **kwargs)
        if isinstance(qs, dict):
            qs = manager.public(**qs)
        elif isinstance(qs, (list, tuple)):
            qs = manager.public(*qs)
        if offset or limit:
            qs = qs[offset:limit]
        return qs.fetch_generic_relations()
    return wrapped
