from django.contrib import admin

from .models import GeneralData


class GeneralDataAdmin(admin.ModelAdmin):
    list_display = (
        'key',
        'value',
        'created_at',
    )
    fields = (
        'key',
        'value'
    )


admin.site.register(GeneralData, GeneralDataAdmin)
