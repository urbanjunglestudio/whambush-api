from django.db import models

from whambush.libs.models import TimeStampedModel


class GeneralData(TimeStampedModel):
    key = models.CharField(max_length=100, unique=True, primary_key=True)
    value = models.TextField()

    class Meta:
        verbose_name_plural = 'general data'

    def __unicode__(self):
        return u'"{0}": "{1}"'.format(self.key, self.value)
