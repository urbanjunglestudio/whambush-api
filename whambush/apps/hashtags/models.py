from django.db import models

from whambush.libs.models import TimeStampedModel


class Hashtag(TimeStampedModel):
    name = models.CharField(max_length=200, unique=True)

    def __unicode__(self):
        return self.name
