from os.path import join, dirname, abspath, sep
from datetime import timedelta
import sys

from celery.schedules import crontab
from .reserved_username_list import RESERVED_USERNAME
from .country_list import COUNTRY_CHOICES, GLOBAL_COUNTRY_CODE

ROOT_PATH = join(dirname(dirname(__file__)))

SITE_ID = 1

# translation and locale
LANGUAGE_CODE = 'en'
LANGUAGES = [
    ('en', 'English'),
    ('fi', 'Suomi'),
]
LOCALE_PATHS = [abspath(join(ROOT_PATH, join('..', 'locale'))) + sep]
USE_I18N = True
USE_L10N = True
USE_TZ = False
TIME_ZONE = 'UTC'

# media
MEDIA_ROOT = abspath(join(ROOT_PATH, 'media'))
MEDIA_URL = '/media/'

# static
STATIC_ROOT = ''
STATIC_URL = '/static/'
STATICFILES_DIRS = (
    abspath(join(ROOT_PATH, 'static')),
)
COUNTRY_FLAGS_DIR_NAME = 'country_flags'
COUNTRY_FLAGS_DIR = join(
    STATICFILES_DIRS[0],
    'img/{}'.format(COUNTRY_FLAGS_DIR_NAME)
)

ALLOWED_HOSTS = [
    'localhost', '127.0.0.1', '.whambush.net', '.whambush.com',
]

ADMINS = (
    ('Hieu Nguyen', 'hieu@whambush.com'),
)

MANAGERS = ADMINS

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.filesystem.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'whambush.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'whambush.wsgi.application'

TEMPLATE_DIRS = (
    abspath(join(ROOT_PATH, 'templates')),
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.contrib.messages.context_processors.messages",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.request",
    'constance.context_processors.config',
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.comments',
    'django.contrib.sitemaps',
    'django.contrib.admin',

    # --- 3rd party modules --- #
    'south',               # south for DB migration
    'gunicorn',            # gunicorn for deployment
    'rest_framework',      # rest framework for API
    'rest_framework.authtoken',
    'django_extensions',   # django extensions
    'django_nose',         # django nose for testing
    'constance',           # django constance for backend settings
    'constance.backends.database',
    'kombu.transport.django',  # for celery broker transport
    'djcelery',            # django celery for celery result backend
    'sortedm2m',           # django-sortedm2m for featured video list sorting
    'push_notifications',
    'rosetta',

    # ----- WHAMBUSH APP  ----- #
    'whambush.apps.api',
    'whambush.apps.hashtags',
    'whambush.apps.videos',
    'whambush.apps.accounts',
    'whambush.apps.missions',
    'whambush.apps.likes',
    'whambush.apps.promotions',
    'whambush.apps.activities',
    'whambush.apps.channels',
    'whambush.apps.country',
    'whambush.apps.settings',
    'whambush.apps.general_data',
    'whambush.apps.main',
)

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': join(ROOT_PATH, 'logs/whambush.log'),
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'whambush_log': {
            'handlers': ['logfile'],
            'level': 'DEBUG',
        }
    }
}


##############################################################################
# ---------------------------- MAGIC COMES NEXT ----------------------------
##############################################################################

DEBUG = True
TEMPLATE_DEBUG = DEBUG

PROTOCOL = 'http://'
WEB_URL = 'http://whambush.net'

AUTH_USER_MODEL = 'accounts.WhamUser'
ACTIVATION_PERIOD = 3  # user should activate their account in 3 days

INTERNAL_IPS = ('127.0.0.1',)

VZAAR_API = {
    'key': 'tofuhead',
    'secret': 'eAM4tJ3LaKwGfOHG3YG9sySxFdK1osTuzz0RhB9i8I',
}

VZAAR_SETTINGS = {
    'video_url': 'http://vzaar.com/api/videos/{}'
}

PUSH_NOTIFICATIONS_SETTINGS = {
    'GCM_API_KEY': 'AIzaSyAo8FxX9j5AeuZfLqeELgtMgdzvK-TjsVw',
    'APNS_CERTIFICATE': abspath(join(ROOT_PATH, '../devops/apns/dev.pem')),
    'APNS_HOST': 'gateway.sandbox.push.apple.com',
}

ACTIVITIES_MODELS = [
    'accounts.whamuser',
    'accounts.follow',
    'likes.like',
    'comments.comment',
    'videos.video',
]

##############################################################################
# Email
##############################################################################

DEFAULT_FROM_EMAIL = 'support@whambush.com'
SERVER_EMAIL = 'server@whambush.com'
SUPPORT_EMAIL = 'support@whambush.com'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

##############################################################################
# REST framework
##############################################################################

REST_FRAMEWORK = {
    'PAGINATE_BY': 10,
    'PAGINATE_BY_PARAM': 'page_size',
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'whambush.apps.api.authentication.ExpiringTokenAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
    ),
}

##############################################################################
# Celery
##############################################################################

CELERY_TASK_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_RESULT_SERIALIZER = 'json'
BROKER_URL = 'django://'
CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'

CELERYBEAT_SCHEDULE = {
    'Update users score': {
        'task': 'whambush.apps.accounts.tasks.update_user_score',
        'schedule': timedelta(hours=1)
    },
    'Update videos view count': {
        'task': 'whambush.apps.videos.tasks.update_video_view_count',
        'schedule': timedelta(hours=1)
    },
    'Update videos rank': {
        'task': 'whambush.apps.videos.tasks.update_videos_rank',
        'schedule': timedelta(hours=1)
    },
    'Push notification for new missions': {
        'task': 'whambush.apps.missions.tasks.push_notification_new_missions',
        'schedule': timedelta(minutes=10)
    },
    'Publish videos': {
        'task': 'whambush.apps.videos.tasks.publish_videos',
        'schedule': timedelta(minutes=10)
    },
    'Send mission winner email': {
        'task': 'whambush.apps.missions.tasks.send_mission_winner_email',
        'schedule': crontab(hour=8, minute=0, day_of_week=2)
    }
}

##############################################################################
# Testing
##############################################################################

if 'test' in sys.argv:
    EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
    LANGUAGE_CODE = 'en'  # force test to have english only

TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
NOSE_ARGS = ['--nologcapture', '--nocapture', '--verbosity=2']

FIXTURE_DIRS = (
    # fixtures for version dependent APIs
    abspath(join(ROOT_PATH, 'apps/api/fixtures/')),
    # different version of API fixtures
    abspath(join(ROOT_PATH, 'apps/api/v1_2/fixtures/')),
    abspath(join(ROOT_PATH, 'apps/api/v1_3/fixtures/')),
    abspath(join(ROOT_PATH, 'apps/api/v2_0/fixtures/')),
    abspath(join(ROOT_PATH, 'apps/api/v2_1/fixtures/')),
)

##############################################################################
# Basic auth for staging
##############################################################################
BASICAUTH_USERNAME = 'wham'
BASICAUTH_PASSWORD = 'BushThe#'
BASIC_PROTECT_URLS = [
    '/',
    '/urbanjungleadmin/',
]

##############################################################################
# Constance
##############################################################################

CONSTANCE_CONFIG = {
    'IS_BETA': (True, 'Are we in beta phase?'),
    'FLAG_LIMIT': (
        3,
        'Number of flags a video can take before a report email be sent'
        ' to support email.'
    ),
    'TRACKING_CODE': ('', 'Google Analytics code and so on.'),
    'GOOGLE_ANALYTICS_ID': ('', 'Google Analytics ID.')
}
CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
CONSTANCE_DATABASE_PREFIX = 'constance:whambush:'
