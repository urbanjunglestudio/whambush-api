from whambush.settings.base import *

# Make this unique, and don't share it with anybody.
SECRET_KEY = '56$8g^39f3-8gi523+5&9a$%1p79dnm2hf+0s+j6m+pc)$-ksa'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'whambush',
        'USER': 'whambush',
        'PASSWORD': 'whambush_PASS',
        'HOST': 'localhost',
        'PORT': '',
    }
}

DEBUG = True

REST_FRAMEWORK['DEFAULT_RENDERER_CLASSES'] += (
    'rest_framework.renderers.BrowsableAPIRenderer',
)
