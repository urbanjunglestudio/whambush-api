from os.path import join, dirname, abspath

ROOT_PATH = join(dirname(dirname(__file__)))

SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTOCOL', 'https')
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
PROTOCOL = 'https://'
WEB_URL = 'https://whambush.com'

PUSH_NOTIFICATIONS_SETTINGS = {
    'GCM_API_KEY': 'AIzaSyAo8FxX9j5AeuZfLqeELgtMgdzvK-TjsVw',
    'APNS_CERTIFICATE': abspath(join(ROOT_PATH, '../devops/apns/prod.pem')),
    'APNS_HOST': 'gateway.push.apple.com',
}
