from __future__ import with_statement
from fabric.api import (
    task, local, hosts, env, cd, run, warn_only, sudo
)
from fabvenv import virtualenv


# ------------------------------ Shortcuts ------------------------------ #
@task(alias='t')
def test(version='', module='', class_name='', test_name=''):
    '''
    Executes test suite
    fab t => run ALL tests
    fab t:version => run all tests in 1 version
    fab t:version,module => run all test in 1 module in a version
        Example: fab t:1.1 => run all API 1.1 tests
            fab t:1.2,accounts => run all accounts test in API 1.2

    '''
    if version:
        local(
            'python manage.py test '
            'whambush/apps/api/v{}{}{}{}'.format(
                version.replace('.', '_'),
                '/tests/test_api_{}.py'.format(module) if module != '' else '',
                ':{}'.format(class_name) if class_name != '' else '',
                '.{}'.format(test_name) if test_name != '' else ''
            )
        )
    else:
        local('python manage.py test')


@task(alias='r')
def runser():
    '''
    Lazy shortcut for runserver_plus
    '''
    local('python manage.py runserver_plus 0.0.0.0:8258')


@task(alias='s')
def shell():
    '''
    Another lazy shortcut for shell_plus
    '''
    local('python manage.py shell_plus')


@task(alias='m')
def make_msg():
    '''
    Another lazy shortcut for make messages
    '''
    local(
        'python manage.py makemessages --locale=fi -e py,html,email,txt'
    )


@task(alias='c')
def compile_msg():
    '''
    Another lazy shortcut for compilemessages
    '''
    local('python manage.py compilemessages')


# ------------------------------ Deployment ------------------------------ #
def _deploy():
    env.user = 'deployer'
    with cd('/srv/whambush/src'):
        with virtualenv('/srv/whambush/venv/'):
            # pull latest source
            run('git pull')
            # install new dependencies
            run('pip install -r requirements/base.txt')
            # sync DB
            run('python manage.py syncdb')
            # run south migrate
            run('python manage.py migrate')
            # collect static
            run('python manage.py collectstatic --noinput')
            # clear old sessions
            run('python manage.py clearsessions')
            # remove all pyc files
            run('find . -name "*.pyc" -exec rm -rf {} \;')
            # restart through supervisor
            sudo('supervisorctl restart all')


@task(alias='rc')
@hosts('whambush.com')
def run_command(command='ls'):
    '''
    Run any command in server
    '''
    env.user = 'deployer'
    with cd('/srv/whambush/src'):
        with virtualenv('/srv/whambush/venv/'):
            run(command)

"""
@task(alias='ds')
@hosts('whambush.net')
def deploy_stag():
    '''
    Deploy latest changes to staging server
    '''
    _deploy()


@task(alias='dp')
@hosts('whambush.com')
def deploy_prod():
    '''
    Deploy latest changes to PRODUCTION server
    '''
    _deploy()
"""


@task(alias='dl')
def deploy_local():
    '''
    Deploy latest changes to local environment
    '''
    local('git stash')
    local('git pull')
    local('find . -name "*.pyc" -exec rm -rf {} \;')
    local('pip install -r requirements/local.txt')
    local('python manage.py syncdb')
    local('python manage.py migrate')
    with warn_only():
        local('git stash apply')


# ------------------------------ Devops ------------------------------ #
@task(alias='is')
def init_server():
    '''
    Deploy the whole infrastructure to a new server
    '''
    local('ansible-playbook devops/site.yml -i devops/hosts --ask-vault-pass')
    local('ansible-playbook devops/app.yml -i devops/hosts --ask-vault-pass')


@task(alias='ss')
def sync_server_settings():
    '''
    Sync the latest templates to the server app
    '''
    local(
        'ansible-playbook devops/app.yml -i devops/hosts --ask-vault-pass '
        '--tags templated_settings'
    )
