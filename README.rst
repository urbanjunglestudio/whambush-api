How to start developing on Whambush
===================================

0. git clone git@github.com:urbanjunglestudio/whambush-api.git whambush
1. Make a new virtualenv for whambush, e.g: whambush_env and activate it
2. cd whambush
3. pip install -r requirements/local.txt
4. cp whambush/settings/local.sample.py whambush/settings/local.py
5. Change local.py to reflect your local settings
6. python manage.py syncdb
7. python manage.py migrate
8. python manage.py runserver_plus
9. Visit http://localhost:8000 and and start coding!
