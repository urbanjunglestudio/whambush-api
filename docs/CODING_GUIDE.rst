CODING STYLE GUIDE

Python
======

- Follow `PEP8 <http://legacy.python.org/dev/peps/pep-0008/>`_.
- Use pylint to guarantee you don't break anything.
- Write tests, a lot!


HTML
====

- Use HTML5!
- Sematic + valid HTML (when in doubt, validate yours).
- Use tab (4 spaces) to indent your code.
- Use tool like Emmet (ZenCoding) to generate beautiful valid code in a short time.
- Don't use id attribute for styling purpose. Classes only.
- Use id ONLY for Javascript selectors, add prefix js_* into it. Example: id="js_thisButtonDoSomethingCool".
- Write comments when you can.


CSS
===

- Use tab (4 spaces) to indent your code.
- Put spaces after : in property declarations.
- Put spaces before { in rule declarations.
- Use hex color codes #000.
- Don't write style for any id, kick the ones who use ids and ask for style in the arse.
- Avoid using !important to the minimum.
- Write comments when you can.


Javascript
==========

- Use tab (4 spaces) to indent your code.
- Use JSlint.
- Use only 1 var in the beginning of the function.
- Write your own selectors in HTML, so that any CSS change won't break your code.
- Write comments when you can.