{%- if env == 'prod' %}
server {
    listen 80;
    server_name {{ host_name }};
    rewrite ^ https://$server_name$request_uri? permanent;
}
{% endif -%}

server {
{%- if env == 'prod' %}

    listen 443 default ssl;
    ssl on;
    ssl_certificate /etc/ssl/SSL.crt;
    ssl_certificate_key /etc/ssl/server.key;
    ssl_prefer_server_ciphers on;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers DHE-RSA-CAMELLIA256-SHA:DHE-RSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA:DES-CBC3-SHA:!ADH:!aNULL;

{% elif env == 'stag' %}

    listen *:80;

{% endif -%}

    server_name {{ host_name }};

    index index.html index.htm index.php index.cgi index.pl index.xhtml;

    recursive_error_pages on;

    access_log  {{ webapps_dir }}/{{ app_name }}/log/nginx-access.log;
    error_log {{ webapps_dir }}/{{ app_name }}/log/nginx-error.log;

    rewrite ^/hyvis/?$ http://px8.fi/hyvis/index.html redirect;

    if ($host !~* ^({{ host_name }}|www.{{ host_name }})$) {
        return 444;
    }

    location / {
        proxy_pass http://127.0.0.1:{{ gunicorn_port }};
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Protocol $scheme;
    }

    # Disable .htaccess and other hidden files
    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }

    location /static {
        alias {{ webapps_dir }}/{{ app_name }}/static;
        if ($uri ~* ".*\.[a-f0-9]{12,}\.(css|js|png|jpg|jpeg|gif|swf|ico)" ) {
            expires max;
        }
    }

    location /media {
        root            {{ webapps_dir }}/{{ app_name }}/src/{{ app_name }};
        access_log      off;
        log_not_found   off;
    }

    location /robots.txt {
        root            {{ webapps_dir }}/{{ app_name }}/static;
        access_log      off;
        log_not_found   off;
    }
}
