from {{ app_name }}.settings.base import *

{% if env == 'prod' %}
from {{ app_name }}.settings.prod import *
{% elif env == 'stag' %}
from {{ app_name }}.settings.stag import *
{% endif %}

SECRET_KEY = '{{ secretkey }}'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ dbname }}',
        'USER': '{{ dbuser }}',
        'PASSWORD': '{{ dbpassword }}',
        'HOST': 'localhost',
        'PORT': '',
    }
}

VZAAR_API = {
    'key': '{{ vzaar_key }}',
    'secret': '{{ vzaar_secret }}',
}

DEBUG = False

EMAIL_BACKEND = 'sgbackend.SendGridBackend'
SENDGRID_API_KEY = '{{ sendgrid_api_key }}'

STATIC_ROOT = '/srv/{{ app_name }}/static'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

LOGGING['handlers']['logfile']['filename'] = (
    '/srv/{{ app_name }}/log/whambush.log'
)
